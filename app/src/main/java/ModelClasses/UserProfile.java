package ModelClasses;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfile implements Parcelable{

    @Expose
    @SerializedName("company_id")
    private String company_id;

    @Expose
    @SerializedName("bank_name")
    private String bank_name;

    @Expose
    @SerializedName("bill_no_series")
    private String bill_no_series;

    protected UserProfile(Parcel in) {
        company_id = in.readString();
        bank_name = in.readString();
        bill_no_series = in.readString();
        state_code = in.readString();
        ac_type = in.readString();
        state = in.readString();
        bank_branch = in.readString();
        password = in.readString();
        pan = in.readString();
        id = in.readString();
        first_name = in.readString();
        mid_name = in.readString();
        ifsc_code = in.readString();
        address = in.readString();
        email = in.readString();
        company_name = in.readString();
        company_logo = in.readString();
        last_name = in.readString();
        ac_no = in.readString();
        created_at = in.readString();
        gender = in.readString();
        role = in.readString();
        gstn = in.readString();
        cin = in.readString();
        mobile = in.readString();
    }

    public static final Creator<UserProfile> CREATOR = new Creator<UserProfile>() {
        @Override
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        @Override
        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };

    public String getBill_no_series() {
        return bill_no_series;
    }

    public void setBill_no_series(String bill_no_series) {
        this.bill_no_series = bill_no_series;
    }

    @Expose
    @SerializedName("state_code")
    private String state_code;

    @Expose
    @SerializedName("ac_type")
    private String ac_type;

    @Expose
    @SerializedName("state")
    private String state;

    @Expose
    @SerializedName("bank_branch")
    private String bank_branch;

    @Expose
    @SerializedName("password")
    private String password;

    @Expose
    @SerializedName("pan")
    private String pan;

    @Expose
    @SerializedName("id;")
    private String id;

    @Expose
    @SerializedName("first_name")
    private String first_name;

    @Expose
    @SerializedName("mid_name")
    private String mid_name;

    @Expose
    @SerializedName("ifsc_code")
    private String ifsc_code;

    @Expose
    @SerializedName("address")
    private String address;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("company_name")
    private String company_name;

    @Expose
    @SerializedName("company_logo")
    private String company_logo;

    @Expose
    @SerializedName("last_name")
    private String last_name;

    @Expose
    @SerializedName("ac_no")
    private String ac_no;

    @Expose
    @SerializedName("created_at")
    private String created_at;

    @Expose
    @SerializedName("gender")
    private String gender;

    @Expose
    @SerializedName("role")
    private String role;

    @Expose
    @SerializedName("gstn")
    private String gstn;

    @Expose
    @SerializedName("cin")
    private String cin;

    @Expose
    @SerializedName("mobile")
    private String mobile;


    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getAc_type() {
        return ac_type;
    }

    public void setAc_type(String ac_type) {
        this.ac_type = ac_type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBank_branch() {
        return bank_branch;
    }

    public void setBank_branch(String bank_branch) {
        this.bank_branch = bank_branch;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMid_name() {
        return mid_name;
    }

    public void setMid_name(String mid_name) {
        this.mid_name = mid_name;
    }

    public String getIfsc_code() {
        return ifsc_code;
    }

    public void setIfsc_code(String ifsc_code) {
        this.ifsc_code = ifsc_code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_logo() {
        return company_logo;
    }

    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAc_no() {
        return ac_no;
    }

    public void setAc_no(String ac_no) {
        this.ac_no = ac_no;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getGstn() {
        return gstn;
    }

    public void setGstn(String gstn) {
        this.gstn = gstn;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "ClassPojo [company_id = " + company_id + ", bank_name = " + bank_name + ", state_code = " + state_code +
                ", ac_type = " + ac_type + ", state = " + state + ", bank_branch = " + bank_branch + ", password = "
                + password + ", pan = " + pan + ", id = " + id + ", first_name = " + first_name + ", mid_name = "
                + mid_name + ", ifsc_code = " + ifsc_code + ", address = " + address + ", email = " + email + ", company_name = "
                + company_name + ", company_logo = " + company_logo + ", last_name = " + last_name + ", ac_no = " + ac_no + ", created_at = "
                + created_at + ", gender = " + gender + ", role = " + role + ", gstn = " + gstn + ", cin = " +
                cin + ",bill_no_series=" + bill_no_series + ", mobile = " + mobile + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(company_id);
        dest.writeString(bank_name);
        dest.writeString(bill_no_series);
        dest.writeString(state_code);
        dest.writeString(ac_type);
        dest.writeString(state);
        dest.writeString(bank_branch);
        dest.writeString(password);
        dest.writeString(pan);
        dest.writeString(id);
        dest.writeString(first_name);
        dest.writeString(mid_name);
        dest.writeString(ifsc_code);
        dest.writeString(address);
        dest.writeString(email);
        dest.writeString(company_name);
        dest.writeString(company_logo);
        dest.writeString(last_name);
        dest.writeString(ac_no);
        dest.writeString(created_at);
        dest.writeString(gender);
        dest.writeString(role);
        dest.writeString(gstn);
        dest.writeString(cin);
        dest.writeString(mobile);
    }
}
