package ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GSTListDetails {

    @SerializedName("igst")
    @Expose
    private String igst;
    @SerializedName("igst_per")
    @Expose
    private String igstPer;
    @SerializedName("cgst")
    @Expose
    private String cgst;
    @SerializedName("cgst_per")
    @Expose
    private String cgstPer;
    @SerializedName("sgst")
    @Expose
    private String sgst;
    @SerializedName("sgst_per")
    @Expose
    private String sgstPer;
    @SerializedName("ugst")
    @Expose
    private String ugst;
    @SerializedName("ugst_percetage")
    @Expose
    private String ugstPercetage;

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getIgstPer() {
        return igstPer;
    }

    public void setIgstPer(String igstPer) {
        this.igstPer = igstPer;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getCgstPer() {
        return cgstPer;
    }

    public void setCgstPer(String cgstPer) {
        this.cgstPer = cgstPer;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getSgstPer() {
        return sgstPer;
    }

    public void setSgstPer(String sgstPer) {
        this.sgstPer = sgstPer;
    }

    public String getUgst() {
        return ugst;
    }

    public void setUgst(String ugst) {
        this.ugst = ugst;
    }

    public String getUgstPercetage() {
        return ugstPercetage;
    }

    public void setUgstPercetage(String ugstPercetage) {
        this.ugstPercetage = ugstPercetage;
    }

}
