package ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BillListDetailPojo {




    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("name_of_supplier")
    @Expose
    private String nameOfSupplier;
    @SerializedName("gstin_uin")
    @Expose
    private String gstinUin;
    @SerializedName("state_name")
    @Expose
    private String stateName;
    @SerializedName("pos")
    @Expose
    private String pos;
    @SerializedName("invoice_no")
    @Expose
    private String invoiceNo;
    @SerializedName("invoice_date")
    @Expose
    private String invoiceDate;
    @SerializedName("invoice_value")
    @Expose
    private String invoiceValue;
    @SerializedName("hsn_sac_code")
    @Expose
    private String hsnSacCode;
    @SerializedName("good_service_description")
    @Expose
    private String goodServiceDescription;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("invoice_taxable_amt")
    @Expose
    private String invoiceTaxableAmt;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("igst")
    @Expose
    private String igst;
    @SerializedName("igst_per")
    @Expose
    private String igstPer;
    @SerializedName("cgst")
    @Expose
    private String cgst;
    @SerializedName("cgst_per")
    @Expose
    private String cgstPer;
    @SerializedName("sgst")
    @Expose
    private String sgst;
    @SerializedName("sgst_per")
    @Expose
    private String sgstPer;
    @SerializedName("ship_details")
    @Expose
    private String shipDetails;
    @SerializedName("vechical_no")
    @Expose
    private String vechicalNo;
    @SerializedName("ugst")
    @Expose
    private String ugst;
    @SerializedName("ugst_percetage")
    @Expose
    private String ugstPercetage;
    @SerializedName("qnty")
    @Expose
    private String qnty;
    @SerializedName("amount_per_tax")
    @Expose
    private String amountPerTax;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("product")
    @Expose
    private List<ProductDetails> product = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getNameOfSupplier() {
        return nameOfSupplier;
    }

    public void setNameOfSupplier(String nameOfSupplier) {
        this.nameOfSupplier = nameOfSupplier;
    }

    public String getGstinUin() {
        return gstinUin;
    }

    public void setGstinUin(String gstinUin) {
        this.gstinUin = gstinUin;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceValue() {
        return invoiceValue;
    }

    public void setInvoiceValue(String invoiceValue) {
        this.invoiceValue = invoiceValue;
    }

    public String getHsnSacCode() {
        return hsnSacCode;
    }

    public void setHsnSacCode(String hsnSacCode) {
        this.hsnSacCode = hsnSacCode;
    }

    public String getGoodServiceDescription() {
        return goodServiceDescription;
    }

    public void setGoodServiceDescription(String goodServiceDescription) {
        this.goodServiceDescription = goodServiceDescription;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getInvoiceTaxableAmt() {
        return invoiceTaxableAmt;
    }

    public void setInvoiceTaxableAmt(String invoiceTaxableAmt) {
        this.invoiceTaxableAmt = invoiceTaxableAmt;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getIgstPer() {
        return igstPer;
    }

    public void setIgstPer(String igstPer) {
        this.igstPer = igstPer;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getCgstPer() {
        return cgstPer;
    }

    public void setCgstPer(String cgstPer) {
        this.cgstPer = cgstPer;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getSgstPer() {
        return sgstPer;
    }

    public void setSgstPer(String sgstPer) {
        this.sgstPer = sgstPer;
    }

    public String getShipDetails() {
        return shipDetails;
    }

    public void setShipDetails(String shipDetails) {
        this.shipDetails = shipDetails;
    }

    public String getVechicalNo() {
        return vechicalNo;
    }

    public void setVechicalNo(String vechicalNo) {
        this.vechicalNo = vechicalNo;
    }

    public String getUgst() {
        return ugst;
    }

    public void setUgst(String ugst) {
        this.ugst = ugst;
    }

    public String getUgstPercetage() {
        return ugstPercetage;
    }

    public void setUgstPercetage(String ugstPercetage) {
        this.ugstPercetage = ugstPercetage;
    }

    public String getQnty() {
        return qnty;
    }

    public void setQnty(String qnty) {
        this.qnty = qnty;
    }

    public String getAmountPerTax() {
        return amountPerTax;
    }

    public void setAmountPerTax(String amountPerTax) {
        this.amountPerTax = amountPerTax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<ProductDetails> getProduct() {
        return product;
    }

    public void setProduct(List<ProductDetails> product) {
        this.product = product;
    }









    @Override
    public String toString()
    {
        return "ClassPojo [company_id = "+companyId+", gstin_uin = "+gstinUin+", sgst_per = "+sgstPer+", invoice_value = "+invoiceValue+", good_service_description = "+goodServiceDescription+", qty = "+qty+", invoice_date = "+invoiceDate+", sgst = "+sgst+", state_name = "+stateName+", hsn_sac_code = "+hsnSacCode+", pos = "+pos+", id = "+id+", rate = "+rate+", address = "+address+", cgst_per = "+cgstPer+", igst = "+igst+", name_of_supplier = "+nameOfSupplier+", cgst = "+cgst+", igst_per = "+igstPer+", invoice_no = "+invoiceNo+", invoice_taxable_amt = "+invoiceTaxableAmt+"]";
    }
}
