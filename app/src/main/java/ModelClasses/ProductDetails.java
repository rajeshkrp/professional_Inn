package ModelClasses;

import android.support.annotation.NonNull;
import android.view.View;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import UtilClasses.InvoiceTempData;

public class ProductDetails  implements  Comparable<ProductDetails>{



    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("pos")
    @Expose
    private String pos;
    @SerializedName("invoice_no")
    @Expose
    private String invoiceNo;
    @SerializedName("hsn_sac_code")
    @Expose
    private String hsnSacCode;
    @SerializedName("good_service_description")
    @Expose
    private String goodServiceDescription;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("invoice_taxable_amt")
    @Expose
    private String invoiceTaxableAmt;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("igst")
    @Expose
    private String igst;
    @SerializedName("igst_per")
    @Expose
    private String igstPer;
    @SerializedName("cgst")
    @Expose
    private String cgst;
    @SerializedName("cgst_per")
    @Expose
    private String cgstPer;
    @SerializedName("sgst")
    @Expose
    private String sgst;
    @SerializedName("sgst_per")
    @Expose
    private String sgstPer;
    @SerializedName("ugst")
    @Expose
    private String ugst;
    @SerializedName("ugst_percetage")
    @Expose
    private String ugstPercetage;
    @SerializedName("qnty")
    @Expose
    private String qnty;
    @SerializedName("amount_per_tax")
    @Expose
    private String amountPerTax;
    @SerializedName("serial_no")
    @Expose
    private String serialNo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getHsnSacCode() {
        return hsnSacCode;
    }

    public void setHsnSacCode(String hsnSacCode) {
        this.hsnSacCode = hsnSacCode;
    }

    public String getGoodServiceDescription() {
        return goodServiceDescription;
    }

    public void setGoodServiceDescription(String goodServiceDescription) {
        this.goodServiceDescription = goodServiceDescription;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getInvoiceTaxableAmt() {
        return invoiceTaxableAmt;
    }

    public void setInvoiceTaxableAmt(String invoiceTaxableAmt) {
        this.invoiceTaxableAmt = invoiceTaxableAmt;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getIgstPer() {
        return igstPer;
    }

    public void setIgstPer(String igstPer) {
        this.igstPer = igstPer;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getCgstPer() {
        return cgstPer;
    }

    public void setCgstPer(String cgstPer) {
        this.cgstPer = cgstPer;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getSgstPer() {
        return sgstPer;
    }

    public void setSgstPer(String sgstPer) {
        this.sgstPer = sgstPer;
    }

    public String getUgst() {
        return ugst;
    }

    public void setUgst(String ugst) {
        this.ugst = ugst;
    }

    public String getUgstPercetage() {
        return ugstPercetage;
    }

    public void setUgstPercetage(String ugstPercetage) {
        this.ugstPercetage = ugstPercetage;
    }

    public String getQnty() {
        return qnty;
    }

    public void setQnty(String qnty) {
        this.qnty = qnty;
    }

    public String getAmountPerTax() {
        return amountPerTax;
    }

    public void setAmountPerTax(String amountPerTax) {
        this.amountPerTax = amountPerTax;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    @Override
    public int compareTo(@NonNull ProductDetails invoiceTempData) {

        if(invoiceTempData.getSgst()!=null&&(!invoiceTempData.getSgst().equalsIgnoreCase("0"))){

            return sgstPer.compareTo(invoiceTempData.getSgstPer());


        }
        else if(invoiceTempData.getUgst()!=null&&(!invoiceTempData.getUgst().equalsIgnoreCase("0"))){

            return ugst.compareTo(invoiceTempData.getUgstPercetage());

        }
        else if(invoiceTempData.getIgst()!=null&&(!invoiceTempData.getIgst().equalsIgnoreCase("0"))){

            return igstPer.compareTo(invoiceTempData.getIgstPer());


        }
        else {
          //  return igstPer.compareTo(invoiceTempData.getIgstPer());
            return 0;

        }

    }
}
