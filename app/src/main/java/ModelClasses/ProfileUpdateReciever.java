package ModelClasses;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by abul on 16/11/17.
 */

public class ProfileUpdateReciever extends BroadcastReceiver{

    public static final int HASH_KEY = 2;

    //public static final String UPDATE_INTENT="com.madhu.action.PROFILE_INTENT";
    public static final String UPDATE_INTENT="com.madhu.action.UPDATE_INTENT";


    private ProfileUpdateListner lisner;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(lisner!=null){
            lisner.onPicUpdate();
        }

    }

    public void setUpdateLisner(ProfileUpdateListner lisner){
        this.lisner=lisner;
    }

    public interface ProfileUpdateListner{
        void onPicUpdate();
    }
}
