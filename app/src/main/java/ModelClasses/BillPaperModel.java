package ModelClasses;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

public class BillPaperModel  extends ViewModel {
    int serialNumber;

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDescription_product() {
        return description_product;
    }

    public void setDescription_product(String description_product) {
        this.description_product = description_product;
    }

    public String getHsn_sas() {
        return hsn_sas;
    }

    public void setHsn_sas(String hsn_sas) {
        this.hsn_sas = hsn_sas;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTaxable() {
        return taxable;
    }

    public void setTaxable(String taxable) {
        this.taxable = taxable;
    }

    String description_product;
    String hsn_sas;
    String quantity;
    String rate;
    String taxable;



    private  MutableLiveData<List<BillPaperModel>> selected = new MutableLiveData<List<BillPaperModel>>();

    public void select(List<BillPaperModel> item) {
        selected.setValue(item);
    }

    public LiveData<List<BillPaperModel>> getSelected() {
        return selected;
    }
    public BillPaperModel(){

}

public BillPaperModel(int serialNumber, String description_product, String hsn_sas, String quantity, String rate, String taxable){
    this.serialNumber=serialNumber;
    this.description_product=description_product;
    this.hsn_sas=hsn_sas;
    this.quantity=quantity;
    this.rate=rate;
    this.taxable=taxable;
}


}
