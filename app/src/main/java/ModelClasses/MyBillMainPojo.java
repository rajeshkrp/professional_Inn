package ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyBillMainPojo {


    @SerializedName("basicRate")
    @Expose
    private Double basicRate;
    @SerializedName("totalInvoice")
    @Expose
    private Integer totalInvoice;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("bills")
    @Expose
    private List<MyBillDetailPojo> bills = null;
    @SerializedName("response")
    @Expose
    private Boolean response;

    public Double getBasicRate() {
        return basicRate;
    }

    public void setBasicRate(Double basicRate) {
        this.basicRate = basicRate;
    }

    public Integer getTotalInvoice() {
        return totalInvoice;
    }

    public void setTotalInvoice(Integer totalInvoice) {
        this.totalInvoice = totalInvoice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MyBillDetailPojo> getBills() {
        return bills;
    }

    public void setBills(List<MyBillDetailPojo> bills) {
        this.bills = bills;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }





}
