package ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainFilterPojo {

    @SerializedName("basicRate")
    @Expose
    private Integer basicRate;
    @SerializedName("totalInvoice")
    @Expose
    private Integer totalInvoice;
    @SerializedName("bills")
    @Expose
    private List<MainFilterDeatilsPojo> bills = null;
    @SerializedName("code")
    @Expose
    private String code;

    public Integer getBasicRate() {
        return basicRate;
    }

    public void setBasicRate(Integer basicRate) {
        this.basicRate = basicRate;
    }

    public Integer getTotalInvoice() {
        return totalInvoice;
    }

    public void setTotalInvoice(Integer totalInvoice) {
        this.totalInvoice = totalInvoice;
    }

    public List<MainFilterDeatilsPojo> getBills() {
        return bills;
    }

    public void setBills(List<MainFilterDeatilsPojo> bills) {
        this.bills = bills;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
