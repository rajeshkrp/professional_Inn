package gstapp.com.retrofitClasses;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import ModelClasses.AllBillMainPojo;
import ModelClasses.MyBillMainPojo;
import ModelClasses.PlanPOJO;
import ModelClasses.ProfileMainPojo;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;


public interface ApiInterface {

    @POST("user-login.php")
    @FormUrlEncoded
    Call<ResponseBody> createUser(@Field("email") String email, @Field("password") String password);

    @POST("user-profile.php")
    @FormUrlEncoded
    Call<ProfileMainPojo> userProfile(@Field("user_id") String user_id);

    @POST("planRequest.php")
    @FormUrlEncoded
    Call<PlanPOJO> getRequestPlan(@Field("name") String name, @Field("plan") String plan, @Field("benfit") String benfit, @Field("periods") String periods, @Field("prices") String price);

    @POST("get-current-bill_no.php")
    @FormUrlEncoded
    Call<ResponseBody> getBillNo(@Field("user_id") String user_id);

    @Multipart
    @POST("update-user-profile.php")
    Call<ResponseBody> updateUserData(
            @Part("user_id") RequestBody user_id,
            @Part("mobile") RequestBody mobile,
            @Part("address") RequestBody address,
            @Part("dob") RequestBody dob,
            @Part("gstn") RequestBody gstn,
            @Part("cin") RequestBody cin,
            @Part("pan") RequestBody pan,
            @Part("state_code") RequestBody state_code,
            @Part("state") RequestBody state,
            @Part("b_name") RequestBody b_name,
            @Part("b_branch") RequestBody b_branch,
            @Part("bill_no") RequestBody bill_no,
            @Part("ac_no") RequestBody ac_no,
            @Part("ifsc_code") RequestBody ifsc_code,
            @Part("ac_type") RequestBody ac_type,
            @Part MultipartBody.Part[] file);

    @POST("save_invoice.php")
    @FormUrlEncoded
    Call<ResponseBody> CreateBill(@FieldMap HashMap<String, String> hashMap);

    /*@POST("get-bills.php")
    @FormUrlEncoded
    Call<MyBillMainPojo> getBillList(@Field("user_id") String user_id);*/


    @POST("get-bills.php")
    @FormUrlEncoded
    Call<ResponseBody> getBillList(@Field("user_id") String user_id);





    @Multipart
    @POST("upload-bills.php")
    Call<ResponseBody> uploadBills(@Part("user_id") RequestBody user_id,@Part("category") RequestBody category, @Part MultipartBody.Part[] file);

    @GET("about.php")
    Call<ResponseBody> getAboutContent();

    @POST("search_invoice.php")
    @FormUrlEncoded
    Call<MyBillMainPojo> getFilteredData(@Field("user_id") String user_id, @Field("fromDate") String fromDate
            , @Field("toDate") String toDate, @Field("name") String name, @Field("invoice_no") String invoice_no);

   /* @POST("filter_billingdata.php")
    @FormUrlEncoded
    Call<AllBillMainPojo> getFiltePurchaseList(@Field("user_id") String user_id, @Field("fromDate") String fromDate
            , @Field("toDate") String toDate);*/




    @POST("filter_billingdata.php")
    @FormUrlEncoded
    Call<ResponseBody> getFiltePurchaseList(@Field("user_id") String user_id, @Field("fromDate") String fromDate
            , @Field("toDate") String toDate);






/*
    @POST("billImages.php")
    @FormUrlEncoded
    Call<AllBillMainPojo> getAllBillsImages(@Field("user_id") String user_id);*/



    @POST("billImages.php")
    @FormUrlEncoded
    Call<ResponseBody> getAllBillsImages(@Field("user_id") String user_id);



    @POST("changePassword.php")
    @FormUrlEncoded
    Call<ResponseBody> changePassword(@Field("user_id") String user_id,
                                         @Field("oldPassword") String oldPassword,
                                         @Field("newPassword") String newPassword);





}