package gstapp.com.retrofitClasses;

public interface RowType {

    int BUTTON_ROW_TYPE = 0;
    int IMAGE_ROW_TYPE = 1;
    int TEXT_ROW_TYPE = 2;
}
