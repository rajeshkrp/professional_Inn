package gstapp.com.create_invoice_module;

/**
 * Created by Abdul on 4/10/2018.
 */

public interface CreateInvoiceView {

    void showProgress();

    void hideProgress();

    void onAlreadysucess( String msg);

    void onSuccess();
}
