package gstapp.com.create_invoice_module;

import UtilClasses.InvoiceTempData;

/**
 * Created by Abdul on 4/10/2018.
 */

public interface CreateInvoiceInteractor {

    void createBill(InvoiceTempData invoiceTempData, String currentDate, String id,OnBillFinishedListener onBillFinishedListener);

    interface OnBillFinishedListener {

        void onUsernameError();

        void onPasswordError();

        void onSuccess();

        void onRoleError(String message);

        void onError();

        void onFailure();
    }

}
