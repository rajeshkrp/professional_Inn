package gstapp.com.create_invoice_module;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import UtilClasses.CommonUtils;
import UtilClasses.InvoiceTempData;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.Adapters.StateCodeAdapter;
import gstapp.com.gstapp.EditProfileActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Customer_Details extends Fragment implements Step, CreateInvoiceView {

    Context context;
    AppCompatSpinner appCompatSpinner;
    private String stateName;

    @BindView(R.id.stateCode_text)
    TextView stateCodeTxt;

    @BindView(R.id.newInvoiceNo)
    TextView newInvoiceNo;

    @BindView(R.id.gstStateName)
    TextView gstStateName;


    @BindView(R.id.supplier_name)
    EditText supplier_name;

    @BindView(R.id.supplier_address)
    EditText supplier_address;

    @BindView(R.id.supplier_shipping_address)
    EditText supplier_shipping_address;


    @BindView(R.id.billgstStateName)
    TextView billgstStateName;

    @BindView(R.id.supplier_GSTN)
    EditText supplier_GSTN;

    @BindView(R.id.supplier_vehicle_number)
    EditText supplier_vehicle_number;


    @BindView(R.id.cbAddress)
    CheckBox cbAddress;


    HashMap<String, String> stateCodes;
    private ApiInterface apiInterface;
    private String billNo="";

    InvoiceTempData invoiceTempData;

    CreateInvoicePresenter createInvoicePresenter;
    private StateCodeAdapter clad;


    public Customer_Details() {
        // Required empty public constructor
    }


    @Override
    public void onDestroy() {
        super.onDestroy();


    }


    public static Customer_Details newInstance() {
        Customer_Details fragment = new Customer_Details();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test, container, false);
        ButterKnife.bind(this, view);
        context=getActivity();



        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Toast.makeText(context, "onActivity", Toast.LENGTH_SHORT).show();


        //appCompatSpinner = (AppCompatSpinner) getView().findViewById(R.id.satate_spinner);
        stateCodes = new HashMap<>();
        /*ArrayList<String> stateList = new ArrayList<>();
        prepareStateList(stateList);*/

        invoiceTempData = InvoiceTempData.getInstance();

        apiInterface = APIClient.getClient().create(ApiInterface.class);

        getPermission();

        getBillNo();

        prepareHashMap(stateCodes);

        gstStateName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openStateDialog("shipState");
            }
        });



        billgstStateName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openStateDialog("billState");
            }
        });



       /* ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, stateList);
        appCompatSpinner.setAdapter(arrayAdapter);

        appCompatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String content = appCompatSpinner.getSelectedItem().toString();
                stateName = content;
                stateCodeTxt.setText(stateCodes.get(content));
                //  String newStateCode = stateCodes.get(content);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        cbAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b){

                    if(supplier_address.getText().toString().equalsIgnoreCase("")){

                     //   Toast.makeText(context, "if", Toast.LENGTH_SHORT).show();

                        // Toast.makeText(context, "else", Toast.LENGTH_SHORT).show();
                       // CommonUtils.snackBar("Please enter  Billing Adress", supplier_name, "300");



                    }

                    supplier_shipping_address.setText(supplier_address.getText().toString());
                    gstStateName.setText(billgstStateName.getText().toString());

                }
                else {
                   // Toast.makeText(context, "else", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }



    private void getPermission() {
    }

    private void prepareHashMap(HashMap<String, String> stateCodes) {

        stateCodes.put("Andaman and Nicobar Islands", "35");
        stateCodes.put("Andhra Pradesh", "37");
        stateCodes.put("Arunachal Pradesh", "12");
        stateCodes.put("Assam", "18");
        stateCodes.put("Bihar", "10");
        stateCodes.put("Chandigarh", "04");
        stateCodes.put("Chhattisgarh", "22");
        stateCodes.put("Dadra and Nagar Haveli", "26");
        stateCodes.put("Daman and Diu", "25");
        stateCodes.put("Delhi", "07");
        stateCodes.put("Goa", "30");
        stateCodes.put("Gujarat", "24");
        stateCodes.put("Haryana", "06");
        stateCodes.put("Himachal Pradesh", "02");
        stateCodes.put("Jammu & Kashmir", "01");
        stateCodes.put("Jharkhand", "20");
        stateCodes.put("Karnataka", "29");
        stateCodes.put("Kerala", "32");
        stateCodes.put("Lakshadweep Islands", "31");
        stateCodes.put("Madhya Pradesh", "23");
        stateCodes.put("Maharashtra", "27");
        stateCodes.put("Manipur", "14");
        stateCodes.put("Meghalaya", "17");
        stateCodes.put("Mizoramh", "15");
        stateCodes.put("Nagaland", "13");
        stateCodes.put("Odisha", "21");
        stateCodes.put("Pondicherry", "34");
        stateCodes.put("Punjab", "03");
        stateCodes.put("Rajasthan", "08");
        stateCodes.put("Sikkim", "11");
        stateCodes.put("Tamil Nadu", "33");
        stateCodes.put("Telangana", "36");
        stateCodes.put("Tripura", "16");
        stateCodes.put("Uttar Pradesh", "09");
        stateCodes.put("Uttarakhand", "05");
        stateCodes.put("West Bengal", "19");
    }

    private void increamentAndSaveBillNo(String bilNo) {

        String numOnly = bilNo.replaceAll("[\\D]", "");
        long num = 0;
        try {
            num = Long.parseLong(numOnly);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        String newstr = bilNo.replaceAll("[^A-Za-z]+", "");

        long newNum = num + 1;

        String requiredString = newstr + newNum;

      //  newInvoiceNo.setText("Invoice No : #" + requiredString);
        newInvoiceNo.setText("Invoice No : #" + billNo);

      //  invoiceTempData.setInvoiceNo(requiredString);
        if(bilNo!=null){
        invoiceTempData.setInvoiceNo(billNo);
      //  saveInvoiceLocal(requiredString);
        saveInvoiceLocal(billNo);
        }

    }


    @Override
    public void onResume() {
        super.onResume();

       // Toast.makeText(context, "onResumeCutomer", Toast.LENGTH_SHORT).show();

        if(!CommonUtils.getPreferencesString(context, "shipState").equalsIgnoreCase("")||!CommonUtils.getPreferencesString(context, "shipState").equalsIgnoreCase("")) {
            gstStateName.setText(CommonUtils.getPreferencesString(context, "shipState"));
            billgstStateName.setText(CommonUtils.getPreferencesString(context, "billState"));
           cbAddress.setChecked(true);
            }
        else {
            //Toast.makeText(context, "blank", Toast.LENGTH_SHORT).show();
            gstStateName.setText("Select State");
            billgstStateName.setText("Select State");
            cbAddress.setChecked(false);

        }
    }

    private void getBillNo() {

        String id = CommonUtils.getUserId(getActivity());
        Call<ResponseBody> responseBodyCall = apiInterface.getBillNo(id);

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String res="";

                if (res != null) {

                    try {
                        res = response.body().string();

                        Log.e("CustomeResponse","CustomeResponse"+res);

                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        if (status.equals("200")) {

                            billNo = jsonObject.getString("bill_no");

                            increamentAndSaveBillNo(billNo);
                            // newInvoiceNo.setText("Invoice No : #" + billNo);

                            saveInvoiceLocal(billNo);

                        } else {
                            checkandGetBillFromLocal();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void checkandGetBillFromLocal() {

        billNo = CommonUtils.getBillNo(getActivity());

        if (billNo != null && !billNo.equals("")) {
            increamentAndSaveBillNo(billNo);
        } else {
            ShowAlertDialog();
        }

    }

    private void ShowAlertDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Please Update Bill Series No");
        builder.setCancelable(false);


        builder.setPositiveButton("Update Profile", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                CommonUtils.isProfile = true;
                getActivity().finish();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                getActivity().finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void saveInvoiceLocal(String requiredString) {

        CommonUtils.saveBillNo(context, requiredString);

    }


    private void prepareStateList(ArrayList<String> stateList) {

        stateList.add("Andaman and Nicobar Islands");
        stateList.add("Andhra Pradesh");
        stateList.add("Arunachal Pradesh");
        stateList.add("Assam");
        stateList.add("Bihar");
        stateList.add("Chandigarh");
        stateList.add("Chhattisgarh");
        stateList.add("Dadra and Nagar Haveli");
        stateList.add("Daman and Diu");
        stateList.add("Delhi");
        stateList.add("Goa");
        stateList.add("Gujarat");
        stateList.add("Haryana");
        stateList.add("Himachal Pradesh");
        stateList.add("Jammu & Kashmir");
        stateList.add("Jharkhand");
        stateList.add("Karnataka");
        stateList.add("Kerala");
        stateList.add("Lakshadweep Islands");
        stateList.add("Madhya Pradesh");
        stateList.add("Maharashtra");
        stateList.add("Manipur");
        stateList.add("Meghalaya");
        stateList.add("Mizoramh");
        stateList.add("Nagaland");
        stateList.add("Odisha");
        stateList.add("Pondicherry");
        stateList.add("Punjab");
        stateList.add("Rajasthan");
        stateList.add("Sikkim");
        stateList.add("Tamil Nadu");
        stateList.add("Telangana");
        stateList.add("Tripura");
        stateList.add("Uttar Pradesh");
        stateList.add("Uttarakhand");
        stateList.add("West Bengal");
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Nullable
    @Override
    public VerificationError verifyStep() {


        if (supplier_name.length() == 0) {
            CommonUtils.snackBar("Please enter name", supplier_name, "300");
            return new VerificationError("Please enter name");
        }

        else if (supplier_vehicle_number.length() == 0) {
            CommonUtils.snackBar("Please enter vehicle number", supplier_name, "300");
            return new VerificationError("Please enter vehicle number");
        }

        else if (supplier_address.length() == 0) {
            CommonUtils.snackBar("Please enter  bill address", supplier_name, "300");
            return new VerificationError("Please enter bill address");
        }

        else if (billgstStateName.getText().toString().equalsIgnoreCase("Select State")) {
            CommonUtils.snackBar("Please enter  Select State", supplier_name, "300");
            return new VerificationError("Please enter Select State");
        }

        else if (supplier_shipping_address.length() == 0) {
            CommonUtils.snackBar("Please enter shipping  address", supplier_name, "300");
            return new VerificationError("Please enter shipping address");
        }
        else if (gstStateName.getText().toString().equalsIgnoreCase("Select State")) {
            CommonUtils.snackBar("Please enter Select State", supplier_name, "300");
            return new VerificationError("Please enter Select State");
        }

        else if (supplier_GSTN.length() == 0) {
            CommonUtils.snackBar("Please enter GSTN No", supplier_name, "300");
            return new VerificationError("Please enter GSTN No");

        }

        else {

            invoiceTempData.setCustName(supplier_name.getText().toString());
            invoiceTempData.setBillAddress(supplier_address.getText().toString());
            invoiceTempData.setBillcustState(billgstStateName.getText().toString());

            invoiceTempData.setCustAddress(supplier_shipping_address.getText().toString());



            invoiceTempData.setWholeshippingAddress(supplier_shipping_address.getText().toString()+","+gstStateName.getText().toString()+","+invoiceTempData.getStateCode());
            invoiceTempData.setWholeBillAddress(supplier_address.getText().toString()+","+billgstStateName.getText().toString()+","+invoiceTempData.getBillstateCode());

            invoiceTempData.setCustState(gstStateName.getText().toString());


            invoiceTempData.setCustGstn(supplier_GSTN.getText().toString());
            invoiceTempData.setVehicle(supplier_vehicle_number.getText().toString());


            invoiceTempData.setInvoiceNo(CommonUtils.getBillNo(getActivity()));


            CommonUtils.saveStringPreferences(context,"billAddress",supplier_address.getText().toString());
            CommonUtils.saveStringPreferences(context,"shipAddress",supplier_shipping_address.getText().toString());
            return null;
        }


    }

    @Override
    public void onSelected() {

        Log.v("selected", "onSelected");
    }

    @Override
    public void onError(@NonNull VerificationError error) {

        Log.v("Error", "Error");
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onAlreadysucess(String msg) {

    }

    @Override
    public void onSuccess() {

    }


    private void openStateDialog( String stateNameAddress) {

        final ArrayList<String> stateList = new ArrayList<>();
        prepareStateList(stateList);

        ListView marital_lv;

        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_occupation);
        // dialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        ImageView leftarrow = (ImageView) dialog.findViewById(R.id.leftarrow);
        final EditText editMobileNo = (EditText) dialog.findViewById(R.id.editMobileNo);
        marital_lv = (ListView) dialog.findViewById(R.id.listview);
        //TextView saveImage = (TextView) dialog.findViewById(R.id.saveImage);
        clad = new StateCodeAdapter(getActivity(), stateList);
        marital_lv.setAdapter(clad);

        editMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                clad.filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        /*if ((muserDetails.getMarital_status() != null)) {
            clad.setSelection(muserDetails.getMarital_status());
        }*/

        marital_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //  visibleGreenTick();
               /* clad.selectedPosition = position;
                maritalValues = stateList[position];
                editor.putString("Marital_status", maritalValues);
                editor.commit();
                clad.notifyDataSetChanged();*/


               if(stateNameAddress.equalsIgnoreCase("shipState")){
                   stateName = stateList.get(position);
                   gstStateName.setText(stateName);
                   stateCodeTxt.setText(stateCodes.get(stateName));

                   invoiceTempData.setStateCode(stateCodes.get(stateName));

                   CommonUtils.saveStringPreferences(context,"shipState",stateName);



               }


               else if(stateNameAddress.equalsIgnoreCase("billState")){
                    stateName = stateList.get(position);
                    billgstStateName.setText(stateName);
                   stateCodeTxt.setText(stateCodes.get(stateName));

                   invoiceTempData.setBillstateCode(stateCodes.get(stateName));
                   invoiceTempData.setStateCode(stateCodes.get(stateName));
                   CommonUtils.saveStringPreferences(context,"billState",stateName);


               }



                InputMethodManager ipmm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                ipmm.hideSoftInputFromWindow(editMobileNo.getWindowToken(), 0);

                dialog.dismiss();

            }
        });


        /*saveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // maritalStatus.setText(maritalValues);
                dialog.dismiss();
            }
        });*/

        dialog.show();

    }
}
