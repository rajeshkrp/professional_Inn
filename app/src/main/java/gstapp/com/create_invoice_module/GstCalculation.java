package gstapp.com.create_invoice_module;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfDocument;
import android.icu.math.BigDecimal;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import ModelClasses.BillPaperModel;
import ModelClasses.UserProfile;
import UtilClasses.CommonUtils;
import UtilClasses.HintSpinner;
import UtilClasses.InvoiceTempData;
import UtilClasses.LogeedUserDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.Adapters.InvoiceGenratorAdapter;
import gstapp.com.Adapters.TaxSlabAdapter;
import gstapp.com.fragments.MyBills;
import gstapp.com.fragments.MyProfile;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import uk.co.senab.photoview.PhotoViewAttacher;


public class GstCalculation extends Fragment implements Step, BlockingStep,
        CreateInvoiceView {


    private OnFragmentInteractionListener mListener;
    // private Spinner appCompatSpinner2;
    Context context;
    String shping_detail = "";
    InvoiceGenratorAdapter invoiceGenratorAdapter;

    public static final double PDF_PAGE_WIDTH = 8.3 * 72;
    /**
     * Page height for our PDF.
     */
    public static final double PDF_PAGE_HEIGHT = 11.7 * 72;

    List<InvoiceTempData> invoiceList = new ArrayList<>();
    List<InvoiceTempData> localigstist;

    SharedPreferences preferences;

    View content;

    @BindView(R.id.CGSTAmountEdit)
    EditText CGSTAmountEdit;


    @BindView(R.id.spinner2)
    HintSpinner appCompatSpinner2;


    @BindView(R.id.SGSTAmountEdit)
    EditText SGSTAmountEdit;

    @BindView(R.id.IGSTAmountEdit)
    EditText IGSTAmountEdit;

    @BindView(R.id.CGSTEdit)
    EditText CGSTEdit;

    @BindView(R.id.SGSTEdit)
    EditText SGSTEdit;

    @BindView(R.id.IGSTEdit)
    EditText IGSTEdit;

    @BindView(R.id.amountEdit)
    EditText amountEdit;

    @BindView(R.id.UGSTEdit)
    EditText UGSTEdit;

    @BindView(R.id.UGSTAmountEdit)
    EditText UGSTAmountEdit;

  /*  @BindView(R.id.submit_button)
    Button submit_button;*/

    @BindView(R.id.btn_done)
    Button btn_done;

    @BindView(R.id.add_More_button)
    Button add_More_button;


    @BindView(R.id.preview_button)
    Button preview_button;

/*

    @BindView(R.id.tv_ship_pdf_cust_add1)
    TextView tv_ship_pdf_cust_add1;

    @BindView(R.id.tv_ship_pdf_cust_state)
    TextView tv_ship_pdf_cust_state;

    @BindView(R.id.tv_ship_pdf_cust_stateCode)
    TextView tv_ship_pdf_cust_stateCode;


    @BindView(R.id.tv_ship_pdf_cust_GSTN)
    TextView tv_ship_pdf_cust_GSTN;
*/


    @BindView(R.id.tv_sgst)
    TextView tv_sgst;

    @BindView(R.id.tv_ugst)
    TextView tv_ugst;
    private String selectedCatgeory;
    List<String> igstist;
    private String selectedCatgeoryPer;
    private float enteredAmount;
    private String totalTaxAmount;
    String stateCode = "";
    private ProgressDialog progressDailog;
    private String currentDate;
    private ApiInterface apiInterface;

    private InvoiceTempData invoiceTempData;
    private LinearLayout view;
    ArrayList<String> taxCat;

    private LogeedUserDetails logeedUserDetails;
    private CreateInvoicePresenter createInvoicePresenter;
    private StepperLayout.OnBackClickedCallback callbackbutton;
    public StepperLayout mStepperLayout;
    private InterGetDEtails interGetDEtails;
    private List<BillPaperModel> tempDataList = new ArrayList<>();
    private List<InvoiceTempData> billList;
    private List<InvoiceTempData> billListGST;
    private List<InvoiceTempData> AllbillListGST;

    public InterGetDEtails getDEtails;
    String wender_state_code="";
    private AlertDialog.Builder builder;
    InvoiceTempData invoiceTempDataList;

    List<UserProfile> userProfiles;
    private boolean amountStatus;
    private Double totalAmountGST = 0.0;
    private double taxrate = 0;
    private int count = 0;
    private float selectedValue = 0;

    String des = null, hsn = null, quty = null, rate = null, taxable = null,
            igst = null, igst_per = null,
            ugst = null, ugst_per = null,
            cgst = null, cgst_per = null,
            sgst = null, sgst_per = null, per_amount = null, total_amount = null, serial_no = null;
    private String wender_state="";


    public GstCalculation() {
        // Required empty public constructor
    }


    public static GstCalculation newInstance(String param1, String param2) {
        GstCalculation fragment = new GstCalculation();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gst_calculation, container, false);
        //  tempDataList.addAll(((CreateInvoiceActivity)getActivity()).billpaperList);

        context = getActivity();
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        wender_state_code = CommonUtils.getPreferencesString(context, "Admin_status_code");
        wender_state = CommonUtils.getPreferencesString(context, "Admin_state");

        billList = new ArrayList<>();
        billListGST = new ArrayList<>();
        AllbillListGST = new ArrayList<>();
        igstist = new ArrayList<>();
        //  Log.e("listsize","list"+((CreateInvoiceActivity)getActivity()).billpaperList.size());
        mStepperLayout = getActivity().findViewById(R.id.stepperLayout);
        ButterKnife.bind(this, view);

        return view;
    }

    // TODO: Rename method, update argu
    // and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onResume() {
        super.onResume();

        if (CommonUtils.getPreferencesString(context, "billState") != null) {

            // Toast.makeText(context, "GSTvalue:"+CommonUtils.getPreferencesString(context,"billState"), Toast.LENGTH_SHORT).show();
            CommonUtils.saveStringPreferences(context, "billState", CommonUtils.getPreferencesString(context, "billState"));
        }

        if (CommonUtils.getPreferencesString(context, "shipState") != null) {

            CommonUtils.saveStringPreferences(context, "shipState", CommonUtils.getPreferencesString(context, "shipState"));

        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();

        if (CommonUtils.getPreferencesString(context, "billState") != null) {

            //  Toast.makeText(context, "value:"+CommonUtils.getPreferencesString(context,"billState"), Toast.LENGTH_SHORT).show();

            CommonUtils.saveStringPreferences(context, "billState", CommonUtils.getPreferencesString(context, "billState"));

        }

        if (CommonUtils.getPreferencesString(context, "shipState") != null) {

            CommonUtils.saveStringPreferences(context, "shipState", CommonUtils.getPreferencesString(context, "shipState"));

        }


        invoiceTempData = InvoiceTempData.getInstance();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
     //   stateCode = preferences.getString("state_code", "");
        stateCode = invoiceTempData.getBillstateCode();

        wender_state_code = CommonUtils.getPreferencesString(context, "Admin_status_code");

        System.out.println(wender_state_code);
        Log.e("TAGstateCode1", wender_state_code);
        Log.e("TAGstateCode2", stateCode);


        if (stateCode.equalsIgnoreCase("35") || stateCode.equalsIgnoreCase("04")
                || stateCode.equalsIgnoreCase("26")
                || stateCode.equalsIgnoreCase("25")
                || stateCode.equalsIgnoreCase("31")
                || stateCode.equalsIgnoreCase("34") && wender_state_code.equalsIgnoreCase("04")
                || wender_state_code.equalsIgnoreCase("26")
                || wender_state_code.equalsIgnoreCase("25")
                || wender_state_code.equalsIgnoreCase("31")
                || wender_state_code.equalsIgnoreCase("35")
                || wender_state_code.equalsIgnoreCase("34")
                ) {


            Log.e("try", "try");
            if (stateCode.equalsIgnoreCase(wender_state_code)) {
                tv_sgst.setText("UGST");
                tv_ugst.setText("SGST");

            } else {
                tv_sgst.setText("SGST");
            }

        } else if (stateCode.equalsIgnoreCase(wender_state_code)) {

            tv_sgst.setText("SGST");
            tv_ugst.setText("UGST");

        } else {

            tv_sgst.setText("SGST");


        }

        //  Log.e("listsize","list"+((CreateInvoiceActivity)getActivity()).billpaperList.size());


        //  Toast.makeText(context, "gst"+(CreateInvoiceActivity)getActivity()).billpaperList.size(), Toast.LENGTH_SHORT).show();


        logeedUserDetails = CommonUtils.getUserDetail(getActivity());
        appCompatSpinner2 = (HintSpinner) getView().findViewById(R.id.spinner2);


        invoiceTempData = InvoiceTempData.getInstance();
        //stateCode = invoiceTempData.getStateCode();

        createInvoicePresenter = new InvoicePresenterImpl(this, new InvoiceInteracotrImpl());

        apiInterface = APIClient.getClient().create(ApiInterface.class);

        progressDailog = new ProgressDialog(getActivity());
        progressDailog.setMessage("Please wait...");

        taxCat = new ArrayList<>();
        taxCat.add("28%");
        taxCat.add("18%");
        taxCat.add("12%");
        taxCat.add("5%");
        // Toast.makeText(context, "just check", Toast.LENGTH_SHORT).show();
        // Toast.makeText(getActivity(), "Invoice No : " + invoiceTempData.getInvoiceNo(), Toast.LENGTH_SHORT).show();
        LogeedUserDetails s = new LogeedUserDetails();
        s.getState_code();
  /*      preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        wender_state_code=preferences.getString("state_code","");
        wender_state_code=CommonUtils.getPreferencesString(getActivity(),"STATE_CODE");*/
        wender_state_code = CommonUtils.getPreferencesString(context, "Admin_status_code");
        wender_state = CommonUtils.getPreferencesString(context, "Admin_state");
        System.out.println(wender_state_code);
        Log.e("TAG", wender_state_code);
        //ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, taxCat);
        //appCompatSpinner2.setAdapter(arrayAdapter2);


        ArrayAdapter adapter1 = new ArrayAdapter(context, R.layout.custom_spinner_layout, R.id.tvSpinner, taxCat);
        adapter1.setDropDownViewResource(R.layout.custom_spinner_layout);
        appCompatSpinner2.setAdapter(adapter1);


        appCompatSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    CommonUtils.saveStringPreferences(context, "Count", String.valueOf(count));


                    IGSTEdit.setText("");
                    CGSTEdit.setText("");
                    SGSTEdit.setText("");
                    CGSTEdit.setText("");

                    CGSTAmountEdit.setText("");
                    SGSTAmountEdit.setText("");
                    UGSTAmountEdit.setText("");
                    IGSTAmountEdit.setText("");

                    selectedCatgeory = appCompatSpinner2.getSelectedItem().toString().replace("%", "");
                    selectedCatgeoryPer = appCompatSpinner2.getSelectedItem().toString();
                    selectedValue = Float.parseFloat(selectedCatgeory);
                    invoiceTempDataList = new InvoiceTempData();


                    invoiceTempDataList.setRate(amountEdit.getText().toString());

                    invoiceTempDataList.setAmount(amountEdit.getText().toString());

                    invoiceTempDataList.setProductDesc(invoiceTempData.getProductDesc());
                    invoiceTempDataList.setSacOrHas(invoiceTempData.getSacOrHas());
                    invoiceTempDataList.setQuantity(invoiceTempData.getQuantity());


                    invoiceTempData.setSelectedCatgeory(selectedCatgeory);


                    invoiceTempDataList.setSelectedCatgeory(selectedCatgeory);
                    try {
                        if (amountEdit.getText() != null && (!amountEdit.getText().equals(""))) {
                            taxrate = Double.valueOf(amountEdit.getText().toString()) * Double.valueOf(invoiceTempData.getQuantity());

                            //invoiceTempDataList.setTaxAmount(taxrate + "");

                            invoiceTempDataList.setPertotalamount(taxrate + "");
                            invoiceTempDataList.setTotalAmount(taxrate + "");
                            // invoiceTempDataList.setRatewithQtyAmt(taxrate + "");
                            // invoiceTempDataList.setAmount(taxrate + "");


                            // invoiceTempData.setTotalAmount(taxrate + "");
                            // invoiceTempData.setPertotalamount(taxrate + "");

                            calculateAmount(selectedValue);
                        }


                        } catch(NumberFormatException e){
                            e.printStackTrace();
                        }




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        preview_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (appCompatSpinner2.getSelectedItem() == null) {

                    CommonUtils.snackBar("Please Choose Tax Category", amountEdit, "300");
                } else {

                    showBitmap();
                }

            }
        });


        add_More_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btn_done.setVisibility(View.VISIBLE);



                amountEdit.setText("");
                IGSTEdit.setText("");
                IGSTAmountEdit.setText("");
                UGSTEdit.setText("");
                UGSTAmountEdit.setText("");
                CGSTAmountEdit.setText("");
                SGSTAmountEdit.setText("");


                ArrayAdapter adapter1 = new ArrayAdapter(context, R.layout.custom_spinner_layout, R.id.tvSpinner, taxCat);
                adapter1.setDropDownViewResource(R.layout.custom_spinner_layout);
                appCompatSpinner2.setAdapter(adapter1);
                mStepperLayout.setCurrentStepPosition(1);

                Product_Details.product_desc.setText("");
                Product_Details.sac_HSN.setText("");
                Product_Details.quantity.setText("");

                IGSTEdit.setText("");
                CGSTEdit.setText("");
                SGSTEdit.setText("");
                CGSTEdit.setText("");
                SGSTAmountEdit.setText("");
                CGSTAmountEdit.setText("");
                UGSTAmountEdit.setText("");
                IGSTAmountEdit.setText("");


            }
        });


        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                count++;

                //  btn_add_more.setText("Add More");
                CommonUtils.saveStringPreferences(context, "Count", String.valueOf(count));


                if (amountEdit.length() == 0) {

                    CommonUtils.snackBar("Please enter amount", amountEdit, "300");

                } else if (appCompatSpinner2.getSelectedItem() == null) {

                    CommonUtils.snackBar("Please Choose Tax Category", amountEdit, "300");
                } else {


                    if (CommonUtils.getPreferencesString(context, "Count") != null) {
                        invoiceTempDataList.setSerialNumber(CommonUtils.getPreferencesString(context, "Count"));
                    }


                    ShowAlertDialog();


                }

                //((CreateInvoiceActivity)getActivity()).callProductDetaisl();
            }
        });

    }


    private void ShowAlertDialog() {


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);


        alertDialog.setMessage("         Are  you sure  add this  product?");
        alertDialog.setCancelable(false);


        alertDialog.setPositiveButton("      YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                btn_done.setVisibility(View.GONE);

        add_More_button.setVisibility(View.VISIBLE);
                preview_button.setVisibility(View.VISIBLE);

                billList.add(invoiceTempDataList);
                // AllbillListGST.add(invoiceTempDataList);


                if (stateCode.equalsIgnoreCase("35") || stateCode.equalsIgnoreCase("04")
                        || stateCode.equalsIgnoreCase("26")
                        || stateCode.equalsIgnoreCase("25")
                        || stateCode.equalsIgnoreCase("31")
                        || stateCode.equalsIgnoreCase("34") &&
                        wender_state_code.equalsIgnoreCase("04")
                        || wender_state_code.equalsIgnoreCase("26")
                        || wender_state_code.equalsIgnoreCase("25")
                        || wender_state_code.equalsIgnoreCase("31")
                        || wender_state_code.equalsIgnoreCase("34")
                        || wender_state_code.equalsIgnoreCase("35")
                        ) {


                    if (stateCode.equals(wender_state_code)) {





                       /* invoiceTempDataList.setCGSTAmount(String.valueOf(totalUgst));
                        invoiceTempDataList.setSGSTAmount(String.valueOf(totalSgst));
                        AllbillListGST.add(invoiceTempDataList);*/
                        AllbillListGST.add(invoiceTempDataList);
                        billListGST = performCountingCGSTUGST(AllbillListGST);


                    } else {

                        AllbillListGST.add(invoiceTempDataList);
                        billListGST = performCountingIGST(AllbillListGST);


                    }
                } else if (stateCode.equalsIgnoreCase(wender_state_code)) {

                    AllbillListGST.add(invoiceTempDataList);
                    billListGST = performCountingCGSTSGST(AllbillListGST);


                } else {


                    AllbillListGST.add(invoiceTempDataList);
                    billListGST = performCountingIGST(AllbillListGST);

                }


            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();


            }
        });
        alertDialog.show();

    }

    Double totalIGSTValue = 0.0;

    public List<InvoiceTempData> performCountingIGST(List<InvoiceTempData> dataList) {

        String previousString = null;
        String previousStringData = null;
        totalIGSTValue = 0.0;

        List<InvoiceTempData> gstList=new ArrayList<>();
        for(InvoiceTempData data:dataList){
            gstList.add(data.clone());
        }

        Log.e("app",gstList.size()+"");
        Collections.sort(gstList);

        List<InvoiceTempData> filteredList = new ArrayList<>();
        for (InvoiceTempData gst : gstList) {
            String filteredString = gst.getSelectedCatgeory().replace("%", "");
            gst.setSelectedCatgeory(filteredString);
            if (gst.getIGSTAmountN() != null) {
                gst.setIGSTAmountN(gst.getIGSTAmountN().replace("", ""));
//                Log.e("unaltered", gst.getIGSTAmountN());
            }

            String tempValue = "";
            String mount = "";
            boolean isComplex = false;
            if (gst.getSelectedCatgeory().contains("+")) {
                String[] list = gst.getSelectedCatgeory().split("\\+");
                if (list.length > 0) {
                    tempValue = list[0];
                    isComplex = true;
                }
            }

            if (gst.getIGSTAmountN().contains("+")) {
                String[] list = gst.getIGSTAmountN().split("\\+");
                if (list.length > 0) {
                    mount = list[0];
                    isComplex = true;
                }
            } else {
                tempValue = gst.getSelectedCatgeory();
                mount = gst.getIGSTAmountN();
            }

            try {
                totalIGSTValue += Double.parseDouble(mount);
                /*double percent=Double.parseDouble(tempValue);
                double cost=Double.parseDouble(gst.getPertotalamount());
                double currentGstCost=(cost*percent)/100;
                Log.e("current",cost+" "+percent+"% ="+currentGstCost);
                totalIGSTValue+=currentGstCost;*/
            } catch (Exception e) {
                Log.e("error", "error calculating gst " + mount);
            }

//            Log.e("unaltered", gst.getSelectedCatgeory());
            if (previousString == null) {
                previousString = tempValue;
                previousStringData = mount;
                gst.setSelectedCatgeory(previousString + "%"/*gst.getSelectedCatgeory() + (isComplex ? "" : "%")*/);
                gst.setIGSTAmountN(gst.getIGSTAmountN() + (isComplex ? "" : ""));
//                Log.e("unaltered", gst.getIGSTAmount());
                filteredList.add(gst);
            } else {
                if (previousString.equals(tempValue)) {
                    InvoiceTempData last = filteredList.get(filteredList.size() - 1);
                    last.setSelectedCatgeory(last.getSelectedCatgeory() + "+" + previousString + "%");

                    String[] data = previousString.split("\\+");
                    String First = data[0];
                    // String Second=data[1];

                    //  last.setIGSTAmount(String.valueOf(Double.valueOf(last.getIGSTAmount()) +  Double.valueOf(previousStringData)));
                    last.setIGSTAmountN(last.getIGSTAmountN() + "+" + mount + "");
//                    Log.e("unaltered", gst.getIGSTAmount());

                } else {
                    previousString = tempValue;
                    previousStringData = mount;
                    gst.setSelectedCatgeory(previousString + "%"/*gst.getSelectedCatgeory() + (isComplex ? "" : "%")*/);
                    gst.setIGSTAmountN(gst.getIGSTAmountN() + (isComplex ? "" : ""));
//                    Log.e("unaltered", gst.getIGSTAmount());


                    filteredList.add(gst);
                }
            }
        }
        for (InvoiceTempData data : filteredList) {
            Log.e("with filter", data.getSelectedCatgeory());

        }

        Log.e("totalIGSTAbul", totalIGSTValue + "");

        return filteredList;
    }


    public List<InvoiceTempData> performCountingCGSTSGST(List<InvoiceTempData> dataList) {

        String previousString = null;
        String previousStringData = null;
        totalIGSTValue = 0.0;

        List<InvoiceTempData> gstList=new ArrayList<>();
        for(InvoiceTempData data:dataList){
            gstList.add(data.clone());
        }

        Log.e("app",gstList.size()+"");
        Collections.sort(gstList);

        List<InvoiceTempData> filteredList = new ArrayList<>();
        for (InvoiceTempData gst : gstList) {
            String filteredString = gst.getSelectedCatgeory().replace("%", "");
            gst.setSelectedCatgeory(filteredString);
            if (gst.getSGSTAmountN() != null) {
                gst.setSGSTAmountN(gst.getSGSTAmountN().replace("", ""));
                gst.setCGSTAmountN(gst.getCGSTAmountN().replace("", ""));


//                Log.e("unaltered", gst.getIGSTAmountN());
            }

            String tempValue = "";
            String mount = "";
            boolean isComplex = false;
            if (gst.getSelectedCatgeory().contains("+")) {
                String[] list = gst.getSelectedCatgeory().split("\\+");
                if (list.length > 0) {
                    tempValue = list[0];
                    isComplex = true;
                }
            }

            if (gst.getSGSTAmountN().contains("+")) {
                String[] list = gst.getSGSTAmountN().split("\\+");
                if (list.length > 0) {
                    mount = list[0];
                    isComplex = true;
                }
            } else {
                tempValue = gst.getSelectedCatgeory();
                mount = gst.getSGSTAmountN();
            }

            try {
                totalIGSTValue += Double.parseDouble(mount);
                /*double percent=Double.parseDouble(tempValue);
                double cost=Double.parseDouble(gst.getPertotalamount());
                double currentGstCost=(cost*percent)/100;
                Log.e("current",cost+" "+percent+"% ="+currentGstCost);
                totalIGSTValue+=currentGstCost;*/
            } catch (Exception e) {
                Log.e("error", "error calculating gst " + mount);
            }

//            Log.e("unaltered", gst.getSelectedCatgeory());
            if (previousString == null) {
                previousString = tempValue;
                previousStringData = mount;
                gst.setSelectedCatgeory(previousString + "%"/*gst.getSelectedCatgeory() + (isComplex ? "" : "%")*/);
                gst.setSGSTAmountN(gst.getSGSTAmountN() + (isComplex ? "" : ""));
                gst.setCGSTAmountN(gst.getCGSTAmountN() + (isComplex ? "" : ""));


//                Log.e("unaltered", gst.getIGSTAmount());
                filteredList.add(gst);
            } else {
                if (previousString.equals(tempValue)) {
                    InvoiceTempData last = filteredList.get(filteredList.size() - 1);
                    last.setSelectedCatgeory(last.getSelectedCatgeory() + "+" + previousString + "%");

                    String[] data = previousString.split("\\+");
                    String First = data[0];
                    // String Second=data[1];

                    //  last.setIGSTAmount(String.valueOf(Double.valueOf(last.getIGSTAmount()) +  Double.valueOf(previousStringData)));
                    last.setSGSTAmountN(last.getSGSTAmountN() + "+" + mount + "");
                    last.setCGSTAmountN(last.getCGSTAmountN() + "+" + mount + "");
//                    Log.e("unaltered", gst.getIGSTAmount());

                } else {
                    previousString = tempValue;
                    previousStringData = mount;
                    gst.setSelectedCatgeory(previousString + "%"/*gst.getSelectedCatgeory() + (isComplex ? "" : "%")*/);
                    gst.setSGSTAmountN(gst.getSGSTAmountN() + (isComplex ? "" : ""));
                    gst.setCGSTAmountN(gst.getCGSTAmountN() + (isComplex ? "" : ""));
//                    Log.e("unaltered", gst.getIGSTAmount());


                    filteredList.add(gst);
                }
            }
        }
        for (InvoiceTempData data : filteredList) {
            Log.e("with filter", data.getSelectedCatgeory());

        }

        Log.e("totalIGSTAbul", totalIGSTValue + "");

        return filteredList;
    }

    public List<InvoiceTempData> performCountingCGSTUGST(List<InvoiceTempData> dataList) {

        String previousString = null;
        String previousStringData = null;
        totalIGSTValue = 0.0;

        List<InvoiceTempData> gstList=new ArrayList<>();
        for(InvoiceTempData data:dataList){
            gstList.add(data.clone());
        }

        Log.e("app",gstList.size()+"");
        Collections.sort(gstList);

        List<InvoiceTempData> filteredList = new ArrayList<>();
        for (InvoiceTempData gst : gstList) {
            String filteredString = gst.getSelectedCatgeory().replace("%", "");
            gst.setSelectedCatgeory(filteredString);
            if (gst.getUGSTAmountN() != null) {
                gst.setUGSTAmountN(gst.getUGSTAmountN().replace("", ""));
                gst.setCGSTAmountN(gst.getCGSTAmountN().replace("", ""));


//                Log.e("unaltered", gst.getIGSTAmountN());
            }

            String tempValue = "";
            String mount = "";
            boolean isComplex = false;
            if (gst.getSelectedCatgeory().contains("+")) {
                String[] list = gst.getSelectedCatgeory().split("\\+");
                if (list.length > 0) {
                    tempValue = list[0];
                    isComplex = true;
                }
            }

            if (gst.getUGSTAmountN().contains("+")) {
                String[] list = gst.getUGSTAmountN().split("\\+");
                if (list.length > 0) {
                    mount = list[0];
                    isComplex = true;
                }
            } else {
                tempValue = gst.getSelectedCatgeory();
                mount = gst.getUGSTAmountN();
            }

            try {
                totalIGSTValue += Double.parseDouble(mount);
                /*double percent=Double.parseDouble(tempValue);
                double cost=Double.parseDouble(gst.getPertotalamount());
                double currentGstCost=(cost*percent)/100;
                Log.e("current",cost+" "+percent+"% ="+currentGstCost);
                totalIGSTValue+=currentGstCost;*/
            } catch (Exception e) {
                Log.e("error", "error calculating gst " + mount);
            }

//            Log.e("unaltered", gst.getSelectedCatgeory());
            if (previousString == null) {
                previousString = tempValue;
                previousStringData = mount;
                gst.setSelectedCatgeory(previousString + "%"/*gst.getSelectedCatgeory() + (isComplex ? "" : "%")*/);
                gst.setUGSTAmountN(gst.getUGSTAmountN() + (isComplex ? "" : ""));
                gst.setCGSTAmountN(gst.getCGSTAmountN() + (isComplex ? "" : ""));
//                Log.e("unaltered", gst.getIGSTAmount());
                filteredList.add(gst);
            } else {
                if (previousString.equals(tempValue)) {
                    InvoiceTempData last = filteredList.get(filteredList.size() - 1);
                    last.setSelectedCatgeory(last.getSelectedCatgeory() + "+" + previousString + "%");

                    String[] data = previousString.split("\\+");
                    String First = data[0];
                    // String Second=data[1];

                    //  last.setIGSTAmount(String.valueOf(Double.valueOf(last.getIGSTAmount()) +  Double.valueOf(previousStringData)));
                    last.setUGSTAmountN(last.getUGSTAmountN() + "+" + mount + "");
                    last.setCGSTAmountN(last.getCGSTAmountN() + "+" + mount + "");
//                    Log.e("unaltered", gst.getIGSTAmount());

                } else {
                    previousString = tempValue;
                    previousStringData = mount;
                    gst.setSelectedCatgeory(previousString + "%"/*gst.getSelectedCatgeory() + (isComplex ? "" : "%")*/);
                    gst.setUGSTAmountN(gst.getUGSTAmountN() + (isComplex ? "" : ""));
                    gst.setSGSTAmountN(gst.getSGSTAmountN() + (isComplex ? "" : ""));
//                    Log.e("unaltered", gst.getIGSTAmount());


                    filteredList.add(gst);
                }
            }
        }
        for (InvoiceTempData data : filteredList) {
            Log.e("with filter", data.getSelectedCatgeory());

        }

        Log.e("totalIGSTAbul", totalIGSTValue + "");

        return filteredList;
    }





    public List<InvoiceTempData> performCountingCGSTSGST1(List<InvoiceTempData> gstList) {


        String previousString = null;
        String previousStringData = null;
        totalIGSTValue = 0.0;


        Collections.sort(gstList);

        List<InvoiceTempData> filteredList = new ArrayList<>();
        for (InvoiceTempData gst : gstList) {
            String filteredString = gst.getSelectedCatgeory().replace("%", "");
            gst.setSelectedCatgeory(filteredString);

            if (gst.getCGSTAmountN() != null) {
                gst.setCGSTAmountN(gst.getCGSTAmountN().replace("", ""));
            }

            if (gst.getSGSTAmountN() != null) {
                gst.setSGSTAmountN(gst.getSGSTAmountN().replace("", ""));
            }


            String tempValue = "";
            String mount = "";
            boolean isComplex = false;
            if (gst.getSelectedCatgeory().contains("+")) {
                String[] list = gst.getSelectedCatgeory().split("\\+");
                if (list.length > 0) {
                    tempValue = list[0];
                    isComplex = true;
                }
            }

            if (gst.getCGSTAmountN().contains("+")) {
                String[] list = gst.getCGSTAmountN().split("\\+");
                if (list.length > 0) {
                    mount = list[0];
                    isComplex = true;
                }
            } else {
                tempValue = gst.getSelectedCatgeory();
                mount = gst.getCGSTAmountN();
            }

            if (gst.getSGSTAmountN().contains("+")) {
                String[] list = gst.getSGSTAmountN().split("\\+");
                if (list.length > 0) {
                    mount = list[0];
                    isComplex = true;
                }
            } else {
                tempValue = gst.getSelectedCatgeory();
                mount = gst.getSGSTAmountN();
            }


            try {
                totalIGSTValue += Double.parseDouble(mount);
                /*double percent=Double.parseDouble(tempValue);
                double cost=Double.parseDouble(gst.getPertotalamount());
                double currentGstCost=(cost*percent)/100;
                Log.e("current",cost+" "+percent+"% ="+currentGstCost);
                totalIGSTValue+=currentGstCost;*/
            } catch (Exception e) {
                Log.e("error", "error calculating gst " + mount);
            }

//            Log.e("unaltered", gst.getSelectedCatgeory());
            if (previousString == null) {
                previousString = tempValue;
                previousStringData = mount;
                gst.setSelectedCatgeory(previousString + "%"/*gst.getSelectedCatgeory() + (isComplex ? "" : "%")*/);
                gst.setCGSTAmountN(gst.getCGSTAmountN() + (isComplex ? "" : ""));
                gst.setSGSTAmountN(gst.getSGSTAmountN() + (isComplex ? "" : ""));


//                Log.e("unaltered", gst.getIGSTAmount());
                filteredList.add(gst);
            } else {
                if (previousString.equals(tempValue)) {
                    InvoiceTempData last = filteredList.get(filteredList.size() - 1);
                    last.setSelectedCatgeory(last.getSelectedCatgeory() + "+" + previousString + "%");

                    String[] data = previousString.split("\\+");
                    String First = data[0];
                    // String Second=data[1];

                    //  last.setIGSTAmount(String.valueOf(Double.valueOf(last.getIGSTAmount()) +  Double.valueOf(previousStringData)));
                    last.setCGSTAmountN(last.getCGSTAmountN() + "+" + mount + "");
                    last.setSGSTAmountN(last.getSGSTAmountN() + "+" + mount + "");
//                    Log.e("unaltered", gst.getIGSTAmount());

                } else {
                    previousString = tempValue;
                    previousStringData = mount;
                    gst.setSelectedCatgeory(previousString + "%"/*gst.getSelectedCatgeory() + (isComplex ? "" : "%")*/);
                    gst.setSGSTAmountN(gst.getSGSTAmountN() + (isComplex ? "" : ""));
                    gst.setCGSTAmountN(gst.getCGSTAmountN() + (isComplex ? "" : ""));
//                    Log.e("unaltered", gst.getIGSTAmount());


                    filteredList.add(gst);
                }
            }
        }
        for (InvoiceTempData data : filteredList) {
            Log.e("with filter", data.getSelectedCatgeory());

        }

        Log.e("totalCGSTSGSTAbul", totalIGSTValue + "");

        return filteredList;
    }


    public List<InvoiceTempData> performCountingCGSTUGST1(List<InvoiceTempData> gstList) {

        String previousString = null;
        String previousStringData = null;
        totalIGSTValue = 0.0;

        Collections.sort(gstList);

        List<InvoiceTempData> filteredList = new ArrayList<>();
        for (InvoiceTempData gst : gstList) {
            String filteredString = gst.getSelectedCatgeory().replace("%", "");
            gst.setSelectedCatgeory(filteredString);

            if (gst.getCGSTAmountN() != null) {
                gst.setCGSTAmountN(gst.getCGSTAmountN().replace("", ""));
            }

            if (gst.getUGSTAmountN() != null) {
                gst.setUGSTAmountN(gst.getUGSTAmountN().replace("", ""));
            }


            String tempValue = "";
            String mount = "";
            boolean isComplex = false;
            if (gst.getSelectedCatgeory().contains("+")) {
                String[] list = gst.getSelectedCatgeory().split("\\+");
                if (list.length > 0) {
                    tempValue = list[0];
                    isComplex = true;
                }
            }

            if (gst.getCGSTAmountN().contains("+")) {
                String[] list = gst.getCGSTAmountN().split("\\+");
                if (list.length > 0) {
                    mount = list[0];
                    isComplex = true;
                }
            } else {
                tempValue = gst.getSelectedCatgeory();
                mount = gst.getCGSTAmountN();
            }

            if (gst.getUGSTAmountN().contains("+")) {
                String[] list = gst.getUGSTAmountN().split("\\+");
                if (list.length > 0) {
                    mount = list[0];
                    isComplex = true;
                }
            } else {
                tempValue = gst.getSelectedCatgeory();
                mount = gst.getUGSTAmountN();
            }


            try {
                totalIGSTValue += Double.parseDouble(mount);
                /*double percent=Double.parseDouble(tempValue);
                double cost=Double.parseDouble(gst.getPertotalamount());
                double currentGstCost=(cost*percent)/100;
                Log.e("current",cost+" "+percent+"% ="+currentGstCost);
                totalIGSTValue+=currentGstCost;*/
            } catch (Exception e) {
                Log.e("error", "error calculating gst " + mount);
            }

//            Log.e("unaltered", gst.getSelectedCatgeory());
            if (previousString == null) {
                previousString = tempValue;
                previousStringData = mount;
                gst.setSelectedCatgeory(previousString + "%"/*gst.getSelectedCatgeory() + (isComplex ? "" : "%")*/);
                gst.setCGSTAmountN(gst.getCGSTAmountN() + (isComplex ? "" : ""));
                gst.setUGSTAmountN(gst.getUGSTAmountN() + (isComplex ? "" : ""));


//                Log.e("unaltered", gst.getIGSTAmount());
                filteredList.add(gst);
            } else {
                if (previousString.equals(tempValue)) {
                    InvoiceTempData last = filteredList.get(filteredList.size() - 1);
                    last.setSelectedCatgeory(last.getSelectedCatgeory() + "+" + previousString + "%");

                    String[] data = previousString.split("\\+");
                    String First = data[0];
                    // String Second=data[1];

                    //  last.setIGSTAmount(String.valueOf(Double.valueOf(last.getIGSTAmount()) +  Double.valueOf(previousStringData)));
                    last.setCGSTAmountN(last.getCGSTAmountN() + "+" + mount + "");
                    last.setUGSTAmountN(last.getUGSTAmountN() + "+" + mount + "");
//                    Log.e("unaltered", gst.getIGSTAmount());

                } else {
                    previousString = tempValue;
                    previousStringData = mount;
                    gst.setSelectedCatgeory(previousString + "%"/*gst.getSelectedCatgeory() + (isComplex ? "" : "%")*/);
                    gst.setUGSTAmountN(gst.getUGSTAmountN() + (isComplex ? "" : ""));
                    gst.setCGSTAmountN(gst.getCGSTAmountN() + (isComplex ? "" : ""));
//                    Log.e("unaltered", gst.getIGSTAmount());


                    filteredList.add(gst);
                }
            }
        }
        for (InvoiceTempData data : filteredList) {
            Log.e("with filter", data.getSelectedCatgeory());

        }

        Log.e("totalCGSTSGSTAbul", totalIGSTValue + "");

        return filteredList;
    }


    @UiThread
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        // Toast.makeText(this.getContext(), "Your custom back action. Here you should cancel currently running operations", Toast.LENGTH_SHORT).show();
        callback.goToPrevStep();
    }


    private void showBitmap() {
        LayoutInflater inflater = (LayoutInflater)
                getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        content = inflater.inflate(R.layout.pdf_layout, null);

        RecyclerView recyclerView = content.findViewById(R.id.recyclerview);
        RecyclerView recy_tax = content.findViewById(R.id.recy_tax);
        if (billList != null && billList.size() > 0) {
            Log.e("listsize", "list" + billList.size());
            invoiceGenratorAdapter = new InvoiceGenratorAdapter(billList,context);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(invoiceGenratorAdapter);


            TaxSlabAdapter slabAdapter = new TaxSlabAdapter(billListGST, context, stateCode);
            LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(context);
            recy_tax.setLayoutManager(mLayoutManager1);
            recy_tax.setItemAnimator(new DefaultItemAnimator());
            recy_tax.setAdapter(slabAdapter);
        }
        TextView PdfComName = (TextView) content.findViewById(R.id.pdf_come_name);
        TextView pdf_vehicle_no = (TextView) content.findViewById(R.id.pdf_vehicle_no);
        ImageView pdf_logo = (ImageView) content.findViewById(R.id.pdf_logo);
        TextView PdfComAdd = (TextView) content.findViewById(R.id.pdf_com_address);
        TextView PdfInvoiceNo = (TextView) content.findViewById(R.id.pdf_invoice);
        TextView PdfDate = (TextView) content.findViewById(R.id.pdf_date);
        TextView total_text = (TextView) content.findViewById(R.id.total_text);
        TextView total_text2 = (TextView) content.findViewById(R.id.total_text2);

        TextView pdf_gstn = (TextView) content.findViewById(R.id.pdf_gstn);
        TextView pdf_cin = (TextView) content.findViewById(R.id.pdf_cin);
        TextView pdf_pan = (TextView) content.findViewById(R.id.pdf_pan);
        TextView pdf_state_code = (TextView) content.findViewById(R.id.pdf_state_code);
        TextView pdf_state = (TextView) content.findViewById(R.id.pdf_state);
        TextView pdf_cust_name = (TextView) content.findViewById(R.id.pdf_cust_name);
        TextView bottom_com_name = (TextView) content.findViewById(R.id.bottom_com_name);


        TextView tv_ship_pdf_cust_add1 = (TextView) content.findViewById(R.id.tv_ship_pdf_cust_add1);
        TextView tv_ship_pdf_cust_state = (TextView) content.findViewById(R.id.tv_ship_pdf_cust_state);
        TextView tv_ship_pdf_cust_stateCode = (TextView) content.findViewById(R.id.tv_ship_pdf_cust_stateCode);
        TextView tv_ship_pdf_cust_GSTN = (TextView) content.findViewById(R.id.tv_ship_pdf_cust_GSTN);


        //............set Bank Details..............................................................
        TextView ac_name_pdf = (TextView) content.findViewById(R.id.ac_name_pdf);
        TextView bnkName_pdf = (TextView) content.findViewById(R.id.bnkName_pdf);
        TextView bank_branch_pdf = (TextView) content.findViewById(R.id.bank_branch_pdf);
        TextView AC_no_pdf = (TextView) content.findViewById(R.id.AC_no_pdf);
        TextView IFSC_pdf = (TextView) content.findViewById(R.id.IFSC_pdf);
        TextView ac_type_pdf = (TextView) content.findViewById(R.id.ac_type_pdf);

        ac_name_pdf.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getLName());
        bnkName_pdf.setText(logeedUserDetails.getBank_name());
        bank_branch_pdf.setText(logeedUserDetails.getBank_branch());
        AC_no_pdf.setText(logeedUserDetails.getAc_no());
        IFSC_pdf.setText(logeedUserDetails.getIfsc_code());
        ac_type_pdf.setText(logeedUserDetails.getAc_type());


        TextView pdf_cust_add1 = (TextView) content.findViewById(R.id.pdf_cust_add1);
        TextView tv_shipping_to = content.findViewById(R.id.tv_shipping_add);

        TextView pdf_cust_state = (TextView) content.findViewById(R.id.pdf_cust_state);
        TextView pdf_cust_stateCode = (TextView) content.findViewById(R.id.pdf_cust_stateCode);
        TextView pdf_cust_GSTN = (TextView) content.findViewById(R.id.pdf_cust_GSTN);
        TextView service_desc = (TextView) content.findViewById(R.id.service_desc);
        TextView hsn_sas_code = (TextView) content.findViewById(R.id.hsn_sas_code);
        TextView tex_amount_pdf = (TextView) content.findViewById(R.id.tex_amount_pdf);
        TextView total_amnt_pdf = (TextView) content.findViewById(R.id.total_amnt_pdf);
        TextView grand_total = (TextView) content.findViewById(R.id.grand_total);
        TextView tv_rupee_grand_total = (TextView) content.findViewById(R.id.tv_rupee_grand_total);

        Double grandAmount = 0.0;
        Double totalAmount = 0.0;
        Double pertoatgst = 0.0;


        if (billListGST != null && billListGST.size() > 0) {


            for (int i = 0; i < billList.size(); i++) {


                if (billList.get(i).getPertotalamount() != null && (!billList.get(i).getPertotalamount().equals(""))) {
                    if (i == 0) {
                        totalAmount = Double.valueOf((billList.get(0).getPertotalamount()));
                    } else {
                        //  totalAmount=totalsGst.pl
                        totalAmount = totalAmount + Double.valueOf(billList.get(i).getPertotalamount());
                        ;
                    }
                }
            }

            if (String.valueOf(totalAmount).length() == 5 || String.valueOf(totalAmount).length() > 5) {

                invoiceTempData.setRatewithQtyAmt(String.valueOf(totalAmount).substring(0, 5));


            } else {
                invoiceTempData.setRatewithQtyAmt(String.valueOf(totalAmount));


            }


        }

        //grandAmount = totalIGSTValue + totalAmount;


        if (stateCode.equalsIgnoreCase("35") || stateCode.equalsIgnoreCase("04")
                || stateCode.equalsIgnoreCase("26")
                || stateCode.equalsIgnoreCase("25")
                || stateCode.equalsIgnoreCase("31")
                || stateCode.equalsIgnoreCase("34") &&
                wender_state_code.equalsIgnoreCase("04")
                || wender_state_code.equalsIgnoreCase("26")
                || wender_state_code.equalsIgnoreCase("25")
                || wender_state_code.equalsIgnoreCase("31")
                || wender_state_code.equalsIgnoreCase("34")
                || wender_state_code.equalsIgnoreCase("35")
                ) {


            if (stateCode.equals(wender_state_code)) {

                billListGST = performCountingCGSTUGST(AllbillListGST);

                if (String.valueOf(totalIGSTValue).length() >= 5) {


                    total_text.setText("CGST : ₹" + String.valueOf(totalIGSTValue).substring(0, 5) + "");
                    total_text2.setVisibility(View.VISIBLE);

                    total_text2.setText("UGST :₹ " + String.valueOf(totalIGSTValue).substring(0, 5) + "");

                    totalIGSTValue = 2 * totalIGSTValue;
                    grandAmount = totalIGSTValue + totalAmount;

                    invoiceTempData.setTotalGst(String.valueOf(totalIGSTValue));

                    //  invoiceTempData.setTaxAmount(String.valueOf(totalIGSTValue).substring(0,5));
                } else {


                    total_text.setText("CGST : ₹" + totalIGSTValue + "");

                    total_text2.setVisibility(View.VISIBLE);

                    total_text2.setText("UGST : ₹" + totalIGSTValue + "");
                    totalIGSTValue = 2 * totalIGSTValue;
                    grandAmount = totalIGSTValue + totalAmount;
                    invoiceTempData.setTotalGst(String.valueOf(totalIGSTValue));

                    // invoiceTempData.setTaxAmount(String.valueOf(totalIGSTValue));
                }


            } else {

                if (String.valueOf(totalIGSTValue).length() >= 5) {
                    grandAmount = totalIGSTValue + totalAmount;


                    total_text.setText("IGST :₹ " + String.valueOf(totalIGSTValue).substring(0, 5) + "");
                    invoiceTempData.setTotalGst(String.valueOf(totalIGSTValue));

                    //  invoiceTempData.setTaxAmount(String.valueOf(totalIGSTValue).substring(0,5));
                } else {
                    grandAmount = totalIGSTValue + totalAmount;

                    total_text.setText("IGST : ₹" + totalIGSTValue + "");
                    invoiceTempData.setTotalGst(String.valueOf(totalIGSTValue));

                    // invoiceTempData.setTaxAmount(String.valueOf(totalIGSTValue));
                }


            }
        } else if (stateCode.equalsIgnoreCase(wender_state_code)) {

            billListGST = performCountingCGSTSGST(AllbillListGST);


            if (String.valueOf(totalIGSTValue).length() >= 5) {


                total_text.setText("CGST : ₹" + String.valueOf(totalIGSTValue).substring(0, 5) + "");

                total_text2.setVisibility(View.VISIBLE);

                total_text2.setText("SGST : ₹" + String.valueOf(totalIGSTValue).substring(0, 5) + "");
                totalIGSTValue = 2 * totalIGSTValue;
                grandAmount = totalIGSTValue + totalAmount;
                invoiceTempData.setTotalGst(String.valueOf(totalIGSTValue));

                //  invoiceTempData.setTaxAmount(String.valueOf(totalIGSTValue).substring(0,5));
            } else {

                total_text2.setVisibility(View.VISIBLE);

                total_text2.setText("CGST : ₹ " + totalIGSTValue + "");


                total_text.setText("SGST : ₹ " + totalIGSTValue + "");


                if (String.valueOf(totalIGSTValue).length() >= 5) {

                    totalIGSTValue = 2 * totalIGSTValue;
                    grandAmount = totalIGSTValue + totalAmount;
                    invoiceTempData.setTotalGst(String.valueOf(totalIGSTValue).substring(0, 5));


                } else {

                    totalIGSTValue = 2 * totalIGSTValue;
                    grandAmount = totalIGSTValue + totalAmount;
                    invoiceTempData.setTotalGst(String.valueOf(totalIGSTValue));


                }// invoiceTempData.setTaxAmount(String.valueOf(totalIGSTValue));
            }


        } else {


            if (String.valueOf(totalIGSTValue).length() >= 5) {
                grandAmount = totalIGSTValue + totalAmount;



                total_text.setText("IGST : ₹ " + String.valueOf(totalIGSTValue).substring(0, 5) + "");
                invoiceTempData.setTotalGst(String.valueOf(totalIGSTValue));

                //  invoiceTempData.setTaxAmount(String.valueOf(totalIGSTValue).substring(0,5));
            } else {
                grandAmount = totalIGSTValue + totalAmount;

                total_text.setText("IGST : ₹" + totalIGSTValue + "");
                invoiceTempData.setTotalGst(String.valueOf(totalIGSTValue));

                // invoiceTempData.setTaxAmount(String.valueOf(totalIGSTValue));
            }

        }



        if(!TextUtils.isEmpty(String.valueOf(grandAmount))) {

            if(String.valueOf(grandAmount).length()>=6){
                grand_total.setTextSize(10);
                tv_rupee_grand_total.setTextSize(10);
                grand_total.setText(String.valueOf(grandAmount));
                invoiceTempData.setTotalAmount(String.valueOf(grandAmount));

            }
            else {
                grand_total.setText(String.valueOf(grandAmount));
                invoiceTempData.setTotalAmount(String.valueOf(grandAmount));

            }
        }

       /* if (String.valueOf(grandAmount).length() == 5 || String.valueOf(grandAmount).length() > 5) {
            grand_total.setText( String.valueOf(grandAmount).substring(0, 5));
            grand_total.setTextSize(8);
            invoiceTempData.setTotalAmount(String.valueOf(grandAmount).substring(0, 5) + "");


        } else {

            grand_total.setText( String.valueOf(grandAmount));
            invoiceTempData.setTotalAmount(grandAmount + "");
        }*/


        // tyigjhj
        total_amnt_pdf.setText("Rs." + totalAmount + "");

        PdfComName.setText(logeedUserDetails.getCompany_name());
        PdfComAdd.setText(logeedUserDetails.getAddress());
        PdfInvoiceNo.setText("Invoice No : \n" + invoiceTempData.getInvoiceNo());
        PdfDate.setText(currentDate);
        pdf_gstn.setText( logeedUserDetails.getGstn());
        pdf_cin.setText( logeedUserDetails.getCin());
        pdf_pan.setText( logeedUserDetails.getPan());
       // pdf_state_code.setText(logeedUserDetails.getState_code());
        pdf_state_code.setText(logeedUserDetails.getState_code());
       // pdf_state.setText( logeedUserDetails.getState());
        pdf_state.setText(logeedUserDetails.getState());


        bottom_com_name.setText(logeedUserDetails.getCompany_name());


      /*  Glide.with(context)
                .load(logeedUserDetails.getCompany_logo())
                .error(R.drawable.professional_logo)
                .into(pdf_logo)*/;


        pdf_cust_name.setText(invoiceTempData.getCustName());
        pdf_cust_add1.setText(invoiceTempData.getBillAddress());
        pdf_cust_state.setText("State : "+invoiceTempData.getBillcustState());
        pdf_cust_stateCode.setText("State Code :"+invoiceTempData.getBillstateCode());
        pdf_cust_GSTN.setText("Customer GSTIN :"+invoiceTempData.getCustGstn());


        tv_ship_pdf_cust_add1.setText(invoiceTempData.getCustAddress());
        tv_ship_pdf_cust_state.setText("State :"+invoiceTempData.getCustState());
        tv_ship_pdf_cust_stateCode.setText("State Code :"+invoiceTempData.getStateCode());

        tv_ship_pdf_cust_GSTN.setText("Customer GSTIN :"+invoiceTempData.getCustGstn());


        //  service_desc.setText(invoiceTempData.getProductDesc());
        // hsn_sas_code.setText(invoiceTempData.getSacOrHas());
        //tex_amount_pdf.setText(amountEdit.getText().toString());
        // total_amnt_pdf.setText(amountEdit.getText().toString());
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        shping_detail = new String();
        shping_detail = shping_detail.concat(preferences.getString("first_name", ""));
        preferences.getString("first_name", "");
        List<UserProfile> userProfiles;


        tv_shipping_to.setText(invoiceTempData.getBillAddress());
        pdf_vehicle_no.setText( invoiceTempData.getVehicle());

        /*igst_pdf.setText(totalTaxAmount);
        if (stateCode.equals("07")) {
            igst_infigure.setText("IGST Amount(In figure) : " + totalTaxAmount);
        } else {
            igst_infigure.setText("CGST Amount(In figure) : " + totalTaxAmount);
        }*/


        view = (LinearLayout) content.findViewById(R.id.bitmap_layout);
        view.setDrawingCacheEnabled(true);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache(true);


        Bitmap b = null;
        try {
            b = Bitmap.createBitmap(view.getDrawingCache());
        } catch (Exception e) {
            e.printStackTrace();
        }





        final Dialog dialog;
        dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bitmap_view);
        //dialog.getWindow().getAttributes().windowAnimations = animationdialog;
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ImageView imageView = (ImageView) dialog.findViewById(R.id.bitmap_image);
        Button submitForm = (Button) dialog.findViewById(R.id.submitForm);
        Button submitCancel = (Button) dialog.findViewById(R.id.submitCancel);
        imageView.setImageBitmap(b);
        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(imageView);
        pAttacher.update();
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        submitForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getCurrentData();

            }
        });

        submitCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();

      /* final Dialog dialog;
        dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.bitmap_view, null, false);
        ImageView imageView = (ImageView) view1.findViewById(R.id.bitmap_image);
        Button submitForm = (Button) view1.findViewById(R.id.submitForm);
        imageView.setImageBitmap(b);
        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(imageView);
        pAttacher.update();


    *//*  ViewGroup.LayoutParams layoutParams=(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.addContentView(view1, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);*//*

       // dialog.setView(view1);

        submitForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                getCurrentData();


            }
        });

        dialog.show();*/
        view.setDrawingCacheEnabled(false);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "v2i.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (Exception e) {
        }

    }


    private void getCurrentData() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(date);
        currentDate = formattedDate;

        SubmitDetails();
    }

    private void SubmitDetails() {

        //   billList.clear();
        // billList.clear();


        String id = CommonUtils.getUserId(getActivity());


        calculateValueforDetails();


        createInvoicePresenter.createBill(invoiceTempData, currentDate, id);

       /* HashMap<String, String> hashMap = new HashMap();
        hashMap.put("user_id", id);
        hashMap.put("name_of_supplier", invoiceTempData.getCustName());
        hashMap.put("gstin_uin", invoiceTempData.getCustGstn());
        hashMap.put("state_name", invoiceTempData.getCustState());
        hashMap.put("pos", invoiceTempData.getStateCode());
        hashMap.put("invoice_no", invoiceTempData.getInvoiceNo());
        hashMap.put("invoice_date", currentDate);
        hashMap.put("invoice_value", "20");
        hashMap.put("hsn_sac_code", invoiceTempData.getSacOrHas());
        hashMap.put("description", invoiceTempData.getProductDesc());
        hashMap.put("rate", selectedCatgeory);
        hashMap.put("invoice_taxable_amt", amountEdit.getText().toString());
        hashMap.put("qty", "1");
        hashMap.put("igst", IGSTAmountEdit.getText().toString());
        hashMap.put("igst_per", IGSTEdit.getText().toString().replace("%", ""));
        hashMap.put("cgst", CGSTAmountEdit.getText().toString());
        hashMap.put("cgst_per", CGSTEdit.getText().toString().replace("%", ""));
        hashMap.put("sgst", SGSTAmountEdit.getText().toString());
        hashMap.put("sgst_per", SGSTEdit.getText().toString().replace("%", ""));
        hashMap.put("address", invoiceTempData.getCustAddress());*/

        //  Call<ResponseBody> bodyCall = apiInterface.CreateBill(hashMap);

      /*  bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                progressDailog.dismiss();
                try {
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equals("200")) {
                        saveLocal();
                        CreatePDf();
                        // ShowThankYouScreen();
                        CommonUtils.snackBar(message, amountEdit, "200");

                    } else {
                        CommonUtils.snackBar(message, amountEdit, "300");
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                progressDailog.dismiss();
                CommonUtils.snackBar(t.toString(), amountEdit, "300");
                Log.e("Error", t.toString());
            }
        });*/
    }

    private void calculateValueforDetails() {


        if (billList == null) {
            return;
        }

        for (int i = 0; i < billList.size(); i++) {

            if (billList.get(i).getProductDesc() != null) {

                if (i == 0) {
                    des = billList.get(i).getProductDesc();
                } else {
                    des = des + "," + billList.get(i).getProductDesc();
                }


            }
        }

        if (des != null) {
            invoiceTempData.setProductDesc(des);
        }


        for (int i = 0; i < billList.size(); i++) {
            if (billList.get(i).getQuantity() != null) {

                if (i == 0) {
                    quty = billList.get(i).getQuantity();
                } else {
                    quty = quty + "," + billList.get(i).getQuantity();

                }
            }

        }
        if (quty != null) {
            invoiceTempData.setQuantity(quty);
        }

        for (int i = 0; i < billList.size(); i++) {
            if (billList.get(i).getSacOrHas() != null) {

                if (i == 0) {
                    hsn = billList.get(i).getSacOrHas();
                } else {
                    hsn = hsn + "," + billList.get(i).getSacOrHas();

                }
            }

        }
        if (hsn != null) {
            invoiceTempData.setSacOrHas(hsn);
        }

        for (int i = 0; i < billList.size(); i++) {

            if (billList.get(i).getAmount() != null) {

                if (i == 0) {
                    per_amount = billList.get(i).getAmount();
                } else {
                    per_amount = per_amount + "," + billList.get(i).getAmount();

                }
            }

        }
        if (per_amount != null) {
            invoiceTempData.setAmount(per_amount);
        }

        for (int i = 0; i < billList.size(); i++) {

            if (billList.get(i).getPertotalamount() != null) {

                if (i == 0) {
                    taxable = billList.get(i).getPertotalamount();
                } else {
                    taxable = taxable + "," + billList.get(i).getPertotalamount();

                }
            }

        }
        if (taxable != null) {
            invoiceTempData.setPertotalamount(taxable);
        }


     /*   for (int i = 0; i < billListGST.size(); i++) {
            if (billList.get(i).getRatewithQtyAmt() != null) {
                if (i == 0) {
                    total_amount = billList.get(i).getRatewithQtyAmt();
                } else {
                    total_amount = total_amount + "," + billList.get(i).getRatewithQtyAmt();

                }
            }
        }
        if(total_amount!=null) {
            invoiceTempData.setRatewithQtyAmt(total_amount);
        }*/


        for (int i = 0; i < billList.size(); i++) {
            if (billList.get(i).getSerialNumber() != null) {

                if (i == 0) {
                    serial_no = billList.get(i).getSerialNumber();
                } else {
                    serial_no = serial_no + "," + billList.get(i).getSerialNumber();

                }
            }

        }
        if (serial_no != null) {
            invoiceTempData.setSerialNumber(serial_no);

        }

        //   invoiceTempData.setRate(taxable);

        for (int i = 0; i < billListGST.size(); i++) {
            if (billListGST.get(i).getIGSTAmountN() != null) {

                if (i == 0) {
                    igst = billListGST.get(i).getIGSTAmountN();
                } else {
                    igst = igst + "," + billListGST.get(i).getIGSTAmountN();

                }
            }

        }
        if (igst != null) {
            invoiceTempData.setCGSTAmountN("");
            invoiceTempData.setUGSTAmountN("");
            invoiceTempData.setSGSTAmountN("");

            invoiceTempData.setIGSTAmountN(igst);
        }


        for (int i = 0; i < billListGST.size(); i++) {

            if (billListGST.get(i).getIGSTPer() != null) {
                if (i == 0) {
                    igst_per = billListGST.get(i).getIGSTPer();
                } else {
                    igst_per = igst_per + "," + billListGST.get(i).getIGSTPer();

                }
            }

        }

        if (igst_per != null) {
            invoiceTempData.setIGSTPer(igst_per);

            invoiceTempData.setCGSTPer("");
            invoiceTempData.setSGSTPer("");
            invoiceTempData.setUGSTPer("");
        }


        for (int i = 0; i < billListGST.size(); i++) {

            if (billListGST.get(i).getCGSTAmountN() != null) {

                if (i == 0) {
                    cgst = billListGST.get(i).getCGSTAmountN();
                } else {
                    cgst = cgst + "," + billListGST.get(i).getCGSTAmountN();

                }
            }

        }

        if (cgst != null) {

            invoiceTempData.setCGSTAmountN(cgst);



        }


        for (int i = 0; i < billListGST.size(); i++) {
            if (billListGST.get(i).getCGSTPer() != null) {

                if (i == 0) {
                    cgst_per = billListGST.get(i).getCGSTPer();
                } else {
                    cgst_per = cgst_per + "," + billListGST.get(i).getCGSTPer();

                }


            }


        }
        if (cgst_per != null) {

          //  cgst_per.substring(3, -1);
            invoiceTempData.setCGSTPer(cgst_per);
            invoiceTempData.setIGSTPer("");
          //  invoiceTempData.setUGSTPer("");

        }


        for (int i = 0; i < billListGST.size(); i++) {
            if (billListGST.get(i).getUGSTAmountN() != null) {
                if (i == 0) {

                    ugst = billListGST.get(i).getUGSTAmountN();
                } else {
                    ugst = ugst + "," + billListGST.get(i).getUGSTAmountN();

                }
            }

        }
        if (ugst != null) {
            invoiceTempData.setUGSTAmountN(ugst);
            invoiceTempData.setIGSTAmountN("");
            invoiceTempData.setSGSTAmountN("");
        }


        for (int i = 0; i < billListGST.size(); i++) {

            if (billListGST.get(i).getUGSTPer() != null) {

                if (i == 0) {
                    ugst_per = billListGST.get(i).getUGSTPer();
                } else {
                    ugst_per = ugst_per + "," + billListGST.get(i).getUGSTPer();

                }
            }
        }
        if (ugst_per != null) {
            invoiceTempData.setUGSTPer(ugst_per);

            invoiceTempData.setIGSTPer("");
            invoiceTempData.setSGSTPer("");

        }

        for (int i = 0; i < billListGST.size(); i++) {
            if (billListGST.get(i).getIGSTAmountN() != null) {


                if (i == 0) {
                    igst = billListGST.get(i).getIGSTAmountN();
                } else {
                    igst = igst + "," + billListGST.get(i).getIGSTAmountN();

                }
            }

        }
        //  invoiceTempData.setUGSTPer(igst);


        for (int i = 0; i < billListGST.size(); i++) {

            if (billListGST.get(i).getSGSTAmountN() != null) {
                if (i == 0) {
                    sgst = billListGST.get(i).getSGSTAmountN();
                } else {
                    sgst = sgst + "," + billListGST.get(i).getSGSTAmountN();

                }
            }
        }
        if (sgst != null) {
            invoiceTempData.setSGSTAmountN(sgst);

            invoiceTempData.setIGSTAmountN("");
           // invoiceTempData.setUGSTAmountN("");

        }


        for (int i = 0; i < billListGST.size(); i++) {

            if (billListGST.get(i).getSGSTPer() != null) {

                if (i == 0) {
                    sgst_per = billListGST.get(i).getSGSTPer();
                } else {
                    sgst_per = sgst_per + "," + billListGST.get(i).getSGSTPer();

                }
            }
        }
        if (sgst_per != null) {
            invoiceTempData.setSGSTPer(sgst_per);
            invoiceTempData.setIGSTPer("");
           // invoiceTempData.setUGSTPer("");
        }

    }

    private void CreatePDf() {

        String externalStorageDirectory = "" + Environment.getExternalStorageDirectory().getAbsolutePath();
        String filePah = externalStorageDirectory + "/GST/" + invoiceTempData.getInvoiceNo() + ".pdf";
        File file = new File(filePah);

        try {
            if (file.exists()) {
                file.delete();
            }
            file.getParentFile().mkdirs();

            file.createNewFile();

            PdfDocument.PageInfo pageInfo = null;
            FileOutputStream outputStream;
            PdfDocument pdfDocument;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                pageInfo = new PdfDocument.PageInfo.Builder((int) PDF_PAGE_WIDTH, (int) PDF_PAGE_HEIGHT, 1).create();

                // start a page
                pdfDocument = new PdfDocument();
                PdfDocument.Page page = pdfDocument.startPage(pageInfo);

                /*// draw something on the page
                LayoutInflater inflater = (LayoutInflater)
                        getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View content = inflater.inflate(R.layout.pdf_layout, null);

                TextView PdfComName = (TextView) content.findViewById(R.id.pdf_come_name);
                TextView PdfComAdd = (TextView) content.findViewById(R.id.pdf_com_address);
                TextView PdfInvoiceNo = (TextView) content.findViewById(R.id.pdf_invoice);
                TextView PdfDate = (TextView) content.findViewById(R.id.pdf_date);

                TextView pdf_gstn = (TextView) content.findViewById(R.id.pdf_gstn);
                TextView pdf_cin = (TextView) content.findViewById(R.id.pdf_cin);
                TextView pdf_pan = (TextView) content.findViewById(R.id.pdf_pan);
                TextView pdf_state_code = (TextView) content.findViewById(R.id.pdf_state_code);
                TextView pdf_state = (TextView) content.findViewById(R.id.pdf_state);
                TextView pdf_cust_name = (TextView) content.findViewById(R.id.pdf_cust_name);
                TextView igst_pdf = (TextView) content.findViewById(R.id.igst_pdf);

                view = (LinearLayout) content.findViewById(R.id.bitmap_layout);

                TextView pdf_cust_add1 = (TextView) content.findViewById(R.id.pdf_cust_add1);
                TextView pdf_cust_state = (TextView) content.findViewById(R.id.pdf_cust_state);
                TextView pdf_cust_stateCode = (TextView) content.findViewById(R.id.pdf_cust_stateCode);
                TextView pdf_cust_GSTN = (TextView) content.findViewById(R.id.pdf_cust_GSTN);
                TextView service_desc = (TextView) content.findViewById(R.id.service_desc);
                TextView hsn_sas_code = (TextView) content.findViewById(R.id.hsn_sas_code);
                TextView tex_amount_pdf = (TextView) content.findViewById(R.id.tex_amount_pdf);
                TextView total_amnt_pdf = (TextView) content.findViewById(R.id.total_amnt_pdf);
                TextView grand_total = (TextView) content.findViewById(R.id.grand_total);


                PdfComName.setText("Wehyphens Pvt Ltd");
                PdfComAdd.setText("Noida Sect 64");
                PdfInvoiceNo.setText(invoiceTempData.getInvoiceNo());
                PdfDate.setText(currentDate);
                pdf_gstn.setText(invoiceTempData.getCustGstn());

                pdf_cust_name.setText(invoiceTempData.getCustName());
                pdf_cust_add1.setText(invoiceTempData.getCustAddress());
                pdf_cust_state.setText(invoiceTempData.getCustState());
                pdf_cust_stateCode.setText(invoiceTempData.getStateCode());
                pdf_cust_GSTN.setText(invoiceTempData.getCustGstn());
                service_desc.setText(invoiceTempData.getProductDesc());
                hsn_sas_code.setText(invoiceTempData.getSacOrHas());
                tex_amount_pdf.setText(amountEdit.getText().toString());
                total_amnt_pdf.setText(amountEdit.getText().toString());
                igst_pdf.setText(totalTaxAmount);
                grand_total.setText(String.valueOf(calcualteGrandTotal()));
*/

                int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight() + 200, View.MeasureSpec.EXACTLY);

                content.measure(measureWidth, measuredHeight);
                content.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                content.draw(page.getCanvas());

                // finish the page
                pdfDocument.finishPage(page);
                // add more pages
                // write the document content
                outputStream = new FileOutputStream(file);
                try {
                    pdfDocument.writeTo(outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                CommonUtils.snackBar("Bill Generated", amountEdit, "200");
                // getActivity().finish();
            } else {
                CommonUtils.snackBar("cant create PDF on this Device", amountEdit, "300");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void saveLocal() {

        String bilNo = CommonUtils.getBillNo(getActivity());
        String numOnly = bilNo.replaceAll("[\\D]", "");
        long num = Long.parseLong(numOnly);
        String newstr = bilNo.replaceAll("[^A-Za-z]+", "");

        long newNum = num + 1;

        String requiredString = newstr + newNum;

        //saveInvoiceLocal(requiredString);


        saveInvoiceLocal(bilNo);
    }

    private void saveInvoiceLocal(String requiredString) {

        CommonUtils.saveBillNo(getActivity(), requiredString);
      Log.e("string",requiredString);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            getDEtails = (InterGetDEtails) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement MyInterface ");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;


    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    private void calculateAmount(float selectedValue) {

        float percentAmount = 0, halgst = 0;
        String hh = amountEdit.getText().toString();
        if (hh != null && !hh.equals("")) {
            enteredAmount = Float.parseFloat(hh);

            percentAmount = (float) (taxrate * (selectedValue / 2) / 100);
            //   percentAmount = (float) ((taxrate * selectedValue) / 100);
            halgst = (float) selectedValue / 2;
            totalTaxAmount = String.valueOf(percentAmount * 2);


            if (stateCode.equalsIgnoreCase("35") || stateCode.equalsIgnoreCase("04")
                    || stateCode.equalsIgnoreCase("26")
                    || stateCode.equalsIgnoreCase("25")
                    || stateCode.equalsIgnoreCase("31")
                    || stateCode.equalsIgnoreCase("34") &&
                    wender_state_code.equalsIgnoreCase("04")
                    || wender_state_code.equalsIgnoreCase("26")
                    || wender_state_code.equalsIgnoreCase("25")
                    || wender_state_code.equalsIgnoreCase("31")
                    || wender_state_code.equalsIgnoreCase("34")
                    || wender_state_code.equalsIgnoreCase("35")
                    ) {


                if (stateCode.equals(wender_state_code)) {

                    tv_sgst.setText("UGST");
                    tv_ugst.setText("SGST");

                    CGSTAmountEdit.setText(percentAmount + "");
                    SGSTAmountEdit.setText(percentAmount + "");

                    IGSTAmountEdit.setText("");
                    UGSTAmountEdit.setText("");

                    CGSTEdit.setText(halgst + " %");
                    SGSTEdit.setText(halgst + " %");

                    invoiceTempDataList.setUGSTAmount(SGSTAmountEdit.getText().toString());
                    invoiceTempDataList.setUGSTAmountN(SGSTAmountEdit.getText().toString());
                    invoiceTempDataList.setCGSTAmount(CGSTAmountEdit.getText().toString());
                    invoiceTempDataList.setCGSTAmountN(CGSTAmountEdit.getText().toString());

                    invoiceTempDataList.setUGSTPer(halgst + " %");

                    invoiceTempDataList.setCGSTPer(halgst + " %");

                    invoiceTempDataList.setUGSTAmountPer(SGSTAmountEdit.getText().toString());


                    invoiceTempDataList.setCGSTAmountPer(CGSTAmountEdit.getText().toString());


                } else {


                    IGSTEdit.setText(selectedValue + " %");
                    IGSTAmountEdit.setText(percentAmount + "");
                    invoiceTempDataList.setIGSTAmountN(IGSTAmountEdit.getText().toString());
                    invoiceTempDataList.setIGSTAmount(IGSTAmountEdit.getText().toString());
                    invoiceTempDataList.setIGSTPer(selectedValue + "%");


                }
            } else if (stateCode.equalsIgnoreCase(wender_state_code)) {

                tv_sgst.setText("SGST");
                tv_ugst.setText("UGST");

                CGSTAmountEdit.setText(percentAmount + "");
                SGSTAmountEdit.setText(percentAmount + "");

                IGSTAmountEdit.setText("");
                UGSTAmountEdit.setText("");

                CGSTEdit.setText(halgst + " %");
                SGSTEdit.setText(halgst + " %");

                invoiceTempDataList.setSGSTAmount(SGSTAmountEdit.getText().toString());
                invoiceTempDataList.setCGSTAmount(CGSTAmountEdit.getText().toString());
                invoiceTempDataList.setSGSTAmountN(SGSTAmountEdit.getText().toString());
                invoiceTempDataList.setCGSTAmountN(CGSTAmountEdit.getText().toString());

                invoiceTempDataList.setSGSTPer(halgst + " %");

                invoiceTempDataList.setCGSTPer(halgst + " %");

                invoiceTempDataList.setSGSTAmountPer(SGSTAmountEdit.getText().toString());
                invoiceTempDataList.setCGSTAmountPer(CGSTAmountEdit.getText().toString());


            } else {

                tv_sgst.setText("SGST");
                percentAmount = (float) (taxrate * (selectedValue) / 100);
                totalTaxAmount = String.valueOf(percentAmount);
                CGSTAmountEdit.setText("");
                SGSTAmountEdit.setText("");
                UGSTAmountEdit.setText("");
                IGSTAmountEdit.setText(percentAmount + "");

                IGSTEdit.setText(selectedValue + " %");
                invoiceTempDataList.setIGSTAmountN(IGSTAmountEdit.getText().toString());
                invoiceTempDataList.setIGSTAmount(IGSTAmountEdit.getText().toString());
                invoiceTempDataList.setIGSTPer(selectedValue + "%");


                // invoiceTempDataList.setUGSTPer(selectedValue+" %");

            }

        }
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        //  MyBills homeFragment = MyBills.newInstance();
        //  getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.containerstepper, homeFragment).commit();

        getActivity().onBackPressed();


    }

   /* @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {

    }*/

    @Override
    public void showProgress() {

        progressDailog.show();
    }

    @Override
    public void hideProgress() {

        progressDailog.dismiss();
    }

    @Override
    public void onAlreadysucess(String msg) {
        //    progressDailog.dismiss();

        CommonUtils.snackBar(msg, amountEdit, "400");
    }

    @Override
    public void onSuccess() {


        CommonUtils.snackBar("Bill Generated", amountEdit, "200");
        saveLocal();

        /*new Thread(new Runnable() {
            @Override
            public void run() {
                saveLocal();
            }
        });*/

        CreatePDf();

        Intent intent=new Intent(context,DashBoardActivity.class);
        intent.putExtra("GST","GST");
        startActivity(intent);

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (createInvoicePresenter != null) {
            createInvoicePresenter.onDestroy();
        }

    }
}
