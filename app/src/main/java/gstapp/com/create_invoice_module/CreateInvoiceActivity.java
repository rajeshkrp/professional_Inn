package gstapp.com.create_invoice_module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.List;

import ModelClasses.BillPaperModel;
import UtilClasses.CommonUtils;
import UtilClasses.InvoiceTempData;
import UtilClasses.LogeedUserDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.gstapp.MyStepperAdapter;
import gstapp.com.gstapp.R;

public class CreateInvoiceActivity extends AppCompatActivity implements
        StepperLayout.StepperListener, CreateInvoiceView,InterGetDEtails{

    public StepperLayout mStepperLayout;

    @BindView(R.id.backEdit)
    ImageView backEdit;

    @BindView(R.id.user_name)
    TextView user_name;

    @BindView(R.id.logOut)
    TextView logOut;
     int postion=0;
     private Context context;
     public   List<InvoiceTempData> billpaperList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_invoice);
        context=CreateInvoiceActivity.this;

        ButterKnife.bind(this);
        billpaperList =new ArrayList<>();

        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new MyStepperAdapter(getSupportFragmentManager(), CreateInvoiceActivity.this));

        LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(CreateInvoiceActivity.this);
       user_name.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getMName() + " " + logeedUserDetails.getLName());
      // user_name.setText(user_name);

        backEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
      //  mStepperLayout.setCurrentStepPosition(6);
    }




    @Override
    public void onCompleted(View completeButton) {

        Toast.makeText(this, "onCompleted!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(VerificationError verificationError) {
        Toast.makeText(this, "onError! -> " + verificationError.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStepSelected(int newStepPosition) {
        Toast.makeText(this, "onStepSelected! -> " + newStepPosition, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReturn() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void showProgress() {


    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onAlreadysucess(String msg) {

    }

    @Override
    public void onSuccess() {



    }



    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    public  List<InvoiceTempData> getBilllist(){
        return billpaperList;
    }

    @Override
    public void getData(List<InvoiceTempData> value) {

        billpaperList.addAll(value);

        Toast.makeText(context, "oncreateInvoice:::"+ billpaperList.size(), Toast.LENGTH_SHORT).show();

        Log.e("oncreateInvoice",""+ billpaperList.size());

        /* GstCalculation firstCallFragment1=new GstCalculation();
         firstCallFragment1.getDataAll(value,context);*/



    }
}
