package gstapp.com.create_invoice_module;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import UtilClasses.CommonUtils;
import UtilClasses.InvoiceTempData;
import gstapp.com.login_module.LoginInteractor;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class InvoiceInteracotrImpl implements CreateInvoiceInteractor {

    ApiInterface apiInterface = CommonUtils.InitilizeInterface();

    @Override
    public void createBill(InvoiceTempData invoiceTempData, String currentDate, String id, final CreateInvoiceInteractor.OnBillFinishedListener listener) {



        HashMap<String, String> hashMap = new HashMap();

        String id1= id!=null? hashMap.put("user_id", id):"0.0";
        String name_of_supplier= invoiceTempData.getCustName()!=null ? hashMap.put("name_of_supplier", invoiceTempData.getCustName()):"0.0";
        String gstin_uin= invoiceTempData.getCustGstn()!=null? hashMap.put("gstin_uin", invoiceTempData.getCustGstn()):"0.0";
        String state_name= invoiceTempData.getCustState()!=null? hashMap.put("state_name", invoiceTempData.getCustState()):"0.0";
        String pos= invoiceTempData.getStateCode()!=null? hashMap.put("pos", invoiceTempData.getStateCode()):"0.0";
        String invoice_no= invoiceTempData.getInvoiceNo()!=null? hashMap.put("invoice_no", invoiceTempData.getInvoiceNo()):"0.0";
        String invoice_date= currentDate!=null? hashMap.put("invoice_date", currentDate):"0.0";
        hashMap.put("invoice_value", "20");
        String hsn_sac_code= invoiceTempData.getSacOrHas()!=null? hashMap.put("hsn_sac_code", invoiceTempData.getSacOrHas()):"0.0";
        String description= invoiceTempData.getProductDesc()!=null? hashMap.put("description", invoiceTempData.getProductDesc()):"0.0";
        String rate= invoiceTempData.getAmount()!=null? hashMap.put("rate", invoiceTempData.getAmount()):"0.0";
        String ship_details= invoiceTempData.getWholeshippingAddress()!=null? hashMap.put("ship_details", invoiceTempData.getWholeshippingAddress()):"0.0";
        String Vechical_no= invoiceTempData.getVehicle()!=null? hashMap.put("vechical_no", invoiceTempData.getVehicle()):"0.0";
        String qnty= invoiceTempData.getQuantity()!=null? hashMap.put("qnty", invoiceTempData.getQuantity()):"0.0";
        String qty= invoiceTempData.getQuantity()!=null? hashMap.put("qty", invoiceTempData.getQuantity()):"0.0";
        String billAddress= invoiceTempData.getWholeBillAddress()!=null? hashMap.put("address", invoiceTempData.getWholeBillAddress()):"No ";
       String sgst= invoiceTempData.getSGSTAmountN()!=null? hashMap.put("sgst", invoiceTempData.getSGSTAmountN()):"0.0";
       String cgst= invoiceTempData.getCGSTAmountN()!=null? hashMap.put("cgst", invoiceTempData.getCGSTAmountN()):"0.0";
       String ugst= invoiceTempData.getUGSTAmountN()!=null? hashMap.put("ugst", invoiceTempData.getUGSTAmountN()):"0.0";
       String igst= invoiceTempData.getIGSTAmountN()!=null? hashMap.put("igst", invoiceTempData.getIGSTAmountN()):"0.0";
       String sgst_per= invoiceTempData.getSGSTPer()!=null? hashMap.put("sgst_per", invoiceTempData.getSGSTPer()):"0.0";
       String cgst_per= invoiceTempData.getCGSTPer()!=null? hashMap.put("cgst_per", invoiceTempData.getCGSTPer()):"0.0";
       String igst_per= invoiceTempData.getIGSTPer()!=null? hashMap.put("igst_per", invoiceTempData.getIGSTPer()):"0.0";

       String ugst_percetage= invoiceTempData.getUGSTPer()!=null? hashMap.put("ugst_percetage",invoiceTempData.getUGSTPer()):"0.0";


       String amount_per_tax= invoiceTempData.getPertotalamount()!=null? hashMap.put("amount_per_tax", invoiceTempData.getPertotalamount()):"0.0";
       String invoice_taxable_amt= invoiceTempData.getRatewithQtyAmt()!=null? hashMap.put("invoice_taxable_amt", invoiceTempData.getRatewithQtyAmt()):"0.0";

       String per_amount= invoiceTempData.getTotalAmount()!=null? hashMap.put("total_amount", invoiceTempData.getTotalAmount()):"0.0";
       String total_gst= invoiceTempData.getTotalGst()!=null? hashMap.put("total_gst", invoiceTempData.getTotalGst()):"0.0";
        String serial_no= invoiceTempData.getSerialNumber()!=null? hashMap.put("serial_no", invoiceTempData.getSerialNumber()):"0.0";
        Log.e("allValue",hashMap.toString());




        Call<ResponseBody> bodyCall = apiInterface.CreateBill(hashMap);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res = response.body().string();
                    Log.e("invoiceresponse",res);

                    JSONObject jsonObject = new JSONObject(res);






                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");

                    Log.e("invoiceresponse",jsonObject.toString());

                    if (status==200) {

                        listener.onSuccess();
                        //  CommonUtils.snackBar(message, amountEdit, "200");

                    }


                    else   if (status==201) {

                        listener.onRoleError(message);
                        //  CommonUtils.snackBar(message, amountEdit, "200");

                    }
                    else {
                        listener.onError();
                      //  CommonUtils.snackBar(message, amountEdit, "300");
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {


                listener.onFailure();
               // CommonUtils.snackBar(t.toString(), amountEdit, "300");
                Log.e("Error", t.toString());
            }
        });

    }


}
