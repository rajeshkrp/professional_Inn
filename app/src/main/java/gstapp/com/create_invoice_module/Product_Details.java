package gstapp.com.create_invoice_module;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.List;

import ModelClasses.BillPaperModel;
import UtilClasses.CommonUtils;
import UtilClasses.InvoiceTempData;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.gstapp.R;


public class Product_Details extends Fragment implements Step {


    static EditText product_desc;

    static EditText sac_HSN;

    @BindView(R.id.secondInvoiceNo)
     TextView secondInvoiceNo;


    static  EditText quantity;


     private BillPaperModel  paperModel;

    InvoiceTempData invoiceTempData;
    public StepperLayout mStepperLayout;

    private StepperLayout.OnBackClickedCallback callbackbutton;
    private Context context;
    private Activity activity;


    private int count=0;


    public Product_Details() {
        // Required empty public constructor
    }


    public static Product_Details newInstance() {
        Product_Details fragment = new Product_Details();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product__details, container, false);

      //  paperModel = ViewModelProviders.of(this).get(BillPaperModel.class);

        mStepperLayout=getActivity().findViewById(R.id.stepperLayout);
        product_desc=view.findViewById(R.id.product_desc);
        sac_HSN=view.findViewById(R.id.sac_HSN);
        quantity=view.findViewById(R.id.quantityProduct);

        context=getActivity();
        activity=getActivity();





        ButterKnife.bind(this, view);


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);

        }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        invoiceTempData = InvoiceTempData.getInstance();
        secondInvoiceNo.setText(invoiceTempData.getInvoiceNo());


    }

    @Override
    public void onDetach() {
        super.onDetach();


    }


    @Override
    public void onResume() {
        super.onResume();

        if(CommonUtils.getPreferencesString(context,"shipState")!=null){

           // Toast.makeText(context, "onresumeProduct", Toast.LENGTH_SHORT).show();

            CommonUtils.saveStringPreferences(context,"shipState",CommonUtils.getPreferencesString(context,"shipState"));

        }

        if(CommonUtils.getPreferencesString(context,"billState")!=null){

            CommonUtils.saveStringPreferences(context,"billState",CommonUtils.getPreferencesString(context,"billState"));

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        }

    @Nullable
    @Override
    public VerificationError verifyStep() {

        count++;

        if (product_desc.length() == 0) {
            CommonUtils.snackBar("Please enter Product Description", product_desc,"300");
            return new VerificationError("Please enter Product Description");
        } else if (sac_HSN.length() == 0) {
            CommonUtils.snackBar("Please enter HSN/SAC", sac_HSN,"300");
            return new VerificationError("Please enter Product Description");
        } else {



            CommonUtils.saveStringPreferences(context,"Count",count+"");


            invoiceTempData.setProductDesc(product_desc.getText().toString());
            invoiceTempData.setSacOrHas(sac_HSN.getText().toString());
            invoiceTempData.setQuantity(quantity.getText().toString());


            sac_HSN.setText("");
           product_desc.setText("");
            quantity.setText("");

            //quantity.setText("");

            return null;
        }
    }

    @Override
    public void onSelected() {
        Log.v("OnSelected", this.toString());
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }




    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
