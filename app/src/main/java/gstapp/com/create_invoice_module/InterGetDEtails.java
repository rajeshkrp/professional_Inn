package gstapp.com.create_invoice_module;

import java.util.List;

import ModelClasses.BillPaperModel;
import ModelClasses.GSTDetails;
import UtilClasses.InvoiceTempData;

public interface InterGetDEtails {

    public void getData( List<InvoiceTempData> value);
}
