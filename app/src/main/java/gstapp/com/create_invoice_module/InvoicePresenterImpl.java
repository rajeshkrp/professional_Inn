package gstapp.com.create_invoice_module;

import UtilClasses.InvoiceTempData;

/**
 * Created by Abdul on 4/10/2018.
 */

public class InvoicePresenterImpl implements CreateInvoicePresenter, CreateInvoiceInteractor.OnBillFinishedListener {

    CreateInvoiceView createInvoiceView;
    CreateInvoiceInteractor createInvoiceInteractor;

    public InvoicePresenterImpl(CreateInvoiceView createInvoiceView, CreateInvoiceInteractor createInvoiceInteractor) {

        this.createInvoiceView = createInvoiceView;
        this.createInvoiceInteractor = createInvoiceInteractor;

    }

    @Override
    public void createBill(InvoiceTempData invoiceTempData, String currentDate, String id) {

        if (createInvoiceView != null) {
            createInvoiceView.showProgress();
        }
        createInvoiceInteractor.createBill(invoiceTempData, currentDate, id, this);

    }

    @Override
    public void onDestroy() {

        createInvoiceView = null;
    }

    @Override
    public void onUsernameError() {

    }

    @Override
    public void onPasswordError() {

    }

    @Override
    public void onSuccess() {

        if (createInvoiceView != null) {
            createInvoiceView.hideProgress();
            createInvoiceView.onSuccess();
        }


    }

    @Override
    public void onRoleError(String message) {


        if (createInvoiceView != null) {
            createInvoiceView.hideProgress();
            createInvoiceView.onAlreadysucess(message);
        }



    }

    @Override
    public void onError() {

    }

    @Override
    public void onFailure() {

    }
}
