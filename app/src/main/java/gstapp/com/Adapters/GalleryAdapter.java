package gstapp.com.Adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import gstapp.com.GalleryViewActivity;
import gstapp.com.gstapp.PayGstActivity;
import gstapp.com.gstapp.R;
import uk.co.senab.photoview.PhotoViewAttacher;

public class GalleryAdapter extends PagerAdapter {

    Context context;
    ArrayList<String> imageList;
    GalleryViewActivity galleryViewActivity;

    public GalleryAdapter(Context context, ArrayList<String> imageList, GalleryViewActivity galleryViewActivity) {

        this.context = context;
        this.imageList = imageList;
        this.galleryViewActivity = galleryViewActivity;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        RelativeLayout linearLayout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.gallery_pager_items, null);

        final ZoomageView imageView = (ZoomageView) linearLayout.findViewById(R.id.imgeGellery);
        final ProgressBar progressBar = (ProgressBar) linearLayout.findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        //PDFView pdfView = (PDFView) linearLayout.findViewById(R.id.pdfView);
        WebView webView = (WebView) linearLayout.findViewById(R.id.webView);
        // webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        // webView.setWebViewClient(new MyBrowser());

        webView.getSettings().setJavaScriptEnabled(true);

        String imagePath = imageList.get(position);
        if (!imagePath.contains(".pdf")) {
            webView.setVisibility(View.GONE);

            //PhotoViewAttacher pAttacher;
            // pAttacher = new PhotoViewAttacher(imageView);
            // pAttacher.update();
            Picasso.get().load(imagePath).into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                    imageView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {

                }
            });
        } else {
            webView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);

            webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + imagePath);
            //galleryViewActivity.setPdfImage(webView, imagePath);
        }
        container.addView(linearLayout);
        return linearLayout;

    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
