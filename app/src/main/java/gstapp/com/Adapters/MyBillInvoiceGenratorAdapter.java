package gstapp.com.Adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ModelClasses.ProductDetails;
import gstapp.com.gstapp.R;

public class MyBillInvoiceGenratorAdapter extends RecyclerView.Adapter<MyBillInvoiceGenratorAdapter.MyViewHolder> {
    List<ProductDetails> ProductDetailsList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_for_invoice, parent, false);

        return new MyViewHolder(itemView);
    }
    public MyBillInvoiceGenratorAdapter(List<ProductDetails> ProductDetailsList) {
        this.ProductDetailsList = ProductDetailsList;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ProductDetails paper = ProductDetailsList.get(position);
       // String s= String.valueOf(ProductDetailsList.size());
        holder.tv_serial_no.setText(paper.getSerialNo());
       // holder.tv_serial_no.setText(s);
        holder.tv_description_product.setText(paper.getGoodServiceDescription());
        holder.tv_hsn_sas.setText(paper.getHsnSacCode());
        holder.tv_quantity.setText(paper.getQty());
      //  holder.tv_rate.setText("₹ "+paper.getRate());
       // holder.tv_taxable.setText("₹ "+paper.getAmountPerTax());

        if(!TextUtils.isEmpty(paper.getAmountPerTax())) {

            if(paper.getAmountPerTax().length()>=5){
                holder.tv_taxable.setTextSize(10);
                holder.rupee_tv_rate_amount.setTextSize(10);
                holder.tv_taxable.setText(paper.getAmountPerTax());

            }
            else {
                holder.tv_taxable.setText(paper.getAmountPerTax());

            }
            //holder.tv_taxable.setText("₹ "+"123467744332");
        }



        if(!TextUtils.isEmpty(paper.getRate())) {

            if(paper.getRate().length()>=5){
                holder.tv_rate.setTextSize(10);
                holder.rupee_tv_rate_amount.setTextSize(10);
                holder.tv_rate.setText(paper.getRate());

            }
            else {
                holder.tv_rate.setText(paper.getRate());

            }
            //holder.tv_taxable.setText("₹ "+"123467744332");
        }


    }

    @Override
    public int getItemCount() {
        return ProductDetailsList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_serial_no,tv_description_product,tv_hsn_sas,tv_quantity,tv_rate,tv_taxable,rupee_tv_rate_amount,rupee_tv_rate;

        public MyViewHolder(View view) {
            super(view);
            tv_serial_no = (TextView) view.findViewById(R.id.serial_no);
            tv_description_product = (TextView) view.findViewById(R.id.service_desc);
            tv_hsn_sas = (TextView) view.findViewById(R.id.hsn_sas_code);
            tv_quantity = (TextView) view.findViewById(R.id.quantity);
            tv_rate = (TextView) view.findViewById(R.id.tv_rate);
            tv_taxable = (TextView) view.findViewById(R.id.tv_rate_amount);
            rupee_tv_rate_amount = (TextView) view.findViewById(R.id.rupee_tv_rate_amount);
            rupee_tv_rate = (TextView) view.findViewById(R.id.rupee_tv_rate);
        }
    }
}
