package gstapp.com.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ModelClasses.MyBillDetailPojo;
import gstapp.com.gstapp.R;

public class MembershipAdapter extends RecyclerView.Adapter {

    Context context;
    List<MyBillDetailPojo> myBillDetailPojo;
    int viewType;

    public MembershipAdapter(Context context, List<MyBillDetailPojo> myBillDetailPojo) {

        this.context = context;
        this.myBillDetailPojo = myBillDetailPojo;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = null;
        if (viewType == 0) {
            view = LayoutInflater.from(context).inflate(R.layout.my_bill_first_row, parent, false);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.my_items, parent, false);
        }


        BillViewHolder billViewHolder = new BillViewHolder(view);

        return billViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (position != 0) {
            BillViewHolder billViewHolder = (BillViewHolder) holder;
            billViewHolder.invoice_no.setText(myBillDetailPojo.get(position).getInvoiceNo());
            billViewHolder.supplier_name.setText(myBillDetailPojo.get(position).getNameOfSupplier());
            billViewHolder.bill_date.setText(myBillDetailPojo.get(position).getInvoiceDate());
        }

    }

    @Override
    public int getItemCount() {
        return myBillDetailPojo.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            viewType = 0;
        } else {
            viewType = position;
        }
        return viewType;
    }

    public class BillViewHolder extends RecyclerView.ViewHolder {

        TextView invoice_no, supplier_name, bill_date;

        public BillViewHolder(View itemView) {
            super(itemView);

            invoice_no = (TextView) itemView.findViewById(R.id.invoice_no);
            supplier_name = (TextView) itemView.findViewById(R.id.supplier_name);
            bill_date = (TextView) itemView.findViewById(R.id.bill_date);
        }
    }


}
