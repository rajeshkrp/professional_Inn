package gstapp.com.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ModelClasses.MyBillDetailPojo;
import ModelClasses.ProductDetails;
import UtilClasses.InvoiceTempData;
import gstapp.com.fragments.MyBills;
import gstapp.com.gstapp.R;

public class MyBillsAdapter extends RecyclerView.Adapter {

    Context context;
    private List<MyBillDetailPojo> myBillDetailPojo;

    private ArrayList<MyBillDetailPojo> arraylist;
    MyBillInvoiceGenratorAdapter invoiceGenratorAdapter;

    List<InvoiceTempData> invoiceList = new ArrayList<>();
    List<InvoiceTempData> myBillDetailPojoList = new ArrayList<>();


    int viewType;
    MyBills myBills;

    public MyBillsAdapter(Context context, List<MyBillDetailPojo> myBillDetailPojo, MyBills myBills) {

        this.context = context;
        this.myBillDetailPojo = myBillDetailPojo;

        this.arraylist = new ArrayList<MyBillDetailPojo>();
        this.arraylist.addAll(myBillDetailPojo);
        this.myBills = myBills;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = null;
       /* if (viewType == 0) {
            view = LayoutInflater.from(context).inflate(R.layout.my_bill_first_row, parent, false);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.my_items, parent, false);
        }*/
        view = LayoutInflater.from(context).inflate(R.layout.my_items, parent, false);
        BillViewHolder billViewHolder = new BillViewHolder(view);

        return billViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {


            BillViewHolder billViewHolder = (BillViewHolder) holder;
            billViewHolder.invoice_no.setText(myBillDetailPojo.get(position ).getInvoiceNo());
            billViewHolder.supplier_name.setText(myBillDetailPojo.get(position ).getNameOfSupplier());
            billViewHolder.bill_date.setText(myBillDetailPojo.get(position ).getInvoiceDate());





            billViewHolder.tv_view_invoice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    myBills.previewBill(myBillDetailPojo.get(position), myBillDetailPojo.get(position).getGstList(),  myBillDetailPojo.get(position).getProduct(),context);


                   // myBills.openDialog(myBillDetailPojo.get(position - 1),context);
                }
            });

            if (position % 2 == 0) {
                billViewHolder.mybill_linear.setBackgroundColor(Color.WHITE);
            } else {

                billViewHolder.mybill_linear.setBackgroundColor(Color.parseColor("#eeeeee"));
            }


    }

    @Override
    public int getItemCount() {
        return myBillDetailPojo.size();
    }

    @Override
    public int getItemViewType(int position) {
        /*if (position == 0) {
            viewType = 0;
        } else {
            viewType = position;
        }*/
        viewType=position;
        return viewType;
    }

    public void filter(String charText) {


        charText = charText.toLowerCase(Locale.getDefault());
        myBillDetailPojo.clear();
        if (charText.length() == 0) {
            myBillDetailPojo.addAll(arraylist);
        } else {
            for (MyBillDetailPojo wp : arraylist) {
                if (wp.getNameOfSupplier().toLowerCase(Locale.getDefault()).contains(charText)) {
                    myBillDetailPojo.add(wp);
                }
            }
        }
        notifyDataSetChanged();

    }

    public class BillViewHolder extends RecyclerView.ViewHolder {

        TextView invoice_no, supplier_name, bill_date, tv_view_invoice;

        LinearLayout mybill_linear;
        RecyclerView recyclerView,recy_tax;

        public BillViewHolder(View itemView) {
            super(itemView);

            invoice_no = (TextView) itemView.findViewById(R.id.invoice_no);
            supplier_name = (TextView) itemView.findViewById(R.id.supplier_name);
            bill_date = (TextView) itemView.findViewById(R.id.bill_date);
            tv_view_invoice = (TextView) itemView.findViewById(R.id.tv_view_invoice);
            mybill_linear = (LinearLayout) itemView.findViewById(R.id.mybill_linear);

        }
    }


}
