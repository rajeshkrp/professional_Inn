package gstapp.com.Adapters;

import android.content.Context;
import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import UtilClasses.InvoiceTempData;
import gstapp.com.gstapp.R;

public class InvoiceGenratorAdapter extends RecyclerView.Adapter<InvoiceGenratorAdapter.MyViewHolder> {
    List<InvoiceTempData> InvoiceTempDataList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_for_invoice, parent, false);

        return new MyViewHolder(itemView);
    }
    public InvoiceGenratorAdapter(List<InvoiceTempData> InvoiceTempDataList, Context context) {
        this.InvoiceTempDataList = InvoiceTempDataList;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        InvoiceTempData paper = InvoiceTempDataList.get(position);
        String s= String.valueOf(paper.getSerialNumber());
        holder.tv_serial_no.setText(paper.getSerialNumber());
       // holder.tv_serial_no.setText(s);
        holder.tv_description_product.setText(paper.getProductDesc());
        holder.tv_hsn_sas.setText(paper.getSacOrHas());
        holder.tv_quantity.setText(InvoiceTempDataList.get(position).getQuantity());
        //holder.tv_rate.setText("₹ "+paper.getAmount());
      /*  if(!TextUtils.isEmpty(paper.getPertotalamount())) {

           // holder.tv_taxable.setText("₹ "+paper.getPertotalamount());
           holder.tv_taxable.setText("9504.0");
            holder.tv_rate.setText("213123412412423");
            //holder.tv_taxable.setText("₹ "+"950467896989");
        }
*/
        if(!TextUtils.isEmpty(paper.getPertotalamount())) {

            if(paper.getPertotalamount().length()>=5){
                holder.tv_taxable.setTextSize(10);
                holder.rupee_tv_rate_amount.setTextSize(10);
                holder.tv_taxable.setText(paper.getPertotalamount());

            }
            else {
                holder.tv_taxable.setText(paper.getPertotalamount());

            }
            //holder.tv_taxable.setText("₹ "+"123467744332");
        }



        if(!TextUtils.isEmpty(paper.getAmount())) {

            if(paper.getAmount().length()>=5){
                holder.tv_rate.setTextSize(10);
                holder.rupee_tv_rate_amount.setTextSize(10);
                holder.tv_rate.setText(paper.getAmount());

            }
            else {
                holder.tv_rate.setText(paper.getAmount());

            }
            //holder.tv_taxable.setText("₹ "+"123467744332");
        }



        Log.e("totalamount",paper.getTotalAmount());
        Log.e("totalamount",paper.getPertotalamount());
        Log.e("totalamount",InvoiceTempDataList.get(position).getPertotalamount());

    }

    @Override
    public int getItemCount() {
        return InvoiceTempDataList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_serial_no,tv_description_product,tv_hsn_sas,tv_quantity,tv_rate,tv_taxable,rupee_tv_rate_amount,rupee_tv_rate;

        public MyViewHolder(View view) {
            super(view);
            tv_serial_no = (TextView) view.findViewById(R.id.serial_no);
            tv_description_product = (TextView) view.findViewById(R.id.service_desc);
            tv_hsn_sas = (TextView) view.findViewById(R.id.hsn_sas_code);
            tv_quantity = (TextView) view.findViewById(R.id.quantity);
            tv_rate = (TextView) view.findViewById(R.id.tv_rate);
            tv_taxable = (TextView) view.findViewById(R.id.tv_rate_amount);
            rupee_tv_rate_amount = (TextView) view.findViewById(R.id.rupee_tv_rate_amount);
            rupee_tv_rate = (TextView) view.findViewById(R.id.rupee_tv_rate);
        }
    }
}
