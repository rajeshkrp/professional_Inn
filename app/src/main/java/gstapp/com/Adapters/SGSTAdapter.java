package gstapp.com.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import UtilClasses.CommonUtils;
import UtilClasses.InvoiceTempData;
import UtilClasses.LogeedUserDetails;
import gstapp.com.gstapp.R;

public class SGSTAdapter extends RecyclerView.Adapter<SGSTAdapter.MyViewHolder> {
    List<InvoiceTempData> InvoiceTempDataList=new ArrayList<>();


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sgst_row_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public SGSTAdapter(List<InvoiceTempData> InvoiceTempDataList) {
        this.InvoiceTempDataList = InvoiceTempDataList;
        }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

            InvoiceTempData paper = InvoiceTempDataList.get(position);
            holder.sgst_tax_value.setText(InvoiceTempDataList.get(position).getSGSTAmount());
            holder.tv_sgst_infigure.setText(InvoiceTempDataList.get(position).getSGSTAmount());
            holder.tv_sgst_percentage.setText("SGST @"+InvoiceTempDataList.get(position).getSGSTPer());
    }

    @Override
    public int getItemCount() {
       return InvoiceTempDataList.size();
      // return 2;

    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_sgst_infigure,tv_sgst_percentage,sgst_tax_value;

        public MyViewHolder(View view) {
            super(view);
            tv_sgst_infigure = (TextView) view.findViewById(R.id.tv_sgst_infigure);
            tv_sgst_percentage = (TextView) view.findViewById(R.id.tv_sgst_percentage);
            sgst_tax_value = (TextView) view.findViewById(R.id.sgst_tax_value);

        }
    }
}
