package gstapp.com.Adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import ModelClasses.FileBean;
import gstapp.com.gstapp.PdfActivity;
import gstapp.com.gstapp.R;

/**
 * Created by Abdul on 4/2/2018.
 */

public class FileAdapter extends ArrayAdapter<FileBean> {

    Context cxt;
    int res;
    ArrayList<FileBean> list;
    PdfActivity pdfActivity;

    public FileAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<FileBean> objects, PdfActivity pdfActivity) {
        super(context, resource, objects);

        cxt = context;
        res = resource;
        list = objects;
        this.pdfActivity = pdfActivity;

    }

    @Nullable
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //Initializing view which will point to layout file list_item
        View view = LayoutInflater.from(cxt).inflate(res, parent, false);

        //Text view showing pdf file name
        TextView txtView = (TextView) view.findViewById(R.id.txtFileName);

        FileBean fileBean = list.get(position);
        final CheckBox checkBox = (CheckBox) view.findViewById(R.id.chbContent);
        if (fileBean.isClicked()) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list.get(position).setClicked(true);
                pdfActivity.addCurrentPdfToList(position,checkBox);
            }
        });

        //setting the file name
        txtView.setText(list.get(position).getFileName());
        return view;
    }

}
