package gstapp.com.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import UtilClasses.InvoiceTempData;
import gstapp.com.gstapp.R;

public class CGSTAdapter extends RecyclerView.Adapter<CGSTAdapter.MyViewHolder> {
    List<InvoiceTempData> InvoiceTempDataList=new ArrayList<>();

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cgst_row_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public CGSTAdapter(List<InvoiceTempData> InvoiceTempDataList) {
        this.InvoiceTempDataList = InvoiceTempDataList;
        }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
            InvoiceTempData paper = InvoiceTempDataList.get(position);
            holder.tv_cgst_infigure.setText(InvoiceTempDataList.get(position).getCGSTAmount());
            holder.tv_cgst_percentage.setText("CGST @"+InvoiceTempDataList.get(position).getCGSTPer());
            holder.cgst_tax_value.setText(InvoiceTempDataList.get(position).getCGSTAmount());
    }

    @Override
    public int getItemCount() {
       return InvoiceTempDataList.size();
      // return 2;

    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cgst_infigure,tv_cgst_percentage,
                cgst_tax_value;
        public MyViewHolder(View view) {
            super(view);
            tv_cgst_infigure = (TextView) view.findViewById(R.id.tv_cgst_infigure);
            tv_cgst_percentage = (TextView) view.findViewById(R.id.tv_cgst_percentage);
            cgst_tax_value = (TextView) view.findViewById(R.id.cgst_tax_value);
        }
    }
}
