package gstapp.com.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ModelClasses.GSTListDetails;
import ModelClasses.MyBillDetailPojo;
import UtilClasses.CommonUtils;
import UtilClasses.LogeedUserDetails;
import gstapp.com.gstapp.R;

public class MyBillTaxSlabAdapter extends RecyclerView.Adapter<MyBillTaxSlabAdapter.MyViewHolder> {
    List<GSTListDetails> GSTListDetailsList;
    String wender_state_code="";
    SharedPreferences preferences;
    String stateCode;
    private Context context1;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tax_slab_row_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public MyBillTaxSlabAdapter(List<GSTListDetails> GSTListDetailsList, Context context, MyBillDetailPojo myBillDetailPojo, String state_code) {
        this.GSTListDetailsList = GSTListDetailsList;
        this.context1 = context;
        stateCode=state_code;

        LogeedUserDetails s=new LogeedUserDetails();
        s.getState_code();
    /*    preferences = PreferenceManager.getDefaultSharedPreferences(context);
        wender_state_code=preferences.getString("state_code","");
        wender_state_code=CommonUtils.getPreferencesString(context,"STATE_CODE");*/


        /*preferences = PreferenceManager.getDefaultSharedPreferences(context);
        stateCode=preferences.getString("state_code","");*/

        wender_state_code=CommonUtils.getPreferencesString(context,"Admin_status_code");


        System.out.println(wender_state_code);
        Log.e("TAG",wender_state_code);

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        GSTListDetails paper = GSTListDetailsList.get(position);


        // double totalGst=0.0;
         String totalGst="";
        int count=0;




        if(GSTListDetailsList!=null&&GSTListDetailsList.size()>0) {
            if (stateCode.equalsIgnoreCase("35")
                    || stateCode.equalsIgnoreCase("04")
                    || stateCode.equalsIgnoreCase("26")
                    || stateCode.equalsIgnoreCase("25")
                    || stateCode.equalsIgnoreCase("31")
                    || stateCode.equalsIgnoreCase("34") && wender_state_code.equalsIgnoreCase("04")
                    || wender_state_code.equalsIgnoreCase("26")
                    || wender_state_code.equalsIgnoreCase("25")
                    || wender_state_code.equalsIgnoreCase("31")
                    || wender_state_code.equalsIgnoreCase("34")
                    || wender_state_code.equalsIgnoreCase("35")
                    ) {
                if (stateCode.equals(wender_state_code)) {


                    holder.llCgst.setVisibility(View.VISIBLE);
                    holder.llUgst.setVisibility(View.VISIBLE);

                    if (GSTListDetailsList.get(position).getCgst() != null) {

                        holder.cgst_tax_value.setText("₹ " + GSTListDetailsList.get(position).getCgst());
                        holder.tv_cgst_infigure.setText("₹ " + GSTListDetailsList.get(position).getCgst());
                        holder.tv_cgst_percentage.setText("CGST - " + GSTListDetailsList.get(position).getCgstPer() + "%");

                        if (GSTListDetailsList.get(position).getUgst() != null) {
                            holder.Ugst_tax_value.setText("₹ " + GSTListDetailsList.get(position).getUgst());
                            holder.tv_Ugst_infigure.setText("₹ " + GSTListDetailsList.get(position).getUgst());
                            holder.tv_Ugst_percentage.setText("UGST - " + GSTListDetailsList.get(position).getUgstPercetage() + "%");

                        }   }

                }

                else {

                    if (GSTListDetailsList.get(position).getIgst() != null) {


                        holder.llIgst.setVisibility(View.VISIBLE);
                        holder.tv_Igst_infigure.setText("₹ " + GSTListDetailsList.get(position).getIgst());
                        //  holder.tv_Igst_infigure.setText(Igstlist.get(position).toString());
                        holder.tv_Igst_percentage.setText("IGST - " + GSTListDetailsList.get(position).getIgstPer() + "%");

                    }
                }
            }

            else if (stateCode.equalsIgnoreCase(wender_state_code)) {
                holder.llCgst.setVisibility(View.VISIBLE);
                holder.llSgst.setVisibility(View.VISIBLE);

                if (GSTListDetailsList.get(position).getCgst() != null) {


                    holder.cgst_tax_value.setText("₹ " + GSTListDetailsList.get(position).getCgst());
                    holder.tv_cgst_infigure.setText("₹ " + GSTListDetailsList.get(position).getCgst());
                    holder.tv_cgst_percentage.setText("CGST - " + GSTListDetailsList.get(position).getCgstPer() + "%");

                    if (GSTListDetailsList.get(position).getSgst() != null) {


                        holder.sgst_tax_value.setText("₹ " + GSTListDetailsList.get(position).getSgst());
                        holder.tv_sgst_infigure.setText("₹ " + GSTListDetailsList.get(position).getSgst());
                        holder.tv_sgst_percentage.setText("SGST - " + GSTListDetailsList.get(position).getSgstPer() + "%");

                    }
                }
            }


            else {
                if (GSTListDetailsList.get(position).getIgst() != null) {


                    holder.llIgst.setVisibility(View.VISIBLE);
                    holder.tv_Igst_infigure.setText("₹ " + GSTListDetailsList.get(position).getIgst());
                    //  holder.tv_Igst_infigure.setText(Igstlist.get(position).toString());
                    holder.tv_Igst_percentage.setText("IGST - " + GSTListDetailsList.get(position).getIgstPer() + "%");


                }
            }




        }
        }



    @Override
    public int getItemCount() {
        return GSTListDetailsList.size();
      //  return 5;

    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cgst_infigure,tv_cgst_percentage,
                cgst_tax_value,tv_sgst_infigure,tv_sgst_percentage,sgst_tax_value,
        Ugst_tax_value,Igst_tax_value,
                tv_Igst_percentage,tv_Ugst_percentage,tv_Ugst_infigure,tv_Igst_infigure;
        private LinearLayout llCgst,llIgst,llSgst,llUgst;
        public MyViewHolder(View view) {
            super(view);
            tv_sgst_infigure = (TextView) view.findViewById(R.id.tv_sgst_infigure);
            tv_cgst_infigure = (TextView) view.findViewById(R.id.tv_cgst_infigure);
            tv_Ugst_infigure = (TextView) view.findViewById(R.id.tv_Ugst_infigure);
            tv_Igst_infigure = (TextView) view.findViewById(R.id.tv_Igst_infigure);

            tv_sgst_percentage = (TextView) view.findViewById(R.id.tv_sgst_percentage);
            tv_cgst_percentage = (TextView) view.findViewById(R.id.tv_cgst_percentage);
            tv_Igst_percentage = (TextView) view.findViewById(R.id.tv_Igst_percentage);
            tv_Ugst_percentage = (TextView) view.findViewById(R.id.tv_Ugst_percentage);

            cgst_tax_value = (TextView) view.findViewById(R.id.cgst_tax_value);
            sgst_tax_value = (TextView) view.findViewById(R.id.sgst_tax_value);
            Igst_tax_value = (TextView) view.findViewById(R.id.Igst_tax_value);
            Ugst_tax_value = (TextView) view.findViewById(R.id.Ugst_tax_value);
            llUgst = (LinearLayout) view.findViewById(R.id.llUgst);
            llIgst = (LinearLayout) view.findViewById(R.id.llIgst);
            llSgst = (LinearLayout) view.findViewById(R.id.llSgst);
            llCgst = (LinearLayout) view.findViewById(R.id.llCgst);
        }
    }
}
