package gstapp.com.Adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import UtilClasses.AppConstant;
import UtilClasses.CommonUtils;
import gstapp.com.fragments.UploadBills;
import gstapp.com.fragments.ViewAllPurchaseBills;

public class PurchaseAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    Context context;

    public PurchaseAdapter(FragmentManager fm, int NumOfTabs,Context context) {

        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.context=context;

    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ViewAllPurchaseBills tab1 = ViewAllPurchaseBills.newInstance();

                CommonUtils.saveStringPreferences(context,AppConstant.SELECTED_TAB,"view_purchaged_bill");
                Log.d("TAG","first");
                return tab1;
            case 1:
                UploadBills beginnersNewTab = UploadBills.newInstance();
                CommonUtils.saveStringPreferences(context,AppConstant.SELECTED_TAB,"upload_purchaged_bill");
                Log.d("TAG","second");
                return beginnersNewTab;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        getItem(position).onDetach();

        //super.destroyItem(container, position, object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        //super.restoreState(state, loader);
    }
}
