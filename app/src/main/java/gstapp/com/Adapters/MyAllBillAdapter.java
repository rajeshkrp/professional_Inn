package gstapp.com.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ModelClasses.AllBillsListPojo;
import gstapp.com.fragments.ViewAllPurchaseBills;
import gstapp.com.gstapp.R;


/**
 * Created by Abdul on 4/5/2018.
 */

public class MyAllBillAdapter extends RecyclerView.Adapter {

    Context context;
    List<AllBillsListPojo> allBillsListPojos;
    ArrayList<String> imagesList;
    ViewAllPurchaseBills viewAllPurchaseBills;
    ArrayList<String> categoryList;
    ArrayList<ArrayList<String>> positionList = new ArrayList<>();

    public MyAllBillAdapter(Context context, List<AllBillsListPojo> allBillsListPojos, ViewAllPurchaseBills viewAllPurchaseBills, ArrayList<String> categoryList) {

        this.context = context;
        this.allBillsListPojos = allBillsListPojos;
        this.viewAllPurchaseBills = viewAllPurchaseBills;
        this.categoryList = categoryList;

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.my_all_list_items, parent, false);
        AllViewHolder allViewHolder = new AllViewHolder(view);
        return allViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        AllViewHolder allViewHolder = (AllViewHolder) holder;

        imagesList = new ArrayList<>();

        if(allBillsListPojos!=null&&allBillsListPojos.size()>0) {

            if(allBillsListPojos.get(position).getCreate_at()!=null) {

                String date[]=allBillsListPojos.get(position).getCreate_at().split(" ");

                allViewHolder.create_at.setText(date[0]);
            }

            String category = categoryList.get(position);
            for (int i = 0; i < allBillsListPojos.size(); i++) {
                if (allBillsListPojos.get(i).getCategory().equals(category)) {
                    if (!imagesList.contains(allBillsListPojos.get(i).getBills())) {
                        imagesList.add(allBillsListPojos.get(i).getBills());
                    }
                }
            }
            positionList.add(imagesList);
            if(positionList!=null&&positionList.size()>0) {

                Glide.with(context).load(imagesList.get(0)).into(allViewHolder.firstBillImage);
            }


            allViewHolder.categotyTxt.setText(category);

            if (imagesList.size() > 1 && imagesList.size() > 0) {
                allViewHolder.moreImages.setText(" + " + (imagesList.size() - 1) + "");
            } else {
                allViewHolder.moreImages.setVisibility(View.GONE);
            }
        }

        allViewHolder.moreImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewAllPurchaseBills.showGallery(positionList.get(position), categoryList.get(position));
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    private class AllViewHolder extends RecyclerView.ViewHolder {

        ImageView firstBillImage;
        TextView create_at, categotyTxt, moreImages;

        public AllViewHolder(View itemView) {
            super(itemView);

            firstBillImage = (ImageView) itemView.findViewById(R.id.firstBillImage);
            create_at = (TextView) itemView.findViewById(R.id.create_at);
            categotyTxt = (TextView) itemView.findViewById(R.id.categotyTxt);
            moreImages = (TextView) itemView.findViewById(R.id.moreImagesTxt);
        }
    }

}
