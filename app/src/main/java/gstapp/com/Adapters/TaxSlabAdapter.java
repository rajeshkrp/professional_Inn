package gstapp.com.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ModelClasses.BillPaperModel;
import UtilClasses.CommonUtils;
import UtilClasses.InvoiceTempData;
import UtilClasses.LogeedUserDetails;
import gstapp.com.gstapp.R;

public class TaxSlabAdapter extends RecyclerView.Adapter<TaxSlabAdapter.MyViewHolder> {
    List<InvoiceTempData> InvoiceTempDataList;
    String wender_state_code;
    SharedPreferences preferences;
    String stateCode;
    private Context context1;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tax_slab_row_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public TaxSlabAdapter(List<InvoiceTempData> InvoiceTempDataList, Context context, String stateCode) {
        this.InvoiceTempDataList = InvoiceTempDataList;
        this.context1 = context;
        this.stateCode=stateCode;

        LogeedUserDetails s=new LogeedUserDetails();
        s.getState_code();
    /*    preferences = PreferenceManager.getDefaultSharedPreferences(context);
        wender_state_code=preferences.getString("state_code","");
        wender_state_code=CommonUtils.getPreferencesString(context,"STATE_CODE");*/

        wender_state_code=CommonUtils.getPreferencesString(context,"Admin_status_code");




        System.out.println(wender_state_code);
        Log.e("TAG",wender_state_code);

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        InvoiceTempData paper = InvoiceTempDataList.get(position);

        // double totalGst=0.0;
         String totalGst="";
        int count=0;


        if(InvoiceTempDataList!=null&&InvoiceTempDataList.size()>0) {
            if (stateCode.equalsIgnoreCase("35")
                    || stateCode.equalsIgnoreCase("04")
                    || stateCode.equalsIgnoreCase("26")
                    || stateCode.equalsIgnoreCase("25")
                    || stateCode.equalsIgnoreCase("31")
                    || stateCode.equalsIgnoreCase("34") && wender_state_code.equalsIgnoreCase("04")
                    || wender_state_code.equalsIgnoreCase("26")
                    || wender_state_code.equalsIgnoreCase("25")
                    || wender_state_code.equalsIgnoreCase("31")
                    || wender_state_code.equalsIgnoreCase("34")
                    || wender_state_code.equalsIgnoreCase("35")
                    ) {
                if (stateCode.equals(wender_state_code)) {

                    holder.llCgst.setVisibility(View.VISIBLE);
                    holder.llUgst.setVisibility(View.VISIBLE);

                    holder.cgst_tax_value.setText("₹ "+InvoiceTempDataList.get(position).getCGSTAmount());
                    holder.tv_cgst_infigure.setText("₹ "+InvoiceTempDataList.get(position).getCGSTAmountN());
                    holder.tv_cgst_percentage.setText("CGST - " + InvoiceTempDataList.get(position).getCGSTPer());

                    holder.Ugst_tax_value.setText("₹ "+InvoiceTempDataList.get(position).getUGSTAmount());
                    holder.tv_Ugst_infigure.setText("₹ "+InvoiceTempDataList.get(position).getUGSTAmountN());
                    holder.tv_Ugst_percentage.setText("UGST - " + InvoiceTempDataList.get(position).getUGSTPer());



                }


                else {

                    holder.llIgst.setVisibility(View.VISIBLE);
                    String igstvalue=InvoiceTempDataList.get(position).getIGSTPer();
                    ArrayList<String> igstist=new ArrayList<>();
                    holder.Igst_tax_value.setText("₹ "+InvoiceTempDataList.get(position).getIGSTAmount());
                  // holder.tv_Igst_infigure.setText(InvoiceTempDataList.get(position).getIGSTAmount());
                    holder.tv_Igst_infigure.setText("₹ "+InvoiceTempDataList.get(position).getIGSTAmountN());


                    holder.tv_Igst_percentage.setText("IGST  - " + InvoiceTempDataList.get(position).getSelectedCatgeory());

                    }
            }

                else if (stateCode.equalsIgnoreCase(wender_state_code)) {
                holder.llCgst.setVisibility(View.VISIBLE);
                holder.llSgst.setVisibility(View.VISIBLE);

                holder.cgst_tax_value.setText("₹ "+InvoiceTempDataList.get(position).getCGSTAmount());
                holder.tv_cgst_infigure.setText("₹ "+InvoiceTempDataList.get(position).getCGSTAmountN());


                holder.tv_cgst_percentage.setText("CGST - " + InvoiceTempDataList.get(position).getCGSTPer());
                holder.sgst_tax_value.setText("₹ "+InvoiceTempDataList.get(position).getSGSTAmount());
                holder.tv_sgst_infigure.setText("₹ "+InvoiceTempDataList.get(position).getSGSTAmountN());
                holder.tv_sgst_percentage.setText("SGST - " + InvoiceTempDataList.get(position).getSGSTPer());

                }


                else {
                        holder.llIgst.setVisibility(View.VISIBLE);
                        holder.Igst_tax_value.setText("₹ "+InvoiceTempDataList.get(position).getIGSTAmount());
                       // holder.tv_Igst_infigure.setText(InvoiceTempDataList.get(position).getIGSTAmount());
                     holder.tv_Igst_infigure.setText("₹ "+InvoiceTempDataList.get(position).getIGSTAmountN());

                    holder.tv_Igst_percentage.setText("IGST - "+InvoiceTempDataList.get(position).getSelectedCatgeory());


                }


            }
        }



    @Override
    public int getItemCount() {
        return InvoiceTempDataList.size();
      //  return 5;

    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cgst_infigure,tv_cgst_percentage,
                cgst_tax_value,tv_sgst_infigure,tv_sgst_percentage,sgst_tax_value,
        Ugst_tax_value,Igst_tax_value,
                tv_Igst_percentage,tv_Ugst_percentage,tv_Ugst_infigure,tv_Igst_infigure;
        private LinearLayout llCgst,llIgst,llSgst,llUgst;
        public MyViewHolder(View view) {
            super(view);
            tv_sgst_infigure = (TextView) view.findViewById(R.id.tv_sgst_infigure);
            tv_cgst_infigure = (TextView) view.findViewById(R.id.tv_cgst_infigure);
            tv_Ugst_infigure = (TextView) view.findViewById(R.id.tv_Ugst_infigure);
            tv_Igst_infigure = (TextView) view.findViewById(R.id.tv_Igst_infigure);

            tv_sgst_percentage = (TextView) view.findViewById(R.id.tv_sgst_percentage);
            tv_cgst_percentage = (TextView) view.findViewById(R.id.tv_cgst_percentage);
            tv_Igst_percentage = (TextView) view.findViewById(R.id.tv_Igst_percentage);
            tv_Ugst_percentage = (TextView) view.findViewById(R.id.tv_Ugst_percentage);

            cgst_tax_value = (TextView) view.findViewById(R.id.cgst_tax_value);
            sgst_tax_value = (TextView) view.findViewById(R.id.sgst_tax_value);
            Igst_tax_value = (TextView) view.findViewById(R.id.Igst_tax_value);
            Ugst_tax_value = (TextView) view.findViewById(R.id.Ugst_tax_value);
            llUgst = (LinearLayout) view.findViewById(R.id.llUgst);
            llIgst = (LinearLayout) view.findViewById(R.id.llIgst);
            llSgst = (LinearLayout) view.findViewById(R.id.llSgst);
            llCgst = (LinearLayout) view.findViewById(R.id.llCgst);
        }
    }
}
