package gstapp.com.gstapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import com.facebook.drawee.backends.pipeline.Fresco;

import UtilClasses.CommonUtils;
import gstapp.com.login_module.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Fresco.initialize(this);
        //GifImageView gifImageView = (GifImageView) findViewById(R.id.gifImage);
        Uri uri = Uri.parse("https://raw.githubusercontent.com/facebook/fresco/master/docs/static/logo.png");
        //  SimpleDraweeView draweeView = (SimpleDraweeView) findViewById(R.id.my_image_view);
        // draweeView.setImageResource(R.drawable.spinner);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (CommonUtils.isLoggedIn(SplashActivity.this)) {
                    startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                    CommonUtils.isProfile = false;
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }

            }
        }, 5000);
    }
}
