package gstapp.com.gstapp;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import UtilClasses.CommonUtils;
import UtilClasses.ImageInterface;
import UtilClasses.LogeedUserDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.create_invoice_module.CreateInvoiceActivity;
import gstapp.com.fragments.AboutFragment;
import gstapp.com.fragments.CreateNewBill;
import gstapp.com.fragments.EditProfile;
import gstapp.com.fragments.HomeFragment;
import gstapp.com.fragments.MemberShipFragment;
import gstapp.com.fragments.MyBills;
import gstapp.com.fragments.MyProfile;
import gstapp.com.fragments.MyPurchase;
import gstapp.com.fragments.NewsFeedFragment;
import gstapp.com.fragments.Setting;
import gstapp.com.fragments.UploadBills;
import gstapp.com.login_module.LoginActivity;

public class DashBoardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, CreateNewBill.OnFragmentInteractionListener,
        HomeFragment.OnFragmentInteractionListener,
        MyProfile.OnFragmentInteractionListener,
        EditProfile.OnFragmentInteractionListener {

    ImageInterface imageInterface;

    @BindView(R.id.log_out)
    LinearLayout linearLayout;


    @BindView(R.id.user_name)
    TextView userName;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.headertitle)
    public TextView headertitle;
    private Context context;


    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=DashBoardActivity.this;

        setContentView(R.layout.activity_dash_board);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        if(getIntent().getStringExtra("GST")!=null&&(!getIntent().getStringExtra("GST").equalsIgnoreCase(""))){



            ButterKnife.bind(this);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.menu_96);

            headertitle.setVisibility(View.GONE);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.menu_96);


            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                }
            });

            replaceMyBillFragment();

            LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(DashBoardActivity.this);
            userName.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getMName() + " " + logeedUserDetails.getLName());

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(DashBoardActivity.this, LoginActivity.class));
                    CommonUtils.makeUserLogout(DashBoardActivity.this);
                    finish();
                }
            });




            }
        else {


            ButterKnife.bind(this);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.menu_96);

            headertitle.setVisibility(View.GONE);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.menu_96);


            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                }
            });

            addHomeFragment();

            LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(DashBoardActivity.this);
            userName.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getMName() + " " + logeedUserDetails.getLName());

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivity(new Intent(DashBoardActivity.this, LoginActivity.class));
                    CommonUtils.makeUserLogout(DashBoardActivity.this);
                    finish();
                }
            });


        }
    }





    private void addHomeFragment() {


        HomeFragment homeFragment = HomeFragment.newInstance();
        getSupportFragmentManager().beginTransaction().add(R.id.mainLayout, homeFragment).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (CommonUtils.isProfile) {
            replaceProfileFragment();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

        getSupportFragmentManager().popBackStack();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {

            // Handle the camera action
            replaceProfileFragment();
        } else if (id == R.id.nav_purchases) {


            replacePurchase();
        } else if (id == R.id.nav_my_bills) {

            replaceMyBillFragment();
        } else if (id == R.id.nav_setting) {

            replaceSettingFragment();
        } else if (id == R.id.nav_create_new) {
            replaceBillFragment();

        } else if (id == R.id.nav_about_us) {

            replaceAboutFragment();
        } else if (id == R.id.nav_home) {
            replaceHomeFragment();
        }else if(id==R.id.nav_pay_gst){

            startActivity(new Intent(DashBoardActivity.this,PayGstActivity.class));

        }else if(id==R.id.nav_e_way){

            startActivity(new Intent(DashBoardActivity.this,EwayActivity.class));

           /* Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.setData(Uri.parse("https://ewaybillgst.gov.in/Others/EBPrintnew.aspx"));
            startActivity(intent);*/

        }else if(id==R.id.nav_membership){


            replaceMemberShipFragment();
        }else if(id==R.id.nav_news_feeds){


            replaceNewsFeed();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void replaceNewsFeed() {
        headertitle.setVisibility(View.VISIBLE);
        headertitle.setText("News Feedback");
        NewsFeedFragment newsFeedFragment=new NewsFeedFragment();
        DashBoardActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout,newsFeedFragment).commit();


    }


    private void replaceMemberShipFragment(){

        headertitle.setVisibility(View.VISIBLE);
        headertitle.setText("Membership");
        MemberShipFragment memberShipFragment=new MemberShipFragment();
        DashBoardActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout,memberShipFragment).commit();

    }


    private void replacePurchase() {


        headertitle.setVisibility(View.VISIBLE);
        headertitle.setText("My Purchases");
        MyPurchase myPurchase = MyPurchase.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, myPurchase).commit();
    }

    private void replaceMyBillFragment() {


        headertitle.setVisibility(View.VISIBLE);
        headertitle.setText("My Bills");
        MyBills homeFragment = MyBills.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, homeFragment).commit();
    }
    private void GSTMyBillFragment() {

      /*  headertitle.setVisibility(View.VISIBLE);
        headertitle.setText("My Bills");*/
        MyBills myBills = MyBills.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, myBills).commit();
    }


    private void replaceSettingFragment() {


        headertitle.setVisibility(View.VISIBLE);
        headertitle.setText("Settings");
        Setting homeFragment = Setting.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, homeFragment).commit();
    }

    private void replaceAboutFragment() {
        headertitle.setVisibility(View.VISIBLE);
        headertitle.setText("About Professional Inn");
        AboutFragment homeFragment = AboutFragment.newInstance(null);
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, homeFragment).commit();
    }

    private void replaceHomeFragment() {
        headertitle.setText("Home");
        HomeFragment homeFragment = HomeFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, homeFragment).commit();

    }

    private void replaceBillFragment() {

   //     Toast.makeText(this, "toast", Toast.LENGTH_SHORT).show();
         CommonUtils.saveStringPreferences(context,"shipState","");
          CommonUtils.saveStringPreferences(context,"billState","");

       /* headertitle.setText("Create New Invoice");
        CreateNewBill homeFragment = new CreateNewBill();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, homeFragment).commit();*/

        startActivity(new Intent(DashBoardActivity.this, CreateInvoiceActivity.class));

    }

    public void replaceProfileFragment() {
        headertitle.setVisibility(View.VISIBLE);
        CommonUtils.isProfile = false;
        headertitle.setText("Manage Company Details");
        MyProfile homeFragment = MyProfile.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, homeFragment).commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 200 && resultCode == RESULT_OK) {

            Uri uri = data.getData();
            EditProfile editProfile = EditProfile.newInstance();
            editProfile.onActivityResult(requestCode, resultCode, data);

        } else if (requestCode == 42141 && resultCode == RESULT_OK) {
            Uri uri = data.getData();

            MyPurchase myPurchase = MyPurchase.newInstance();
            myPurchase.onActivityResult(requestCode, resultCode, data);
        } else {

            UploadBills uploadBills = UploadBills.newInstance();
            uploadBills.onActivityResult(requestCode, resultCode, data);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
