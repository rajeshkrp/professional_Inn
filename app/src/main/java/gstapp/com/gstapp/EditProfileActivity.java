package gstapp.com.gstapp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import ModelClasses.UserProfile;
import UtilClasses.AppConstant;
import UtilClasses.CommonUtils;
import UtilClasses.GSTINValidator;
import UtilClasses.ImageInterface;
import UtilClasses.LogeedUserDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import gstapp.com.Adapters.StateCodeAdapter;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class EditProfileActivity extends AppCompatActivity {

    public static final String GSTINFORMAT_REGEX = "[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}";
    public static final String GSTN_CODEPOINT_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private TagContainerLayout mTagContainerLayout1;
    @BindView(R.id.headertitle)
    TextView headertitle;
    @BindView(R.id.log_out)
    LinearLayout logOut;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_add_mor_add)
    TextView tvAddMorAdd;
    @BindView(R.id.com_add2_edit)
    EditText comAdd2Edit;
    @BindView(R.id.ll_container_seco_address)
    LinearLayout llContainerSecoAddress;
    @BindView(R.id.com_add3_edit)
    EditText comAdd3Edit;
    @BindView(R.id.ll_container_third_address)
    LinearLayout llContainerThirdAddress;
    @BindView(R.id.com_add4_edit)
    EditText comAdd4Edit;
    @BindView(R.id.ll_container_fourth_address)
    LinearLayout llContainerFourthAddress;
    @BindView(R.id.com_add5_edit)
    EditText comAdd5Edit;
    @BindView(R.id.ll_container_fifth_address)
    LinearLayout llContainerFifthAddress;
    @BindView(R.id.ll_container)
    LinearLayout llContainer;
    @BindView(R.id.tv_add_mo2)
    TextView tvAddMo2;
    @BindView(R.id.tv_add_mor3)
    TextView tvAddMor3;
    @BindView(R.id.tv_add_mor4)
    TextView tvAddMor4;
    @BindView(R.id.radioGoods_services)
    RadioGroup radioGoods_services;
    @BindView(R.id.radio1)
    RadioButton radio1;
    @BindView(R.id.radio2)
    RadioButton radio2;
    @BindView(R.id.radio3)
    RadioButton radio3;
    @BindView(R.id.tv_hsn)
    TextView tvHsn;
    @BindView(R.id.tv_add_mor_bill1)
    TextView tvAddMorBill1;
    @BindView(R.id.ll_bill_series_no1)
    LinearLayout llBillSeriesNo1;
    @BindView(R.id.com_bill_no2_edit)
    EditText comBillNo2Edit;
    @BindView(R.id.tv_add_mor_bill2)
    TextView tvAddMorBill2;
    @BindView(R.id.ll_bill_series_no2)
    LinearLayout llBillSeriesNo2;
    @BindView(R.id.com_bill_no3_edit)
    EditText comBillNo3Edit;
    @BindView(R.id.tv_add_mor_bill3)
    TextView tvAddMorBill3;
    @BindView(R.id.ll_bill_series_no3)
    LinearLayout llBillSeriesNo3;
    @BindView(R.id.com_bill_no4_edit)
    EditText comBillNo4Edit;
    @BindView(R.id.tv_add_mor_bill4)
    TextView tvAddMorBill4;
    @BindView(R.id.ll_bill_series_no4)
    LinearLayout llBillSeriesNo4;
    @BindView(R.id.com_bill_no5_edit)
    EditText comBillNo5Edit;
    @BindView(R.id.tv_add_mor_bill5)
    TextView tvAddMorBill5;
    @BindView(R.id.ll_bill_series_no5)
    LinearLayout llBillSeriesNo5;
    @BindView(R.id.com_bill_no6_edit)
    EditText comBillNo6Edit;
    @BindView(R.id.tv_add_mor_bill6)
    TextView tvAddMorBill6;
    @BindView(R.id.ll_bill_series_no6)
    LinearLayout llBillSeriesNo6;
    @BindView(R.id.com_bill_no7_edit)
    EditText comBillNo7Edit;
    @BindView(R.id.tv_add_mor_bill7)
    TextView tvAddMorBill7;
    @BindView(R.id.ll_bill_series_no7)
    LinearLayout llBillSeriesNo7;
    @BindView(R.id.com_bill_no8_edit)
    EditText comBillNo8Edit;
    @BindView(R.id.tv_add_mor_bill8)
    TextView tvAddMorBill8;
    @BindView(R.id.ll_bill_series_no8)
    LinearLayout llBillSeriesNo8;
    @BindView(R.id.com_bill_no9_edit)
    EditText comBillNo9Edit;
    @BindView(R.id.tv_add_mor_bill9)
    TextView tvAddMorBill9;
    @BindView(R.id.ll_bill_series_no9)
    LinearLayout llBillSeriesNo9;
    @BindView(R.id.com_bill_no10_edit)
    EditText comBillNo10Edit;
    @BindView(R.id.tv_add_mor_bill10)
    TextView tvAddMorBill10;
    @BindView(R.id.ll_bill_series_no10)
    LinearLayout llBillSeriesNo10;


    private ProgressDialog progressDialog;

    @BindView(R.id.com_name_edit)
    EditText com_name_edit;

    /*@BindView(R.id.sate_spinner)
    AppCompatSpinner stateSpinner;*/

    @BindView(R.id.acc_type_spinner)
    AppCompatSpinner acc_type_spinner;

    @BindView(R.id.com_phone_edit)
    EditText com_phone_edit;

    @BindView(R.id.com_email_edit)
    EditText com_email_edit;

    @BindView(R.id.com_GSTN_edit)
    EditText com_GSTN_edit;

    @BindView(R.id.com_CIN_edit)
    EditText com_CIN_edit;

    @BindView(R.id.com_PAN_edit)
    EditText com_PAN_edit;

    @BindView(R.id.com_add_edit)
    EditText com_add_edit;

    @BindView(R.id.com_state_code_edit)
    EditText com_state_code_edit;

    @BindView(R.id.com_bank_name_edit)
    EditText com_bank_name_edit;

    @BindView(R.id.com_acc_name_edit)
    EditText com_acc_name_edit;

    @BindView(R.id.com_branch_edit)
    EditText com_branch_edit;

    @BindView(R.id.com_acc_no_edit)
    EditText com_acc_no_edit;

    @BindView(R.id.com_IFSC_edit)
    EditText com_IFSC_edit;

    @BindView(R.id.com_bill_no_edit)
    EditText com_bill_no_edit;

    @BindView(R.id.update_button)
    Button update_button;

    @BindView(R.id.com_logo_edit)
    ImageView com_logo_edit;

    @BindView(R.id.ivEdit)
    ImageView ivEdit;

    @BindView(R.id.backEdit)
    ImageView backEdit;

    @BindView(R.id.user_name)
    TextView user_name;

    @BindView(R.id.stateNameTxt)
    TextView stateNameTxt;

    ApiInterface apiInterface;

    ImageInterface imageInterface;

    private final int PICK_FROM_GALLERY = 200;

    LinkedList<String> ahaha = new LinkedList<>();

    MultipartBody.Part imagePart;
    private Uri uri;
    HashMap<String, String> stateCodes;
    private String stateName, accountType;
    private StateCodeAdapter clad;
    private Context context;
    FloatingActionButton fbHome;
    GSTINValidator gstinValidator;
    String gstinNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        context = EditProfileActivity.this;
        fbHome = findViewById(R.id.fbHome);
        mTagContainerLayout1 = (TagContainerLayout) findViewById(R.id.tagcontainerLayout1);
       // mTagContainerLayout.setTags(List<String> tags);
        progressDialog = new ProgressDialog(EditProfileActivity.this);
        progressDialog.setMessage("Updating Details");
        progressDialog.setCancelable(false);
        gstinValidator = new GSTINValidator();

        findViewById(R.id.ll_container).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return true;
            }
        });

        mTagContainerLayout1.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                Toast.makeText(EditProfileActivity.this, "click-position:" + position + ", text:" + text,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTagLongClick(final int position, String text) {
                AlertDialog dialog = new AlertDialog.Builder(EditProfileActivity.this)
                        .setTitle("long click")
                        .setMessage("You will delete this tag!")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (position < mTagContainerLayout1.getChildCount()) {
                                    mTagContainerLayout1.removeTag(position);
                                }
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                dialog.show();
            }

            @Override
            public void onSelectedTagDrag(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {
//                mTagContainerLayout1.removeTag(position);
                Toast.makeText(EditProfileActivity.this, "Click TagView cross! position = " + position,
                        Toast.LENGTH_SHORT).show();
            }
        });


        gstinNo = com_GSTN_edit.getText().toString();
//CommonUtils.hideSoftKeyboard(EditProfileActivity.this);
        fbHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, DashBoardActivity.class);
                startActivity(i);
            }
        });

        List<UserProfile> userProfile = getIntent().getParcelableArrayListExtra("compnyData");


        stateCodes = new HashMap<>();
        ArrayList<String> accTypeList = new ArrayList<>();
        accTypeList.add("Saving");
        accTypeList.add("Current");
        accTypeList.add("CC");


        try {
            String cName = userProfile.get(0).getCompany_name();
            prepareHashMap(stateCodes);
            if (userProfile.get(0).getCompany_logo() != null && !userProfile.get(0).getCompany_logo().equals("")) {

                Glide.with(context)
                        .load(userProfile.get(0).getCompany_logo()).error(R.drawable.ic_defalt_user)
                        .into(com_logo_edit);
            } else {

                // com_logo_edit.setImageResource(R.drawable.ic_defalt_user);
            }

            com_name_edit.setText(userProfile.get(0).getCompany_name());
            com_add_edit.setText(userProfile.get(0).getAddress());
            com_phone_edit.setText(userProfile.get(0).getMobile());
            com_email_edit.setText(userProfile.get(0).getEmail());
            com_GSTN_edit.setText(userProfile.get(0).getGstn());
            com_CIN_edit.setText(userProfile.get(0).getCin());
            com_PAN_edit.setText(userProfile.get(0).getPan());
            //stateSpinner.setSelected(stateList.get());
            stateName = userProfile.get(0).getState();

            com_bank_name_edit.setText(userProfile.get(0).getBank_name());
            com_acc_name_edit.setText(userProfile.get(0).getFirst_name());
            com_branch_edit.setText(userProfile.get(0).getBank_branch());
            com_acc_no_edit.setText(userProfile.get(0).getAc_no());
            com_IFSC_edit.setText(userProfile.get(0).getIfsc_code());
            com_bill_no_edit.setText(userProfile.get(0).getBill_no_series());

            stateNameTxt.setText(userProfile.get(0).getState());

            Log.e("code", "code" + userProfile.get(0).getState_code());
            com_state_code_edit.setText(userProfile.get(0).getState_code());
        } catch (Exception e) {
            e.printStackTrace();
        }

       /* if(CommonUtils.getPreferencesString(context,"Admin_status")!=null){

            com_state_edit.setText(CommonUtils.getPreferencesString(context,"Admin_status"));
            com_state_code_edit.setText(CommonUtils.getPreferencesString(context,"Admin_status_code"));

        }*/

        LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(EditProfileActivity.this);
        user_name.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getMName() + " " + logeedUserDetails.getLName());

        backEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        apiInterface = APIClient.getClient().create(ApiInterface.class);

        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    validateAndUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        tvAddMorBill1.setOnClickListener(v -> {
            llBillSeriesNo2.setVisibility(View.VISIBLE);
            tvAddMorBill1.setVisibility(View.INVISIBLE);
        });
        tvAddMorBill2.setOnClickListener(v -> {
            if(comBillNo2Edit.getText().length()!=0){
                llBillSeriesNo3.setVisibility(View.VISIBLE);
                tvAddMorBill2.setVisibility(View.INVISIBLE);
            }else {
                CommonUtils.snackBar("Please Enter Bill No2 ", com_acc_name_edit, "300");
            }


        });

        tvAddMorBill3.setOnClickListener(v -> {
            if(comBillNo3Edit.getText().length()!=0){
                llBillSeriesNo4.setVisibility(View.VISIBLE);
                tvAddMorBill3.setVisibility(View.INVISIBLE);
            }else {
                CommonUtils.snackBar("Please Enter Bill No3 ", com_acc_name_edit, "300");
            }

        });
        tvAddMorBill4.setOnClickListener(v -> {
            if(comBillNo4Edit.getText().length()!=0){
            llBillSeriesNo5.setVisibility(View.VISIBLE);
            tvAddMorBill4.setVisibility(View.INVISIBLE);}else {
                CommonUtils.snackBar("Please Enter Bill No4 ", com_acc_name_edit, "300");
            }
        });
        tvAddMorBill5.setOnClickListener(v -> {

            if(comBillNo5Edit.getText().length()!=0){
                llBillSeriesNo6.setVisibility(View.VISIBLE);
                tvAddMorBill5.setVisibility(View.INVISIBLE);
            }else {
                CommonUtils.snackBar("Please Enter Bill No5 ", com_acc_name_edit, "300");
            }

        });
        tvAddMorBill6.setOnClickListener(v -> {
            if(comBillNo6Edit.getText().length()!=0 ||comBillNo6Edit.getText().length()<16){
                llBillSeriesNo7.setVisibility(View.VISIBLE);
                tvAddMorBill6.setVisibility(View.INVISIBLE);
            }else {
                CommonUtils.snackBar("Please Enter Bill No6 ", com_acc_name_edit, "300");
            }

        });
        tvAddMorBill7.setOnClickListener(v -> {
            if(comBillNo7Edit.getText().length()!=0 && comBillNo7Edit.getText().length()<17){
                llBillSeriesNo8.setVisibility(View.VISIBLE);
                tvAddMorBill7.setVisibility(View.INVISIBLE);
            }else {

                    CommonUtils.snackBar("Please Enter Bill No7 ", com_acc_name_edit, "300");

            }

        });
        tvAddMorBill8.setOnClickListener(v -> {
            if(comBillNo8Edit.getText().length()!=0 && comBillNo8Edit.getText().length()<17 ){
                llBillSeriesNo9.setVisibility(View.VISIBLE);
                tvAddMorBill8.setVisibility(View.INVISIBLE);
            }else {
                CommonUtils.snackBar("Please Enter Bill No8 ", com_acc_name_edit, "300");
            }

        });
        tvAddMorBill9.setOnClickListener(v -> {
            if(comBillNo8Edit.getText().length()!=0 && comBillNo8Edit.getText().length()<17){
                llBillSeriesNo10.setVisibility(View.INVISIBLE);
                tvAddMorBill9.setVisibility(View.INVISIBLE);
            }else {
                CommonUtils.snackBar("Please Enter Bill No9 ", com_acc_name_edit, "300");
            }

        });
        tvAddMorBill10.setOnClickListener(v -> {
            llBillSeriesNo10.setVisibility(View.INVISIBLE);
            tvAddMorBill10.setVisibility(View.INVISIBLE);
        });



        tvAddMorAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (com_add_edit.getText().length() != 0) {
                    llContainerSecoAddress.setVisibility(View.VISIBLE);
                    tvAddMorAdd.setVisibility(View.INVISIBLE);
                } else {
                    CommonUtils.snackBar("Please Enter Address", com_acc_name_edit, "300");
                }

            }

        });

        tvAddMo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comAdd2Edit.getText().length() != 0) {
                    llContainerThirdAddress.setVisibility(View.VISIBLE);
                    tvAddMo2.setVisibility(View.INVISIBLE);
                } else {
                    CommonUtils.snackBar("Please Enter Address", com_acc_name_edit, "300");
                }

            }
        });
        tvAddMor3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comAdd3Edit.getText().length() != 0) {
                    llContainerFourthAddress.setVisibility(View.VISIBLE);
                    tvAddMor3.setVisibility(View.INVISIBLE);
                } else {
                    CommonUtils.snackBar("Please Enter Address", com_acc_name_edit, "300");
                }
            }
        });
        tvAddMor4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comAdd4Edit.getText().length() != 0) {
                    llContainerFifthAddress.setVisibility(View.VISIBLE);
                    tvAddMor4.setVisibility(View.INVISIBLE);
                } else {
                    CommonUtils.snackBar("Please Enter Address", com_acc_name_edit, "300");
                }
            }
        });


        stateNameTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openStateDialog();
            }
        });

        ArrayAdapter adapter1 = new ArrayAdapter(context, R.layout.custom_spinner_layout, R.id.tvSpinner, accTypeList);
        adapter1.setDropDownViewResource(R.layout.custom_spinner_layout);
        acc_type_spinner.setAdapter(adapter1);






        /*ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.custom_spinner_layout, stateList);
        stateSpinner.setAdapter(arrayAdapter);*/
/*
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.custom_spinner_layout, accTypeList);
        acc_type_spinner.setAdapter(typeAdapter);*/

        /*stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String content = stateSpinner.getSelectedItem().toString();
                stateName = content;
                com_state_code_edit.setText(stateCodes.get(content));
                //  String newStateCode = stateCodes.get(content);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        acc_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String content = acc_type_spinner.getSelectedItem().toString();

                CommonUtils.saveStringPreferences(context, AppConstant.ACCOUNT_TYPE, content);
                accountType = content;

                //   com_state_code_edit.setText(stateCodes.get(content));
                String newStateCode = stateCodes.get(content);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Method to get the check digit for the gstin (without checkdigit)
     *
     * @param gstinWOCheckDigit
     * @return : GSTIN with check digit
     * @throws Exception
     */
    public static String getGSTINWithCheckDigit(String gstinWOCheckDigit) throws Exception {
        int factor = 2;
        int sum = 0;
        int checkCodePoint = 0;
        char[] cpChars;
        char[] inputChars;

        try {
            if (gstinWOCheckDigit == null) {
                throw new Exception("GSTIN supplied for checkdigit calculation is null");
            }
            cpChars = GSTN_CODEPOINT_CHARS.toCharArray();
            inputChars = gstinWOCheckDigit.trim().toUpperCase().toCharArray();

            int mod = cpChars.length;
            for (int i = inputChars.length - 1; i >= 0; i--) {
                int codePoint = -1;
                for (int j = 0; j < cpChars.length; j++) {
                    if (cpChars[j] == inputChars[i]) {
                        codePoint = j;
                    }
                }
                int digit = factor * codePoint;
                factor = (factor == 2) ? 1 : 2;
                digit = (digit / mod) + (digit % mod);
                sum += digit;
            }
            checkCodePoint = (mod - (sum % mod)) % mod;
            return gstinWOCheckDigit + cpChars[checkCodePoint];
        } finally {
            inputChars = null;
            cpChars = null;
        }
    }


    /**
     * Method to check if an input string matches the regex pattern passed
     *
     * @param inputval
     * @param regxpatrn
     * @return boolean
     */
    public static boolean checkPattern(String inputval, String regxpatrn) {
        boolean result = false;
        if ((inputval.trim()).matches(regxpatrn)) {
            result = true;
        }
        return result;
    }

    /**
     * Method for checkDigit verification.
     *
     * @param gstinWCheckDigit
     * @return
     * @throws Exception
     */
    private static boolean verifyCheckDigit(String gstinWCheckDigit) throws Exception {
        Boolean isCDValid = false;
        String newGstninWCheckDigit = getGSTINWithCheckDigit(
                gstinWCheckDigit.substring(0, gstinWCheckDigit.length() - 1));

        if (gstinWCheckDigit.trim().equals(newGstninWCheckDigit)) {
            isCDValid = true;
        }
        return isCDValid;
    }

    /**
     * Method to check if a GSTIN is valid. Checks the GSTIN format and the
     * check digit is valid for the passed input GSTIN
     *
     * @param gstin
     * @return boolean - valid or not
     * @throws Exception
     */
    private static boolean validGSTIN(String gstin) throws Exception {
        boolean isValidFormat = false;
        if (checkPattern(gstin, GSTINFORMAT_REGEX)) {
            isValidFormat = verifyCheckDigit(gstin);
        }
        return isValidFormat;

    }

    private void openStateDialog() {

        final ArrayList<String> stateList = new ArrayList<>();
        prepareStateList(stateList);

        ListView marital_lv;

        final Dialog dialog = new Dialog(EditProfileActivity.this, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_occupation);
        // dialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        ImageView leftarrow = (ImageView) dialog.findViewById(R.id.leftarrow);
        final EditText editMobileNo = (EditText) dialog.findViewById(R.id.editMobileNo);
        marital_lv = (ListView) dialog.findViewById(R.id.listview);
        //TextView saveImage = (TextView) dialog.findViewById(R.id.saveImage);
        clad = new StateCodeAdapter(EditProfileActivity.this, stateList);
        marital_lv.setAdapter(clad);

        editMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                clad.filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*if ((muserDetails.getMarital_status() != null)) {
            clad.setSelection(muserDetails.getMarital_status());
        }*/

        marital_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //  visibleGreenTick();
               /* clad.selectedPosition = position;
                maritalValues = stateList[position];
                editor.putString("Marital_status", maritalValues);
                editor.commit();
                clad.notifyDataSetChanged();*/
                stateName = stateList.get(position);
                stateNameTxt.setText(stateName);
                com_state_code_edit.setText(stateCodes.get(stateName));

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                dialog.dismiss();

            }
        });


        /*saveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // maritalStatus.setText(maritalValues);
                dialog.dismiss();
            }
        });*/

        dialog.show();

    }

    private void validateAndUpdate() throws Exception {

        if (com_logo_edit.getDrawable() == null) {
            CommonUtils.snackBar("Please upload Company logo ", com_acc_name_edit, "300");
        } else if (com_acc_name_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Name", com_acc_name_edit, "300");
        } else if (com_add_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Address", com_acc_name_edit, "300");
        } else if (com_phone_edit.getText().length() <= 7 || com_phone_edit.getText().length() >= 15) {
            CommonUtils.snackBar("Please Enter Phone No between 7 to 15 digit ", com_acc_name_edit, "300");
        }/*else if(){
            CommonUtils.snackBar("Please Enter Phone No between 7 to 15 digit", com_acc_name_edit, "300");
        }*/ else if (com_GSTN_edit.length() == 0 || com_GSTN_edit.length() > 15) {
            CommonUtils.snackBar("Please Enter 15 Character GSTN No", com_acc_name_edit, "300");
        } else if (!validGSTIN(com_GSTN_edit.getText().toString())) {
            CommonUtils.snackBar("Please Enter valid GSTN No", com_acc_name_edit, "300");
        } else if (com_CIN_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter CIN", com_acc_name_edit, "300");
        } else if (com_PAN_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter PAN", com_acc_name_edit, "300");
        } else if (com_bank_name_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Bank Name", com_acc_name_edit, "300");
        } else if (radioGoods_services.getCheckedRadioButtonId() == -1) {
            CommonUtils.snackBar("Please Select Atleast one Goods and services", com_acc_name_edit, "300");
        } else if (com_acc_name_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Account Holder Name", com_acc_name_edit, "300");
        } else if (com_branch_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Branch Name", com_acc_name_edit, "300");
        } else if (com_acc_no_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Account No", com_acc_name_edit, "300");
        } else if (com_IFSC_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter IFSC code", com_acc_name_edit, "300");
        } else if (com_bill_no_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Bill Serial No", com_acc_name_edit, "300");
        } else {
            progressDialog.show();
            updateUserData();
        }
    }

    private void prepareHashMap(HashMap<String, String> stateCodes) {

        stateCodes.put("Andaman and Nicobar Islands", "35");
        stateCodes.put("Andhra Pradesh", "37");
        stateCodes.put("Arunachal Pradesh", "12");
        stateCodes.put("Assam", "18");
        stateCodes.put("Bihar", "10");
        stateCodes.put("Chandigarh", "04");
        stateCodes.put("Chhattisgarh", "22");
        stateCodes.put("Dadra and Nagar Haveli", "26");
        stateCodes.put("Daman and Diu", "25");
        stateCodes.put("Delhi", "07");
        stateCodes.put("Goa", "30");
        stateCodes.put("Gujarat", "24");
        stateCodes.put("Haryana", "06");
        stateCodes.put("Himachal Pradesh", "02");
        stateCodes.put("Jammu & Kashmir", "01");
        stateCodes.put("Jharkhand", "20");
        stateCodes.put("Karnataka", "29");
        stateCodes.put("Kerala", "32");
        stateCodes.put("Lakshadweep Islands", "31");
        stateCodes.put("Madhya Pradesh", "23");
        stateCodes.put("Maharashtra", "27");
        stateCodes.put("Manipur", "14");
        stateCodes.put("Meghalaya", "17");
        stateCodes.put("Mizoramh", "15");
        stateCodes.put("Nagaland", "13");
        stateCodes.put("Odisha", "21");
        stateCodes.put("Pondicherry", "34");
        stateCodes.put("Punjab", "03");
        stateCodes.put("Rajasthan", "08");
        stateCodes.put("Sikkim", "11");
        stateCodes.put("Tamil Nadu", "33");
        stateCodes.put("Telangana", "36");
        stateCodes.put("Tripura", "16");
        stateCodes.put("Uttar Pradesh", "09");
        stateCodes.put("Uttarakhand", "05");
        stateCodes.put("West Bengal", "19");

    }

    private void prepareStateList(ArrayList<String> stateList) {

        stateList.add("Andaman and Nicobar Islands");
        stateList.add("Andhra Pradesh");
        stateList.add("Arunachal Pradesh");
        stateList.add("Assam");
        stateList.add("Bihar");
        stateList.add("Chandigarh");
        stateList.add("Chhattisgarh");
        stateList.add("Dadra and Nagar Haveli");
        stateList.add("Daman and Diu");
        stateList.add("Delhi");
        stateList.add("Goa");
        stateList.add("Gujarat");
        stateList.add("Haryana");
        stateList.add("Himachal Pradesh");
        stateList.add("Jammu & Kashmir");
        stateList.add("Jharkhand");
        stateList.add("Karnataka");
        stateList.add("Kerala");
        stateList.add("Lakshadweep Islands");
        stateList.add("Madhya Pradesh");
        stateList.add("Maharashtra");
        stateList.add("Manipur");
        stateList.add("Meghalaya");
        stateList.add("Mizoramh");
        stateList.add("Nagaland");
        stateList.add("Odisha");
        stateList.add("Pondicherry");
        stateList.add("Punjab");
        stateList.add("Rajasthan");
        stateList.add("Sikkim");
        stateList.add("Tamil Nadu");
        stateList.add("Telangana");
        stateList.add("Tripura");
        stateList.add("Uttar Pradesh");
        stateList.add("Uttarakhand");
        stateList.add("West Bengal");
    }


    private void openGallery() {

        getUserPermission();

    }

    private void getUserPermission() {

        try {
            if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
            } else {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateUserData() {

        String userId = CommonUtils.getUserId(EditProfileActivity.this);

        HashMap<String, String> hashMap = new HashMap<>();

        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), com_phone_edit.getText().toString());
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), com_add_edit.getText().toString());
        RequestBody com_name = RequestBody.create(MediaType.parse("text/plain"), com_name_edit.getText().toString());
        RequestBody gstn = RequestBody.create(MediaType.parse("text/plain"), com_GSTN_edit.getText().toString());
        RequestBody cin = RequestBody.create(MediaType.parse("text/plain"), com_CIN_edit.getText().toString());
        RequestBody pan = RequestBody.create(MediaType.parse("text/plain"), com_PAN_edit.getText().toString());
        RequestBody state_code = RequestBody.create(MediaType.parse("text/plain"), com_state_code_edit.getText().toString());
        RequestBody state = RequestBody.create(MediaType.parse("text/plain"), stateName);
        RequestBody b_name = RequestBody.create(MediaType.parse("text/plain"), com_bank_name_edit.getText().toString());
        RequestBody b_branch = RequestBody.create(MediaType.parse("text/plain"), com_branch_edit.getText().toString());
        RequestBody bill_no = RequestBody.create(MediaType.parse("text/plain"), com_bill_no_edit.getText().toString());
        RequestBody ac_no = RequestBody.create(MediaType.parse("text/plain"), com_acc_no_edit.getText().toString());
        RequestBody ifsc_code = RequestBody.create(MediaType.parse("text/plain"), com_IFSC_edit.getText().toString());
        RequestBody ac_type = RequestBody.create(MediaType.parse("text/plain"), accountType);

        saveBillNoToLocal(com_bill_no_edit.getText().toString());

        ArrayList<String> mSelectedImages = new ArrayList<>();
        MultipartBody.Part[] body = null;
        if (uri != null) {
            String path = getRealPathFromURIPath(uri, EditProfileActivity.this);
            mSelectedImages.add(path);

            body = prepareFilePartArray("profile_pic", mSelectedImages);
        }


        Call<ResponseBody> responseBodyCall = apiInterface.updateUserData(user_id, mobile, address, com_name, gstn, cin, pan,
                state_code, state, b_name, b_branch, bill_no, ac_no, ifsc_code, ac_type, body);

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }

                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    String message = jsonObject.getString("message");
                    CommonUtils.isProfile = true;
                    finish();
                    //Toast.makeText(EditProfileActivity.this, message, Toast.LENGTH_SHORT).show();


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                Log.e("Error", t.toString());
                Toast.makeText(EditProfileActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void saveBillNoToLocal(String s) {

        CommonUtils.saveBillNo(EditProfileActivity.this, s);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            uri = data.getData();
            try {
                String filePath = getRealPathFromURIPath(uri, EditProfileActivity.this);
                File file = new File(filePath);
                Log.d(TAG, "Filename " + file.getName());

                RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
                imagePart = fileToUpload;
                RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                com_logo_edit.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private String getRealPathFromURIPath(Uri contentURI, FragmentActivity activity) {

        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }


    @NonNull
    private MultipartBody.Part[] prepareFilePartArray(String partName, ArrayList<String> fileUri) {

        MultipartBody.Part[] arrayImage = new MultipartBody.Part[fileUri.size()];

        for (int index = 0; index < fileUri.size(); index++) {
            String image = fileUri.get(index);
            File file = new File(String.valueOf(image));
            String type = null;
            final String extension = MimeTypeMap.getFileExtensionFromUrl(image);
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
            }
            if (type == null) {
                type = "image/*"; // fallback type. You might set it to /
            }
            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse(type),
                            file
                    );
            arrayImage[index] = MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        }
        // MultipartBody.Part is used to send also the actual file name
        return arrayImage;


    }
}
