package gstapp.com.gstapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import gstapp.com.create_invoice_module.Customer_Details;
import gstapp.com.create_invoice_module.GstCalculation;
import gstapp.com.create_invoice_module.Product_Details;


public class MyStepperAdapter extends AbstractFragmentStepAdapter {


    public MyStepperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {

        switch (position) {
            case 0:
                final Customer_Details step2 = new Customer_Details();
                Bundle b1 = new Bundle();
                b1.putInt("CURRENT_STEP_POSITION_KEY", position);
                step2.setArguments(b1);
                return step2;

            case 1:
                final Product_Details step = Product_Details.newInstance();

                Bundle b = new Bundle();
                b.putInt("CURRENT_STEP_POSITION_KEY", position);
                step.setArguments(b);
              //  step.clenDta();


                return step;

            default:
                final GstCalculation step3 = new GstCalculation();
                Bundle b2 = new Bundle();
                b2.putInt("CURRENT_STEP_POSITION_KEY", position);
                step3.setArguments(b2);
                return step3;
        }



        /*Bundle b = new Bundle();
        b.putInt("CURRENT_STEP_POSITION_KEY", position);
        step.setArguments(b);
        return step;*/
    }

    @Override
    public int getCount() {
        return 3;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {

        //Override this method to set Step title for the Tabs, not necessary for other stepper types

        StepViewModel.Builder builder = new StepViewModel.Builder(context);

        if (position == 0) {
            builder.setTitle("Customer Details");
        } else if (position == 1) {
            builder.setTitle("Product Details");
        } else if (position == 2) {
            builder.setTitle("GST Tax")
                    .setEndButtonLabel("FINISH");
        }

        return builder.create();
    }
}
