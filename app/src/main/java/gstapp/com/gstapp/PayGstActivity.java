package gstapp.com.gstapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import UtilClasses.CommonUtils;
import UtilClasses.LogeedUserDetails;
import gstapp.com.login_module.LoginActivity;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 4/5/2018.
 */

public class PayGstActivity extends AppCompatActivity {

    ApiInterface apiInterface;
    TextView webView, user_name_pay;
    ImageView back;
    WebView web_View;
    LinearLayout linearLayout;
    private ProgressDialog progDailog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_gst);


        String url = "https://payment.gst.gov.in/payment";

        apiInterface = CommonUtils.InitilizeInterface();
        webView = (TextView) findViewById(R.id.textHtml);
        user_name_pay = (TextView) findViewById(R.id.user_name_pay);
        linearLayout = (LinearLayout) findViewById(R.id.log_out);
        web_View = findViewById(R.id.web_View);
        LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(PayGstActivity.this);
        user_name_pay.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getLName());

        back = findViewById(R.id.backEdit);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(PayGstActivity.this, LoginActivity.class));
                CommonUtils.makeUserLogout(PayGstActivity.this);
                finish();
            }
        });




        web_View.getSettings().setLoadsImagesAutomatically(true);
        web_View.getSettings().setJavaScriptEnabled(true);
        web_View.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        progDailog = ProgressDialog.show(PayGstActivity.this, "Loading","Please wait...", true);
        progDailog.setCancelable(false);

        web_View.getSettings().setJavaScriptEnabled(true);
        web_View.getSettings().setLoadWithOverviewMode(true);
        web_View.getSettings().setUseWideViewPort(true);


        web_View.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progDailog.show();
                view.loadUrl(url);

                return true;
            }
            @Override
            public void onPageFinished(WebView view, final String url) {
                progDailog.dismiss();
            }
        });
        web_View.loadUrl(url);



    }




    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
