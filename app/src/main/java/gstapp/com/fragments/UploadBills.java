package gstapp.com.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import UtilClasses.AppConstant;
import UtilClasses.CommonUtils;
import UtilClasses.LogeedUserDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import gstapp.com.Adapters.MyGridAdapter;
import gstapp.com.gstapp.PdfActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UploadBills extends Fragment {

    ImageView chooseFile;
    Button uploadButton;
    ImageView imageView;
    ArrayList<String> mPickedImages;
    ProgressDialog progressDialog;
    TextView tvSucessNext;

    @BindView(R.id.image1)
    ImageView image1;
    @BindView(R.id.tvChoseFile)
    TextView tvChoseFile;

    @BindView(R.id.image2)
    ImageView image2;

    @BindView(R.id.image3)
    ImageView image3;

    @BindView(R.id.image4)
    ImageView image4;

    @BindView(R.id.image5)
    ImageView image5;

    @BindView(R.id.uploadLinear)
    LinearLayout uploadLinear;

    @BindView(R.id.tvSucess)
    TextView tvSucess;

    ArrayList<ImageView> imagesList;
    LogeedUserDetails logeedUserDetails;

    Context context;
    static UploadBills fragment;

    RecyclerView gridRecycler;
    FrameLayout flUpload;

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    String[] PERMISSION_CAMERA = {Manifest.permission.CAMERA};


    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 177;
    private ApiInterface apiInterface;
    private Uri uri;
    private int THUMBNAIL_SIZE = 500;
    private ArrayList<String> filePaths;
    private final int SAVE_REQUEST_CODE = 900;
    private MyGridAdapter myGridAdapter;
    private String category;

    public UploadBills() {
        // Required empty public constructor
    }


    public static UploadBills newInstance() {
        if (fragment == null) {
            fragment = new UploadBills();
            Bundle args = new Bundle();
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }



    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override

        public void onReceive( Context context, Intent intent ) {

            String action = intent.getAction();

            String data = intent.getStringExtra("Upload");

            Log.d("UploadBill",data);
            chooseFile.setVisibility(View.VISIBLE);
            tvChoseFile.setVisibility(View.VISIBLE);
            tvSucess.setVisibility(View.GONE);
            tvSucessNext.setVisibility(View.GONE);
        //    Toast.makeText(context, "UploadBill", Toast.LENGTH_SHORT).show();

        }

    };



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upload_bills, container, false);
        ButterKnife.bind(this, view);

         chooseFile=view.findViewById(R.id.chooseFile);
        tvSucessNext=view.findViewById(R.id.tvSucessNext);
        chooseFile.setVisibility(View.VISIBLE);

        context = getActivity();
        logeedUserDetails = CommonUtils.getUserDetail(context);

        LocalBroadcastManager.getInstance(context).registerReceiver(

                receiver, new IntentFilter("Upload"));






        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        imageView = (ImageView) getView().findViewById(R.id.image);

        gridRecycler = (RecyclerView) getView().findViewById(R.id.gridRecycler);
        flUpload = (FrameLayout) getView().findViewById(R.id.flUpload);

        gridRecycler.setLayoutManager(new GridLayoutManager(getActivity(), 5));


        if(CommonUtils.getPreferencesString(getActivity(), AppConstant.SELECTED_TAB).equalsIgnoreCase("view_purchaged_bill")){

            tvChoseFile.setVisibility(View.VISIBLE);
            tvSucess.setVisibility(View.GONE);
            tvSucessNext.setVisibility(View.GONE);
           // Toast.makeText(context, ""+CommonUtils.getPreferencesString(getActivity(), AppConstant.SELECTED_TAB), Toast.LENGTH_SHORT).show();
        }





        mPickedImages = new ArrayList<>();
        myGridAdapter = new MyGridAdapter(getActivity(), mPickedImages);
        gridRecycler.setAdapter(myGridAdapter);

        imagesList = new ArrayList<>();
        imagesList.add(image1);
        imagesList.add(image2);
        imagesList.add(image3);
        imagesList.add(image4);
        imagesList.add(image5);

        filePaths = new ArrayList<>();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Uploading, Please Wait...");
        apiInterface = APIClient.getClient().create(ApiInterface.class);
        uploadButton= getView().findViewById(R.id.uploadButton);

        getView().findViewById(R.id.uploadButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPickedImages != null && mPickedImages.size() != 0) {

                  //  prepareAndSendData(category);
                   showCategoryDialog();

                } else {
                    CommonUtils.snackBar("Please select image or file to upload", uploadLinear, "300");
                }
            }
        });




        getView().findViewById(R.id.uploadLinear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!hasPermissions(getActivity(), PERMISSIONS)) {

                    requestPermissions(PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                    // this.requestPermissions(getActivity(), PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                } else if (hasPermissions(getActivity(), PERMISSIONS)) {
                    openGallery();
                }

                // checkPermission();
            }
        });

        getView().findViewById(R.id.chooseFile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // checkPermission();
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    requestPermissions(PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                    // ContextCompat.requestPermissions(getActivity(), PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                } else if (hasPermissions(getActivity(), PERMISSIONS)) {
                    openGallery();
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void showCategoryDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Enter Bill Name");
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.category_layout, null, false);

        final EditText editText = view.findViewById(R.id.catEdit);
        Button button = view.findViewById(R.id.submitBills);
        builder.setCancelable(true);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                category = editText.getText().toString();
                if (category != null && !category.equals("")) {

                    InputMethodManager ipmm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    ipmm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    alertDialog.dismiss();

                    prepareAndSendData(category);

                } else {
                    Toast.makeText(getActivity(), "Please Enter Bill Name", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void prepareAndSendData(String category) {

        MultipartBody.Part[] body = null;
        progressDialog.show();

        body = prepareFilePartArray("bill_pic[]", mPickedImages);
        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), logeedUserDetails.getUserId());
        RequestBody cat = RequestBody.create(MediaType.parse("text/plain"), category);
        Call<ResponseBody> bodyCall = apiInterface.uploadBills(user_id, cat, body);

        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                gridRecycler.setVisibility(View.GONE);
                flUpload.setVisibility(View.GONE);

                progressDialog.dismiss();
                if (response != null) {

                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                       // CommonUtils.snackBar(message, uploadLinear, status);
                        if (mPickedImages != null) {
                            mPickedImages.clear();
                            myGridAdapter.notifyDataSetChanged();



                           // getView().findViewById(R.id.chooseFile).setVisibility(View.VISIBLE);
                            tvSucess.setVisibility(View.VISIBLE);
                            tvSucessNext.setVisibility(View.VISIBLE);

                            Log.d("TAG","inside methods");
                            uploadButton.setVisibility(View.GONE);
                            tvChoseFile.setVisibility(View.GONE);

                            image1.setVisibility(View.GONE);
                            image2.setVisibility(View.GONE);
                            image3.setVisibility(View.GONE);
                            image4.setVisibility(View.GONE);
                            image5.setVisibility(View.GONE);
                            chooseFile.setVisibility(View.GONE);




                                                    }

                        myGridAdapter.notifyDataSetChanged();

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                progressDialog.dismiss();
                CommonUtils.snackBar("Slow Internet", uploadLinear, "300");
                Log.e("Error", t.toString());
            }
        });

    }


    private String getRealPathFromURIPath(Uri contentURI, FragmentActivity activity) {

        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    CommonUtils.snackBar("Permission Denied", uploadButton, "300");
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void openGallery() {
        pickImages();
    }

    private void pickImages() {


        showSelectionDialog();


    }

    private void showSelectionDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Make your selection");
        final CharSequence[] items = {"Capture Image", "Upload Image"};
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (item == 0) {

                    checkCameraPermission();

//openCa
                } else if (item == 1) {
                    FilePickerBuilder.getInstance().setMaxCount(50)
                            .setActivityTheme(R.style.FilePickerTheme)
                            .pickPhoto(getActivity());
                } else if (item == 2) {


                    Intent intent = new Intent(getActivity(), PdfActivity.class);
                    getActivity().startActivityForResult(intent, 300);


                }

                dialog.dismiss();

            }
        }).show();

    }

    private void checkCameraPermission() {

        if (!hasPermissions(getActivity(), PERMISSION_CAMERA)) {

            requestPermissions(PERMISSION_CAMERA, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

            // this.requestPermissions(getActivity(), PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        } else if (hasPermissions(getActivity(), PERMISSION_CAMERA)) {
            //openGallery();

            openCamera();

        }
    }

    private void openCamera() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(getActivity().getExternalCacheDir(),
                String.valueOf(System.currentTimeMillis()) + ".jpg");
        Uri fileUri = Uri.fromFile(file);
        // intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        getActivity().startActivityForResult(intent, 1200);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uploadButton.setVisibility(View.VISIBLE);
        tvChoseFile.setVisibility(View.GONE);

        getView().findViewById(R.id.chooseFile).setVisibility(View.GONE);

        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE:

                if (resultCode == getActivity().RESULT_OK && data != null) {
                    ArrayList<String> filePaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_PHOTOS);
                    String path = filePaths.get(0);

                    for (int i = 0; i < filePaths.size(); i++) {
                        mPickedImages.add(filePaths.get(i));
                    }
                    setImages2(mPickedImages);
                    //use them anywhere
                }
                break;
            case 1200: {

                if (data != null) {
                    final Bitmap photo = (Bitmap) data.getExtras().get("data");
                    // image1.setImageBitmap(photo);


                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            Uri tempUri = getImageUri(getActivity(), photo);

                            // CALL THIS METHOD TO GET THE ACTUAL PATH
                            File finalFile = new File(getRealPathFromURI(tempUri));

                            String path = getRealPathFromURIPath(tempUri, getActivity());
                            mPickedImages.add(path);

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    myGridAdapter.notifyDataSetChanged();
                                }
                            });
                            System.out.println("Image URI=" + tempUri);
                        }
                    }).start();
                }


                //  ShowUploadDialog();
            }

            break;

            case 300: {

                if (data != null) {
                    ArrayList<String> pdfPaths = data.getStringArrayListExtra("linkedList");

                    if (pdfPaths != null && pdfPaths.size() != 0) {
                        for (int i = 0; i < pdfPaths.size(); i++) {
                            mPickedImages.add(pdfPaths.get(i));
                        }
                    }
                    Toast.makeText(getActivity(), "file selected", Toast.LENGTH_SHORT).show();
                }

                //setImages2(mPickedImages);
                break;
            }

        }

    }

    private void ShowUploadDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Upload");


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                prepareAndSendData(category);
            }
        });

        builder.show();
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void setImages2(ArrayList<String> mPickedImages) {


        myGridAdapter.notifyDataSetChanged();


       /* for (int i = 0; i < mPickedImages.size(); i++) {
            String path = mPickedImages.get(i);
            Uri uri = Uri.parse(path);

            ImageView imageView = new ImageView(getActivity());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200, 250);
            imageView.setLayoutParams(layoutParams);
            uploadLinear.addView(imageView);
            Glide.with(getActivity()).load(path).asBitmap().into(imageView);

        }*/


    }


    @NonNull
    private MultipartBody.Part[] prepareFilePartArray(String partName, /*ArrayList<ImageEntry> fileUri*/ ArrayList<String> fileUri) {

        MultipartBody.Part[] arrayImage = new MultipartBody.Part[fileUri.size()];

        for (int index = 0; index < fileUri.size(); index++) {
            // String image = fileUri.get(index).path;
            String image = fileUri.get(index);
            File file = new File(String.valueOf(image));
            String type = null;
            final String extension = MimeTypeMap.getFileExtensionFromUrl(image);
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
            }
            if (type == null) {
                type = "image/*"; // fallback type. You might set it to /
            }
            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse(type),
                            file
                    );
            arrayImage[index] = MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        }
        // MultipartBody.Part is used to send also the actual file name
        return arrayImage;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
