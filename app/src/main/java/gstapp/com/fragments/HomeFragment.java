package gstapp.com.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import UtilClasses.CommonUtils;
import gstapp.com.create_invoice_module.CreateInvoiceActivity;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.EwayActivity;
import gstapp.com.gstapp.PayGstActivity;
import gstapp.com.gstapp.R;


public class HomeFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private Context context;

    public HomeFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private void addBillFragment() {

        /*((DashBoardActivity) getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("Create New Invoice");
        CreateNewBill createNewBill = new CreateNewBill();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainLayout, createNewBill).commit();*/

        getActivity().startActivity(new Intent(getActivity(), CreateInvoiceActivity.class));

    }

    private void replaceAboutFragment() {

        ((DashBoardActivity) getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("About CA Bulls");
        AboutFragment homeFragment = AboutFragment.newInstance(null);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainLayout, homeFragment).commit();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context=getActivity();

     //   fbHome.setVisibility(View.GONE);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setUpClicks();
    }

    private void setUpClicks() {


        getView().findViewById(R.id.create_new_bill_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.saveStringPreferences(context,"shipState","");
                CommonUtils.saveStringPreferences(context,"billState","");

                addBillFragment();
            }
        });

        getView().findViewById(R.id.my_profile_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                replaceProfileFragment();
            }
        });
        getView().findViewById(R.id.setting_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                replaceSettingFragment();
            }
        });

        getView().findViewById(R.id.myBi_linear_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                replaceMyBillFragment();
            }
        });

        getView().findViewById(R.id.my_purchase_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                replacePurchaseFragment();
            }
        });

        getView().findViewById(R.id.about_cabulls_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                replaceAboutFragment();
            }
        });

        getView().findViewById(R.id.memeberShip_linear_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceMemberShipFragment();
            }
        });

        getView().findViewById(R.id.pay_gst_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(),PayGstActivity.class));

            }
        });

        getView().findViewById(R.id.e_way_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), EwayActivity.class));

               /* Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://ewaybillgst.gov.in/Others/EBPrintnew.aspx"));
                startActivity(intent);*/
            }
        });

    }

    private void replaceMemberShipFragment(){
        ((DashBoardActivity)getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("Membership");
        MemberShipFragment memberShipFragment=new MemberShipFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout,memberShipFragment).commit();

    }

    private void replacePurchaseFragment() {

        ((DashBoardActivity) getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("My Purchases");
        MyPurchase myPurchase = MyPurchase.newInstance();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, myPurchase).commit();
    }

    private void replaceMyBillFragment() {

        ((DashBoardActivity) getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("My Bills");
        MyBills homeFragment = MyBills.newInstance();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainLayout, homeFragment).commit();

    }

    private void replaceSettingFragment() {

        ((DashBoardActivity) getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("Settings");
        Setting homeFragment = Setting.newInstance();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainLayout, homeFragment).commit();
    }

    private void replaceProfileFragment() {

        ((DashBoardActivity) getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("Manage Company Details");
        MyProfile myProfile = MyProfile.newInstance();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainLayout, myProfile).commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
