package gstapp.com.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ModelClasses.ProfileMainPojo;
import ModelClasses.UserProfile;
import UtilClasses.CommonUtils;
import UtilClasses.LogeedUserDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.create_invoice_module.InterGetDEtails;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.EditProfileActivity;
import gstapp.com.gstapp.R;
import gstapp.com.login_module.LoginInteractor;
import gstapp.com.login_module.LoginPresenter;
import gstapp.com.login_module.LoginView;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyProfile extends Fragment{


    private OnFragmentInteractionListener mListener;
    private ApiInterface apiInterface;
    ProgressDialog progressDialog;

    @BindView(R.id.com_name)
    TextView com_name;

    @BindView(R.id.com_address)
    TextView com_address;

    @BindView(R.id.com_phone1)
    TextView com_phone1;

    @BindView(R.id.com_phone2)
    TextView com_phone2;

    @BindView(R.id.com_email1)
    TextView com_email1;

    @BindView(R.id.com_email2)
    TextView com_email2;

    @BindView(R.id.GSTNTxt)
    TextView GSTNTxt;

    @BindView(R.id.CINtxt)
    TextView CINTxt;

    @BindView(R.id.PANTxt)
    TextView PANTxt;

    @BindView(R.id.stateCode_txt)
    TextView stateCodeTxt;

    @BindView(R.id.state_txt)
    TextView state_txt;

    @BindView(R.id.ac_name_txt)
    TextView ac_name_txt;

    @BindView(R.id.bnkName_txt)
    TextView bnkName_txt;

    @BindView(R.id.bank_branch_txt)
    TextView bank_branch_txt;

    @BindView(R.id.AC_no_txt)
    TextView AC_no_txt;

    @BindView(R.id.IFSC_txt)
    TextView IFSC_txt;

    @BindView(R.id.ac_type_txt)
    TextView ac_type_txt;

    @BindView(R.id.com_logo)
    ImageView com_logo;
    private LoginView loginView;


    @BindView(R.id.bill_serial_txt)
    TextView bill_serial_txt;

    @BindView(R.id.mainLAyout)
    LinearLayout mainLAyout;

    @BindView(R.id.edit_layout)
    RelativeLayout edit_layout;
    FloatingActionButton fbHome;
    private List<UserProfile> userProfiles;
    private Context context;
    SharedPreferences userPreference;



    public MyProfile() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static MyProfile newInstance() {

        MyProfile fragment = new MyProfile();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;



    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment



        progressDialog = new ProgressDialog(getActivity());

        context=getActivity();
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, view);


        apiInterface = APIClient.getClient().create(ApiInterface.class);

       userPreference= context.getSharedPreferences("userPref", 0);

        fbHome=view.findViewById(R.id.fbHome);

        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);


        fbHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getActivity(),DashBoardActivity.class);
                startActivity(i);


            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        apiInterface = APIClient.getClient().create(ApiInterface.class);
        getUserData();

        /*progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        getUserData();*/


        edit_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putParcelableArrayListExtra("compnyData", (ArrayList<? extends Parcelable>) userProfiles);
                getActivity().startActivity(intent);


            }
        });

    }

    private void replaceEditFragment() {


        ((DashBoardActivity) getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("Update Details");
        EditProfile homeFragment = EditProfile.newInstance();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("compnyData", (ArrayList<? extends Parcelable>) userProfiles);
        homeFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainLayout, homeFragment).commit();

    }

    private void getUserData() {

        String id = CommonUtils.getUserId(getActivity());
        Call<ProfileMainPojo> responseBodyCall = apiInterface.userProfile(id);
        responseBodyCall.enqueue(new Callback<ProfileMainPojo>() {
            @Override
            public void onResponse(Call<ProfileMainPojo> call, Response<ProfileMainPojo> response) {

                progressDialog.dismiss();
               String res = response.body().toString();

                Log.e("profileRes",res);

                mainLAyout.setVisibility(View.VISIBLE);
                ProfileMainPojo profileMainPojo = response.body();
                String status = profileMainPojo.getStatus();
                if (status.equals("200")) {
                    userProfiles = profileMainPojo.getUser();

                    String companyLogo = userProfiles.get(0).getCompany_logo();
                    if (companyLogo != null && !companyLogo.equals("")) {

                        Glide.with(context)
                                .load(companyLogo)
                                .error(R.drawable.ic_defalt_user)
                                .into(com_logo);

                    } else {
                        com_logo.setImageResource(R.drawable.ic_defalt_user);
                    }

                    com_name.setText(userProfiles.get(0).getCompany_name());
                    com_address.setText(userProfiles.get(0).getAddress());
                    com_phone1.setText(userProfiles.get(0).getMobile());
                    com_email1.setText(userProfiles.get(0).getEmail());

                    GSTNTxt.setText(userProfiles.get(0).getGstn());
                    CINTxt.setText(userProfiles.get(0).getCin());
                    PANTxt.setText(userProfiles.get(0).getPan());

                    stateCodeTxt.setText(userProfiles.get(0).getState_code());

                    CommonUtils.saveStringPreferences(context,"Admin_status_code",userProfiles.get(0).getState_code());
                    CommonUtils.saveStringPreferences(context,"Admin_state",userProfiles.get(0).getState());

                    state_txt.setText(userProfiles.get(0).getState());

                    ac_name_txt.setText(userProfiles.get(0).getFirst_name() + " " + userProfiles.get(0).getMid_name()
                            + " " + userProfiles.get(0).getLast_name());
                    bnkName_txt.setText(userProfiles.get(0).getBank_name());
                    bank_branch_txt.setText(userProfiles.get(0).getBank_branch());
                    AC_no_txt.setText(userProfiles.get(0).getAc_no());
                    IFSC_txt.setText(userProfiles.get(0).getIfsc_code());
                    ac_type_txt.setText(userProfiles.get(0).getAc_type());

                    String bill_no_series = userProfiles.get(0).getBill_no_series();

                    bill_serial_txt.setVisibility(View.VISIBLE);
                    bill_serial_txt.setText("Bill Series No : " + bill_no_series);

                    SharedPreferences.Editor editor = userPreference.edit();
                    editor.putString("first_name", userProfiles.get(0).getFirst_name());
                    editor.putString("mid_name", userProfiles.get(0).getMid_name());
                    editor.putString("last_name", userProfiles.get(0).getLast_name());
                    editor.putString("gender", userProfiles.get(0).getGender());
                    editor.putString("email", userProfiles.get(0).getEmail());
                    editor.putString("password", userProfiles.get(0).getPassword());
                    editor.putString("role", userProfiles.get(0).getRole());
                    editor.putString("company_name", userProfiles.get(0).getCompany_name());
                    editor.putString("company_logo", userProfiles.get(0).getCompany_logo());
                    editor.putString("gstn", userProfiles.get(0).getGstn());
                    editor.putString("cin", userProfiles.get(0).getCin());
                    editor.putString("pan", userProfiles.get(0).getPan());
                    editor.putString("bank_name", userProfiles.get(0).getBank_name());
                    editor.putString("bank_branch", userProfiles.get(0).getBank_branch());
                    editor.putString("ac_no", userProfiles.get(0).getAc_no());
                    editor.putString("ifsc_code", userProfiles.get(0).getIfsc_code());
                    editor.putString("ac_type", userProfiles.get(0).getAc_type());
                    editor.putString("bill_no_series", userProfiles.get(0).getBill_no_series());
                    editor.putString("state_code", userProfiles.get(0).getState_code());
                    editor.putString("state", userProfiles.get(0).getState());
                    editor.putString("address", userProfiles.get(0).getAddress());
                    editor.putString("mobile", userProfiles.get(0).getMobile());
                    editor.putString("created_at", userProfiles.get(0).getCreated_at());
                    editor.putBoolean("isLoggedIn", true);
                    editor.apply();


                    Log.d("MyProfilebody()", response.body().toString());

                }
            }

            @Override
            public void onFailure(Call<ProfileMainPojo> call, Throwable t) {

                Log.d("Error", t.toString());
                progressDialog.dismiss();
                CommonUtils.snackBar(t.toString(), edit_layout, "300");

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }









    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
