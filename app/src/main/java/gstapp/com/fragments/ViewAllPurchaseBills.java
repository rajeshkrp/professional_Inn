package gstapp.com.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ModelClasses.AllBillMainPojo;
import ModelClasses.AllBillsListPojo;
import ModelClasses.AllBillMainPojo;
import UtilClasses.CommonUtils;
import gstapp.com.Adapters.MyAllBillAdapter;
import gstapp.com.Adapters.MyBillsAdapter;
import gstapp.com.GalleryViewActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ViewAllPurchaseBills extends Fragment {


    RecyclerView allBillRecycler;

    ApiInterface apiInterface;
    private ArrayList<String> categoryList;
    private List<AllBillsListPojo> allBillsListPojos;

    ProgressDialog progressDialog;
    private MyAllBillAdapter myAllBillAdapter;
    private Context context;

    private TextView fromDate, toDate, noOfInvoice, turnOver;
    EditText searchEdit;
    private String selectedFromDate;
    DatePickerDialog datePickerDialog;
    private String userId="";
    private String selectedToDate;

    public ViewAllPurchaseBills() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ViewAllPurchaseBills newInstance() {
        ViewAllPurchaseBills fragment = new ViewAllPurchaseBills();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_view_all_purchase_bills, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        context = getActivity();
        allBillsListPojos=new ArrayList<>();


        allBillRecycler = (RecyclerView) getView().findViewById(R.id.allBillRecycler);
        allBillRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));

        categoryList = new ArrayList<>();
        myAllBillAdapter = new MyAllBillAdapter(getActivity(), allBillsListPojos, ViewAllPurchaseBills.this, categoryList);
        allBillRecycler.setAdapter(myAllBillAdapter);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");




        userId = CommonUtils.getUserId(getActivity());
        Log.e("userId",userId);
        fromDate = (TextView) getView().findViewById(R.id.fromDateTxt);
        toDate = (TextView) getView().findViewById(R.id.toDateTxt);






        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker();
            }
        });
        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedFromDate == null || selectedFromDate.equals("")) {
                    CommonUtils.snackBar("Please Select From Date", searchEdit, "300");
                } else {
                    openDatePickerTo();
                }
            }
        });
        
        

        getAllBills();
    }


    private void getAllBills() {

        apiInterface = CommonUtils.InitilizeInterface();
        if (context != null) {
            Call<ResponseBody> bodyCall = apiInterface.getAllBillsImages(CommonUtils.getUserId(context));
            bodyCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (progressDialog != null)
                        progressDialog.dismiss();


                    String str = "";
                    String message="";
                    int status=0;
                    boolean responseBoolean;
                    JSONObject jsonObject = null;

                    AllBillsListPojo  allBillMainPojo;

                    try {
                        str = response.body().string();
                        Log.e("purchase", str);

                             jsonObject = new JSONObject(str);
                            message = jsonObject.getString("message");
                            status = jsonObject.getInt("status");

                             responseBoolean = jsonObject.getBoolean("response");

                        if(responseBoolean) {

                            CommonUtils.snackBar("true", allBillRecycler, "300");
                        }
                        else {

                            CommonUtils.snackBar("No Recored found", allBillRecycler, "300");

                        }
                        }
                    catch (IOException e) {

                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {

                        JSONArray responseArray = jsonObject.getJSONArray("response");
                        if(responseArray != null && responseArray.length() > 0 ) {
                            Gson gson = new Gson();
                            allBillsListPojos = Arrays.asList(gson.fromJson(responseArray.toString(), AllBillsListPojo[].class));

                            Log.e("listpojo","listpojosize"+allBillsListPojos.size());
                            categoryList.clear();
                            if (responseArray != null && responseArray.length() > 0) {
                                for (int i = 0; i < allBillsListPojos.size(); i++) {
                                    String catgery = allBillsListPojos.get(i).getCategory();
                                    for (int j = 0; j < allBillsListPojos.size(); j++) {
                                        String cat2 = allBillsListPojos.get(j).getCategory();
                                        if (cat2.equals(catgery) && !categoryList.contains(cat2)) {
                                            categoryList.add(cat2);
                                        }
                                    }
                                    //myAllBillAdapter.notifyDataSetChanged();
                                }

                                MyAllBillAdapter myAllBillAdapter = new MyAllBillAdapter(getActivity(), allBillsListPojos, ViewAllPurchaseBills.this, categoryList);
                                allBillRecycler.setAdapter(myAllBillAdapter);

                            }
                        }
                    String image = allBillsListPojos.get(0).getCompany_id();


                    }

                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    CommonUtils.snackBar(t.toString(), allBillRecycler, "300");

                  Log.e("respnfail::::",t.toString());  // progressDialog.dismiss();
                }
            });
        }

    }



    private void getFilteredData() {
        String id = CommonUtils.getUserId(getActivity());
        Call<ResponseBody> bodyCall = apiInterface.getFiltePurchaseList(id, selectedFromDate, selectedToDate);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();

                Log.e("selectedFromDate", selectedFromDate);
                Log.e("selectedToDate", selectedToDate);



                String str = "";
                String message="";
                int status=0;
                boolean responseBoolean;
                JSONObject jsonObject = null;
                try {
                    str = response.body().string();
                    Log.e("purchaseFilter", str);

                    jsonObject = new JSONObject(str);
                    message = jsonObject.getString("message");
                    status = jsonObject.getInt("status");

                    JSONArray responseArray = jsonObject.getJSONArray("response");


                    if(responseArray != null && responseArray.length() > 0 ) {

                        Log.e("purchaseFilter","purchaseFilterList"+ responseArray.length());
                     //   allBillsListPojos.clear();
                        categoryList.clear();

                        Gson gson = new Gson();
                        allBillsListPojos = Arrays.asList(gson.fromJson(responseArray.toString(), AllBillsListPojo[].class));

                        for (int i = 0; i < allBillsListPojos.size(); i++) {
                            String catgery = allBillsListPojos.get(i).getCategory();
                            for (int j = 0; j < allBillsListPojos.size(); j++) {
                                String cat2 = allBillsListPojos.get(j).getCategory();
                                if (cat2.equals(catgery) && !categoryList.contains(cat2)) {
                                    categoryList.add(cat2); }
                            }
                            //myAllBillAdapter.notifyDataSetChanged();
                        }


                        if (allBillsListPojos.size() == 0) {
                            Toast.makeText(getActivity(), "No Bill Found", Toast.LENGTH_SHORT).show();
                            allBillRecycler.setAdapter(null);

                        }
                        else {
                            myAllBillAdapter = new MyAllBillAdapter(getActivity(), allBillsListPojos, ViewAllPurchaseBills.this, categoryList);
                            allBillRecycler.setAdapter(myAllBillAdapter);
                        }

                    }

                    else {

                        CommonUtils.snackBar("Filtered Data not  found", allBillRecycler, "300");
                        }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                CommonUtils.snackBar(t.toString(), allBillRecycler, "300");

                Log.e("respnfailFilter::::",t.toString());  // progressDialog.dismiss();

            }
        });

    }

    private void openDatePickerTo() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                toDate.setText(dayOfMonth + "/"
                        + (monthOfYear + 1) + "/" + year);
                selectedToDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    if (selectedFromDate == null || selectedFromDate.equals("")) {
                        CommonUtils.snackBar("Please Select From Date", searchEdit, "300");
                    } else {
                        Date date1 = sdf.parse(selectedFromDate);
                        Date date2 = sdf.parse(selectedToDate);

                        if (date1.compareTo(date2) < 0) {
                            System.out.println("Date1 is before Date2");
                            //call API.........................
                           // CommonUtils.snackBar("if", searchEdit, "300");
                            progressDialog.show();
                            getFilteredData();



                        }
                        else {

                           // CommonUtils.snackBar("else", searchEdit, "300");

                            progressDialog.show();
                            getFilteredData();

                        }

                       /* if (date1.compareTo(date2) > 0) {
                            System.out.println("Date1 is after Date2");
                            Toast.makeText(getActivity(), "Please select ToDate Greater then FromDate", Toast.LENGTH_SHORT).show();
                        } else if (date1.compareTo(date2) < 0) {
                            System.out.println("Date1 is before Date2");
                            //call API.........................
                            progressDialog.show();
                            getFilteredData();

                        } else if (date1.compareTo(date2) == 0) {
                            Toast.makeText(getActivity(), "Please select ToDate Greater then FromDate", Toast.LENGTH_SHORT).show();
                        }*/
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    private void openDatePicker() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                fromDate.setText(dayOfMonth + "/"
                        + (monthOfYear + 1) + "/" + year);

                selectedFromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }
    

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void showGallery(ArrayList<String> imagesList, String title) {

        Intent intent = new Intent(getActivity(), GalleryViewActivity.class);
        intent.putExtra("title", title);
        intent.putStringArrayListExtra("imagesList", imagesList);
        getActivity().startActivity(intent);


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            getAllBills();
        }
    }
}
