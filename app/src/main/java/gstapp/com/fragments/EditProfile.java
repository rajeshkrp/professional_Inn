package gstapp.com.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ModelClasses.UserProfile;
import UtilClasses.CommonUtils;
import UtilClasses.ImageInterface;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class EditProfile extends Fragment {


    private ProgressDialog progressDialog;

    @BindView(R.id.com_name_edit)
    EditText com_name_edit;

    @BindView(R.id.com_add_edit)
    EditText com_add_edit;

    @BindView(R.id.com_phone_edit)
    EditText com_phone_edit;

    @BindView(R.id.com_email_edit)
    EditText com_email_edit;

    @BindView(R.id.com_GSTN_edit)
    EditText com_GSTN_edit;

    @BindView(R.id.com_CIN_edit)
    EditText com_CIN_edit;

    @BindView(R.id.com_PAN_edit)
    EditText com_PAN_edit;

    @BindView(R.id.com_state_edit)
    EditText com_state_edit;

    @BindView(R.id.com_state_code_edit)
    EditText com_state_code_edit;

    @BindView(R.id.com_bank_name_edit)
    EditText com_bank_name_edit;

    @BindView(R.id.com_acc_name_edit)
    EditText com_acc_name_edit;

    @BindView(R.id.com_branch_edit)
    EditText com_branch_edit;

    @BindView(R.id.com_acc_no_edit)
    EditText com_acc_no_edit;

    @BindView(R.id.com_IFSC_edit)
    EditText com_IFSC_edit;

    @BindView(R.id.com_acc_type_edit)
    EditText com_acc_type_edit;

    @BindView(R.id.com_bill_no_edit)
    EditText com_bill_no_edit;

    @BindView(R.id.update_button)
    Button update_button;

    @BindView(R.id.com_logo_edit)
    ImageView com_logo_edit;

    ApiInterface apiInterface;

    ImageInterface imageInterface;

    static EditProfile editProfile;

    private OnFragmentInteractionListener mListener;
    private final int PICK_FROM_GALLERY = 200;

    MultipartBody.Part imagePart;
    private Uri uri;
    private Context context;

    public EditProfile() {
        // Required empty public constructor
    }


    public static EditProfile newInstance() {

        if (editProfile == null) {
            editProfile = new EditProfile();
            Bundle args = new Bundle();

            editProfile.setArguments(args);
        }
        return editProfile;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this, view);
        context=getActivity();




        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Updating Details");
        progressDialog.setCancelable(false);
        List<UserProfile> userProfile = getArguments().getParcelableArrayList("compnyData");
        String cName = userProfile.get(0).getCompany_name();

        com_name_edit.setText(userProfile.get(0).getCompany_name());
        com_add_edit.setText(userProfile.get(0).getAddress());
        com_phone_edit.setText(userProfile.get(0).getMobile());
        com_email_edit.setText(userProfile.get(0).getEmail());
        com_GSTN_edit.setText(userProfile.get(0).getGstn());
        com_CIN_edit.setText(userProfile.get(0).getCin());
        com_PAN_edit.setText(userProfile.get(0).getPan());

        com_bank_name_edit.setText(userProfile.get(0).getBank_name());
        com_acc_name_edit.setText(userProfile.get(0).getFirst_name());
        com_branch_edit.setText(userProfile.get(0).getBank_branch());
        com_acc_no_edit.setText(userProfile.get(0).getAc_no());
        com_IFSC_edit.setText(userProfile.get(0).getIfsc_code());
        com_acc_type_edit.setText(userProfile.get(0).getAc_type());
        com_bill_no_edit.setText(userProfile.get(0).getBill_no_series());









        Glide.with(context)
                .load(userProfile.get(0).getCompany_logo()).error(R.drawable.user_default)
                .into(com_logo_edit);


      //  Picasso.wi


        apiInterface = APIClient.getClient().create(ApiInterface.class);

        com_logo_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog.show();
                updateUserData();

            }
        });

    }

    private void openGallery() {

        getUserPermission();

    }

    private void getUserPermission() {

        try {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
            } else {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                getActivity().startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateUserData() {


        HashMap<String, String> hashMap = new HashMap<>();

        String id = CommonUtils.getUserId(getActivity());
        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), id);
        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), com_phone_edit.getText().toString());
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), com_add_edit.getText().toString());
        RequestBody com_name = RequestBody.create(MediaType.parse("text/plain"), com_name_edit.getText().toString());
        RequestBody gstn = RequestBody.create(MediaType.parse("text/plain"), com_GSTN_edit.getText().toString());
        RequestBody cin = RequestBody.create(MediaType.parse("text/plain"), com_CIN_edit.getText().toString());
        RequestBody pan = RequestBody.create(MediaType.parse("text/plain"), com_PAN_edit.getText().toString());
        RequestBody state_code = RequestBody.create(MediaType.parse("text/plain"), com_state_code_edit.getText().toString());
        RequestBody state = RequestBody.create(MediaType.parse("text/plain"), com_state_edit.getText().toString());
        RequestBody b_name = RequestBody.create(MediaType.parse("text/plain"), com_bank_name_edit.getText().toString());
        RequestBody b_branch = RequestBody.create(MediaType.parse("text/plain"), com_branch_edit.getText().toString());
        RequestBody bill_no = RequestBody.create(MediaType.parse("text/plain"), com_bill_no_edit.getText().toString());
        RequestBody ac_no = RequestBody.create(MediaType.parse("text/plain"), com_acc_no_edit.getText().toString());
        RequestBody ifsc_code = RequestBody.create(MediaType.parse("text/plain"), com_IFSC_edit.getText().toString());
        RequestBody ac_type = RequestBody.create(MediaType.parse("text/plain"), com_acc_type_edit.getText().toString());

        saveBillNoToLocal(com_bill_no_edit.getText().toString());

        ArrayList<String> mSelectedImages = new ArrayList<>();
        MultipartBody.Part[] body = null;
        if (uri != null) {
            String path = getRealPathFromURIPath(uri, getActivity());
            mSelectedImages.add(path);

            body = prepareFilePartArray("profile_pic", mSelectedImages);
        }


        Call<ResponseBody> responseBodyCall = apiInterface.updateUserData(user_id, mobile, address, com_name, gstn, cin, pan,
                state_code, state, b_name, b_branch, bill_no, ac_no, ifsc_code, ac_type, body);

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }

                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    String message = jsonObject.getString("message");

                    CommonUtils.snackBar(message,com_name_edit,"200");


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                Log.e("Error", t.toString());
            }
        });


    }

    private void saveBillNoToLocal(String s) {

        CommonUtils.saveBillNo(getActivity(), s);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        uri = data.getData();
        try {

            String filePath = getRealPathFromURIPath(uri, getActivity());
            File file = new File(filePath);
            Log.d(TAG, "Filename " + file.getName());

            RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
            imagePart = fileToUpload;
            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            com_logo_edit.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String getRealPathFromURIPath(Uri contentURI, FragmentActivity activity) {

        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    getActivity().startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }


    @NonNull
    private MultipartBody.Part[] prepareFilePartArray(String partName, ArrayList<String> fileUri) {

        MultipartBody.Part[] arrayImage = new MultipartBody.Part[fileUri.size()];

        for (int index = 0; index < fileUri.size(); index++) {
            String image = fileUri.get(index);
            File file = new File(String.valueOf(image));
            String type = null;
            final String extension = MimeTypeMap.getFileExtensionFromUrl(image);
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
            }
            if (type == null) {
                type = "image/*"; // fallback type. You might set it to /
            }
            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse(type),
                            file
                    );
            arrayImage[index] = MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        }
        // MultipartBody.Part is used to send also the actual file name
        return arrayImage;
    }
}
