package gstapp.com.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ModelClasses.AllBillsListPojo;
import ModelClasses.GSTListDetails;
import ModelClasses.MyBillDetailPojo;
import ModelClasses.MyBillMainPojo;
import ModelClasses.ProductDetails;
import UtilClasses.CommonUtils;
import UtilClasses.LogeedUserDetails;
import gstapp.com.Adapters.MyAllBillAdapter;
import gstapp.com.Adapters.MyBillInvoiceGenratorAdapter;
import gstapp.com.Adapters.MyBillTaxSlabAdapter;
import gstapp.com.Adapters.MyBillsAdapter;
import gstapp.com.create_invoice_module.Product_Details;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.senab.photoview.PhotoViewAttacher;


public class MyBills extends Fragment {


    RecyclerView recyclerView;
    ApiInterface apiInterface;
    String userId;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 977;
    private TextView fromDate, toDate, noOfInvoice, turnOver;
    EditText searchEdit;
    private String selectedFromDate;
    DatePickerDialog datePickerDialog;
    MyBillsAdapter myBillsAdapter;
    private ProgressDialog progressDialog;
    private String selectedToDate;
    private View content;
    private LinearLayout view;
    Bitmap bitmap;
    private String totalTaxAmount;
    private Dialog mDialog;
    RecyclerView rvInvoice,recy_tax;
    private Context context;
    List<MyBillDetailPojo> myBillDetailPojoList;
    List<ProductDetails> productDetailsList;
    private String billHeaderN="Normal";
    private String billHeaderD="Dublicate";
    private String billHeaderT="Triplicate";
    private String invice_no="",supplier_name="";

    FloatingActionButton fbHome;
    private String stateCode="";
    String wender_state_code="";


    public static MyBills newInstance() {
        MyBills fragment = new MyBills();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_my_bills, container, false);

        fbHome=view.findViewById(R.id.fbHome);

        fbHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dashBoard=new Intent(getActivity(), DashBoardActivity.class);
                startActivity(dashBoard);
            }
        });


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context=getActivity();

        wender_state_code=CommonUtils.getPreferencesString(context,"Admin_status_code");

        myBillDetailPojoList=new ArrayList<>();
        productDetailsList=new ArrayList<>();
        recyclerView = (RecyclerView) getView().findViewById(R.id.myBillsRecycler);
        searchEdit = (EditText) getView().findViewById(R.id.searchEdit);
        userId = CommonUtils.getUserId(getActivity());
        Log.e("userId",userId);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        fromDate = (TextView) getView().findViewById(R.id.fromDateTxt);
        toDate = (TextView) getView().findViewById(R.id.toDateTxt);
        noOfInvoice = (TextView) getView().findViewById(R.id.btn_no_of_invice);
        turnOver = (TextView) getView().findViewById(R.id.btn_turn_over);
        searchEdit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (searchEdit.getRight() - searchEdit.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        InputMethodManager imm = (InputMethodManager) getActivity()
                                .getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(((Activity) getActivity()).getWindow()
                                .getCurrentFocus().getWindowToken(), 0);

                        progressDialog.show();

                        getInoviceandName(searchEdit.getText().toString());


                        getFilteredData();

                        return true;
                    }
                }
                return false;
            }
        });

        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker();
            }
        });
        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedFromDate == null || selectedFromDate.equals("")) {
                    CommonUtils.snackBar("Please Select From Date", searchEdit, "300");
                } else {
                    openDatePickerTo();
                }
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        apiInterface = APIClient.getClient().create(ApiInterface.class);
        getBillList();
        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //String text = searchEdit.getText().toString().toLowerCase(Locale.getDefault());
                // myBillsAdapter.filter(text);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void getInoviceandName(String text) {

        boolean digitsOnly = TextUtils.isDigitsOnly(text);
        if (digitsOnly) {

            invice_no=text;
            } else {

            supplier_name=text; }
            }

    private void getFilteredData() {
        String id = CommonUtils.getUserId(getActivity());
        Call<MyBillMainPojo> bodyCall = apiInterface.getFilteredData(userId, selectedFromDate, selectedToDate,supplier_name, invice_no);
        bodyCall.enqueue(new Callback<MyBillMainPojo>() {
            @Override
            public void onResponse(Call<MyBillMainPojo> call, Response<MyBillMainPojo> response) {
//                myBillDetailPojoList.clear();
                progressDialog.dismiss();
                if (response != null) {
                    MyBillMainPojo myBillMainPojo = response.body();
                    if (myBillMainPojo.getStatus()!=null&&myBillMainPojo.getStatus() == 200) {
                        int status = myBillMainPojo.getStatus();

                        Log.e("filterd","filtered"+response.body());

                       /* String msg = myBillMainPojo.getMessage();
                        int basicRate = myBillMainPojo.getBasicRate();
                        int totalInvoice = myBillMainPojo.getTotalInvoice();*/

                        if (myBillMainPojo.getBills() != null && myBillMainPojo.getBills().size() > 0) {
                            myBillDetailPojoList = myBillMainPojo.getBills();
                            if (myBillDetailPojoList.size() == 0) {
                                Toast.makeText(getActivity(), "No Bill Found", Toast.LENGTH_SHORT).show();
                                recyclerView.setAdapter(null);
                            }
                            else {
                                noOfInvoice.setText("Total Count : " + myBillMainPojo.getTotalInvoice());
                                turnOver.setText("Turn Over : " + myBillMainPojo.getBasicRate());


                                myBillsAdapter = new MyBillsAdapter(getActivity(), myBillDetailPojoList, MyBills.this);
                                recyclerView.setAdapter(myBillsAdapter);
                            }
                        } } } }

            @Override
            public void onFailure(Call<MyBillMainPojo> call, Throwable t) {

            }
        });

    }

    private void openDatePickerTo() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                toDate.setText(dayOfMonth + "/"
                        + (monthOfYear + 1) + "/" + year);
                selectedToDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    if (selectedFromDate == null || selectedFromDate.equals("")) {
                        CommonUtils.snackBar("Please Select From Date", searchEdit, "300");
                    } else {
                        Date date1 = sdf.parse(selectedFromDate);
                        Date date2 = sdf.parse(selectedToDate);

                        if (date1.compareTo(date2) < 0) {
                            System.out.println("Date1 is before Date2");
                            //call API.........................
                            // CommonUtils.snackBar("if", searchEdit, "300");
                            progressDialog.show();
                            getFilteredData();
                        }
                        else {

                            // CommonUtils.snackBar("else", searchEdit, "300");

                            progressDialog.show();
                            getFilteredData();

                        }


                      /*  if (date1.compareTo(date2) > 0) {
                            System.out.println("Date1 is after Date2");
                            Toast.makeText(getActivity(), "Please select ToDate Greater then FromDate", Toast.LENGTH_SHORT).show();
                        } else if (date1.compareTo(date2) < 0) {
                            System.out.println("Date1 is before Date2");
                            //call API.........................
                            progressDialog.show();
                            getFilteredData();

                        } else if (date1.compareTo(date2) == 0) {
                            Toast.makeText(getActivity(), "Please select ToDate Greater then FromDate", Toast.LENGTH_SHORT).show();
                        }*/



                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    private void openDatePicker() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        Toast.makeText(context, "date", Toast.LENGTH_SHORT).show();



        String date = "25/2/2016";
        String parts[] = date.split("/");

        int day = Integer.parseInt(parts[0]);
        int month = Integer.parseInt(parts[1]);
        int year = Integer.parseInt(parts[2]);


        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, (month - 1));
        c.set(Calendar.DAY_OF_MONTH, day);








        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {

                fromDate.setText(dayOfMonth + "/"
                        + (monthOfYear + 1) + "/" + year);

                selectedFromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    private void getBillList() {

        String id = CommonUtils.getUserId(getActivity());

       Call<ResponseBody> myBillMainPojoCall = apiInterface.getBillList(id);
       // Call<ResponseBody> myBillMainPojoCall = apiInterface.getBillList("4");

        myBillMainPojoCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (progressDialog != null)
                    progressDialog.dismiss();

                String str = "";
                String message="",basicRate="",totalInvoice="";
                int status=0;
                boolean responseBoolean;
                JSONObject jsonObject = null;
                try {
                    str = response.body().string();
                    Log.e("purchaseFilter", str);


                    jsonObject = new JSONObject(str);
                    message = jsonObject.getString("message");
                    status = jsonObject.getInt("status");

                    basicRate = jsonObject.getString("basicRate");
                    totalInvoice = jsonObject.getString("totalInvoice");

                //    CommonUtils.snackBar("list", turnOver, "300");



                    JSONArray responseArray = jsonObject.getJSONArray("bills");


                    if(responseArray != null && responseArray.length() > 0 ) {

                        Log.e("MYBills","MyBills"+ responseArray.length());
                        //   allBillsListPojos.clear();

                        Gson gson = new Gson();
                        myBillDetailPojoList = Arrays.asList(gson.fromJson(responseArray.toString(), MyBillDetailPojo[].class));





                        noOfInvoice.setText("Total Count : " +totalInvoice);
                        turnOver.setText("Turn Over : " +basicRate );



                        for (int i = 0; i < myBillDetailPojoList.size(); i++) {
                            productDetailsList.addAll(myBillDetailPojoList.get(i).getProduct());
                          //  gstListPojoList.addAll(myBillDetailPojoList.get(i).getGstList());



                            }



                        //myBillDetailPojoList = myBillMainPojo.getBills();
                        myBillsAdapter = new MyBillsAdapter(getActivity(), myBillDetailPojoList, MyBills.this);
                        LinearLayoutManager mLayoutManager=new LinearLayoutManager(context);
                        mLayoutManager.setReverseLayout(true);
                        mLayoutManager.setStackFromEnd(true);
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setAdapter(myBillsAdapter);
                        }

                    else {

                        CommonUtils.snackBar("Record not  found", turnOver, "300");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                    }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                progressDialog.dismiss();
                CommonUtils.snackBar("Please Check Internet connection", recyclerView, "300");

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



    public void previewBill(final MyBillDetailPojo myBillDetailPojo, List<GSTListDetails> gstList, List<ProductDetails> productList, final Context context) {

        LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(getActivity());

        // draw something on the page
        LayoutInflater inflater = (LayoutInflater)
                getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        content = inflater.inflate(R.layout.pdf_layout, null);
        RelativeLayout relativeLayout = (RelativeLayout) content.findViewById(R.id.rotateRelative);
        relativeLayout.setRotation(90);

        rvInvoice = (RecyclerView) content.findViewById(R.id.recyclerview);
        recy_tax = content.findViewById(R.id.recy_tax);


        TextView PdfComName = (TextView) content.findViewById(R.id.pdf_come_name);
        TextView tvBillHeader = (TextView) content.findViewById(R.id.tvBillHeader);
        ImageView pdf_logo = (ImageView) content.findViewById(R.id.pdf_logo);
        TextView total_text2 = (TextView) content.findViewById(R.id.total_text2);
        String lgog = logeedUserDetails.getCompany_logo();


        /*Glide.with(context)
                .load(lgog)
                .into(pdf_logo);*/

        TextView PdfComAdd = (TextView) content.findViewById(R.id.pdf_com_address);
        TextView PdfInvoiceNo = (TextView) content.findViewById(R.id.pdf_invoice);
        TextView PdfDate = (TextView) content.findViewById(R.id.pdf_date);


        TextView pdf_gstn = (TextView) content.findViewById(R.id.pdf_gstn);
        TextView pdf_cin = (TextView) content.findViewById(R.id.pdf_cin);
        TextView pdf_pan = (TextView) content.findViewById(R.id.pdf_pan);
        TextView pdf_state_code = (TextView) content.findViewById(R.id.pdf_state_code);
        TextView pdf_state = (TextView) content.findViewById(R.id.pdf_state);
        TextView pdf_cust_name = (TextView) content.findViewById(R.id.pdf_cust_name);
       // TextView igst_pdf = (TextView) content.findViewById(R.id.igst_pdf);
      //  TextView igst_infigure = (TextView) content.findViewById(R.id.igst_infigure);
        TextView bottom_com_name = (TextView) content.findViewById(R.id.bottom_com_name);
        //............set Bank Details..............................................................
        TextView ac_name_pdf = (TextView) content.findViewById(R.id.ac_name_pdf);
        TextView bnkName_pdf = (TextView) content.findViewById(R.id.bnkName_pdf);
        TextView bank_branch_pdf = (TextView) content.findViewById(R.id.bank_branch_pdf);
        TextView AC_no_pdf = (TextView) content.findViewById(R.id.AC_no_pdf);
        TextView IFSC_pdf = (TextView) content.findViewById(R.id.IFSC_pdf);
        TextView ac_type_pdf = (TextView) content.findViewById(R.id.ac_type_pdf);
        TextView total_text = (TextView) content.findViewById(R.id.total_text);

        ac_name_pdf.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getLName());
        bnkName_pdf.setText(logeedUserDetails.getBank_name());
        bank_branch_pdf.setText(logeedUserDetails.getBank_branch());
        AC_no_pdf.setText(logeedUserDetails.getAc_no());
        IFSC_pdf.setText(logeedUserDetails.getIfsc_code());
        ac_type_pdf.setText(logeedUserDetails.getAc_type());

        TextView pdf_cust_add1 = (TextView) content.findViewById(R.id.pdf_cust_add1);
        TextView pdf_cust_state = (TextView) content.findViewById(R.id.pdf_cust_state);
        TextView pdf_cust_stateCode = (TextView) content.findViewById(R.id.pdf_cust_stateCode);
        TextView pdf_cust_GSTN = (TextView) content.findViewById(R.id.pdf_cust_GSTN);
        TextView service_desc = (TextView) content.findViewById(R.id.service_desc);
        TextView hsn_sas_code = (TextView) content.findViewById(R.id.hsn_sas_code);
        TextView tex_amount_pdf = (TextView) content.findViewById(R.id.tex_amount_pdf);
        TextView total_amnt_pdf = (TextView) content.findViewById(R.id.total_amnt_pdf);
        TextView grand_total = (TextView) content.findViewById(R.id.grand_total);
        TextView pdf_vehicle_no = (TextView) content.findViewById(R.id.pdf_vehicle_no);
        TextView tv_shipping_to = content.findViewById(R.id.tv_shipping_add);
        TextView tv_ship_pdf_cust_add1 = (TextView) content.findViewById(R.id.tv_ship_pdf_cust_add1);
        TextView tv_ship_pdf_cust_state = (TextView) content.findViewById(R.id.tv_ship_pdf_cust_state);
        TextView tv_ship_pdf_cust_stateCode = (TextView) content.findViewById(R.id.tv_ship_pdf_cust_stateCode);
        TextView tv_ship_pdf_cust_GSTN = (TextView) content.findViewById(R.id.tv_ship_pdf_cust_GSTN);


        PdfComName.setText(logeedUserDetails.getCompany_name());
        PdfComAdd.setText(logeedUserDetails.getAddress());

        pdf_gstn.setText(logeedUserDetails.getGstn());
        pdf_cin.setText( logeedUserDetails.getCin());
        pdf_pan.setText( logeedUserDetails.getPan());

        pdf_state_code.setText(logeedUserDetails.getState_code());
        pdf_state.setText(logeedUserDetails.getState());


        bottom_com_name.setText(logeedUserDetails.getCompany_name());
        PdfInvoiceNo.setText(myBillDetailPojo.getInvoiceNo());
        PdfDate.setText(myBillDetailPojo.getInvoiceDate());
        bottom_com_name.setText(logeedUserDetails.getCompany_name());
        try {
            pdf_cust_name.setText(myBillDetailPojo.getNameOfSupplier());
            pdf_cust_GSTN.setText(myBillDetailPojo.getGstinUin());
            service_desc.setText(myBillDetailPojo.getGoodServiceDescription());
            hsn_sas_code.setText(myBillDetailPojo.getHsnSacCode());
            total_amnt_pdf.setText(myBillDetailPojo.getAmountPerTax());

            if(myBillDetailPojo.getShipDetails()!=null) {

                String[] shipAdsDetails = myBillDetailPojo.getShipDetails().split(",");

                Log.e("shipAdress:",shipAdsDetails[0]);
            //    String[] shipcode1 = codestate[1].split(",");
                Log.e("shipSate::",shipAdsDetails[1]);
                Log.e("shipXCode::",shipAdsDetails[2]);
                tv_ship_pdf_cust_add1.setText(shipAdsDetails[0]);
               tv_ship_pdf_cust_state.setText("State:"+shipAdsDetails[1]);
              tv_ship_pdf_cust_stateCode.setText("State Code :"+shipAdsDetails[2]);




            }

            if(myBillDetailPojo.getAddress()!=null) {

                String[] billAdsDetails = myBillDetailPojo.getAddress().split(",");

                Log.e("shipAdress:",billAdsDetails[0]);
                //    String[] shipcode1 = codestate[1].split(",");
                Log.e("shipSate::",billAdsDetails[1]);
                Log.e("shipXCode::",billAdsDetails[2]);

                pdf_cust_add1.setText(billAdsDetails[0]);

                pdf_cust_state.setText("State:"+billAdsDetails[1]);
                pdf_cust_stateCode.setText("State Code :"+billAdsDetails[2]);

                stateCode=billAdsDetails[2];

                Log.e("MyBillSC","stateCode"+stateCode);

            }

            if(myBillDetailPojo.getGstinUin()!=null) {
                tv_ship_pdf_cust_GSTN.setText("Customer GSTIN :" + myBillDetailPojo.getGstinUin());
            }


            if(myBillDetailPojo.getGoodServiceDescription()!=null) {




                service_desc.setText(myBillDetailPojo.getGoodServiceDescription());
            }

            if(myBillDetailPojo.getHsnSacCode()!=null) {

                hsn_sas_code.setText(myBillDetailPojo.getHsnSacCode());
            }


            if(myBillDetailPojo.getInvoiceTaxableAmt()!=null) {

                total_amnt_pdf.setText("₹ "+myBillDetailPojo.getInvoiceTaxableAmt());
            }


            if(myBillDetailPojo.getVechicalNo()!=null) {
                pdf_vehicle_no.setText(myBillDetailPojo.getVechicalNo());


            }


            if(myBillDetailPojo.getShipDetails()!=null) {

                tv_shipping_to.setText(myBillDetailPojo.getShipDetails());
            }

            if(myBillDetailPojo.getInvoiceTaxableAmt()!=null) {

                tex_amount_pdf.setText("₹ "+myBillDetailPojo.getInvoiceTaxableAmt());


            }


            String totalgst="";

           /* if(myBillDetailPojo.getSgst()!=null&&(!myBillDetailPojo.getSgst().equalsIgnoreCase("0"))){

                total_text2.setVisibility(View.VISIBLE);

                total_text2.setText("SGST : ₹ "+myBillDetailPojo.getTotalGst()+"");
                total_text.setText("CGST : ₹ "+myBillDetailPojo.getTotalGst());

            }
            else if(myBillDetailPojo.getUgst()!=null&&(!myBillDetailPojo.getUgst().equalsIgnoreCase("0"))){

                total_text2.setVisibility(View.VISIBLE);

                total_text2.setText("CGST : ₹ "+myBillDetailPojo.getTotalGst()+"");
                total_text.setText("UGST : ₹ "+myBillDetailPojo.getTotalGst());

            }
            else if(myBillDetailPojo.getIgst()!=null&&(!myBillDetailPojo.getIgst().equalsIgnoreCase("0"))){

                total_text.setText("IGST : ₹ "+myBillDetailPojo.getTotalGst());


            }
            else {


                total_text.setText("₹ "+myBillDetailPojo.getTotalGst());

            }*/


            grand_total.setText("₹ "+myBillDetailPojo.getTotalAmount() + "");






            if (stateCode.equalsIgnoreCase("35")
                    || stateCode.equalsIgnoreCase("04")
                    || stateCode.equalsIgnoreCase("26")
                    || stateCode.equalsIgnoreCase("25")
                    || stateCode.equalsIgnoreCase("31")
                    || stateCode.equalsIgnoreCase("34") && wender_state_code.equalsIgnoreCase("04")
                    || wender_state_code.equalsIgnoreCase("26")
                    || wender_state_code.equalsIgnoreCase("25")
                    || wender_state_code.equalsIgnoreCase("31")
                    || wender_state_code.equalsIgnoreCase("34")
                    || wender_state_code.equalsIgnoreCase("35")
                    ) {
                if (stateCode.equals(wender_state_code)) {

                    total_text2.setVisibility(View.VISIBLE);

                    total_text2.setText("SGST : ₹ "+myBillDetailPojo.getTotalGst()+"");
                    total_text.setText("UGST : ₹ "+myBillDetailPojo.getTotalGst());

                }


                else {

                    total_text.setText("IGST : ₹ "+myBillDetailPojo.getTotalGst());




                }
            }

            else if (stateCode.equalsIgnoreCase(wender_state_code)) {
                total_text2.setVisibility(View.VISIBLE);

                total_text2.setText("CGST : ₹ "+myBillDetailPojo.getTotalGst()+"");
                total_text.setText("SGST : ₹ "+myBillDetailPojo.getTotalGst());


            }


            else {
                total_text.setText("IGST : ₹ "+myBillDetailPojo.getTotalGst());


            }
            }


            catch (Exception e) {
            e.printStackTrace();
        }

        if (productList != null && productList.size() > 0) {
     Log.e("listsize", "previewBilllist::" + productList.size());
     MyBillInvoiceGenratorAdapter invoiceGenratorAdapter = new MyBillInvoiceGenratorAdapter(productList);
     LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(context);
     rvInvoice.setLayoutManager(mLayoutManager2);
     rvInvoice.setItemAnimator(new DefaultItemAnimator());
     rvInvoice.setAdapter(invoiceGenratorAdapter);


     MyBillTaxSlabAdapter slabAdapter = new MyBillTaxSlabAdapter(gstList, context,myBillDetailPojo,stateCode);
     LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(context);
     recy_tax.setLayoutManager(mLayoutManager1);
     recy_tax.setItemAnimator(new DefaultItemAnimator());
     recy_tax.setAdapter(slabAdapter);
     }
        view = (LinearLayout) content.findViewById(R.id.bitmap_layout);
        view.setDrawingCacheEnabled(true);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache(true);






        bitmap = Bitmap.createBitmap(view.getDrawingCache());
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        final AlertDialog alertDialog = builder.create();
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.bitmap_list_view, null, false);
        ImageView imageView = (ImageView) view1.findViewById(R.id.bitmap_image);
        Button downloadButton = (Button) view1.findViewById(R.id.download_button);
        Button shareButton = (Button) view1.findViewById(R.id.shareButton);
        imageView.setImageBitmap(bitmap);
        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(imageView);
        pAttacher.update();
        alertDialog.setView(view1);
        alertDialog.getWindow();
        WindowManager.LayoutParams params = alertDialog.getWindow().getAttributes();
        params.x = 100;
        params.y = 100;
        alertDialog.getWindow().setAttributes(params);
        alertDialog.show();
        view.setDrawingCacheEnabled(false);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        final String bitmapPath = MediaStore.Images.Media.insertImage(getActivity().getApplicationContext().getContentResolver(), bitmap, "title", null);


       /* File f = new File(Environment.getExternalStorageDirectory() + File.separator + myBillDetailPojo.getInvoice_no() + ".jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(billHeaderN.equalsIgnoreCase("Normal")) {

                  /*  tvBillHeader.setVisibility(View.VISIBLE);
                    tvBillHeader.setText("");*/
                    createAndDownloadPdf(myBillDetailPojo, alertDialog);
                }

                if(billHeaderD.equalsIgnoreCase("Dublicate")) {

                    tvBillHeader.setVisibility(View.VISIBLE);
                    tvBillHeader.setText("Duplicate Copy");
                    createAndDownloadPdfDublicate(myBillDetailPojo, alertDialog);
                }
                if(billHeaderT.equalsIgnoreCase("Triplicate")) {

                    tvBillHeader.setVisibility(View.VISIBLE);
                    tvBillHeader.setText("Triplicate Copy");

                    createAndDownloadTriple(myBillDetailPojo, alertDialog);

                }







            }
        });
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (!hasPermissions(getActivity(), PERMISSIONS)) {

                        requestPermissions(PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                        // this.requestPermissions(getActivity(), PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                    } else if (hasPermissions(getActivity(), PERMISSIONS)) {
                        shareImage(bitmap);

                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


            }
        });

    }

    private void shareImage(Bitmap bitmap) {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");

        String bitmapPath = MediaStore.Images.Media.insertImage(getActivity().getApplicationContext().getContentResolver(), bitmap, "title", null);
        Uri bitmapUri = Uri.parse(bitmapPath);
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
        startActivity(Intent.createChooser(intent, "Share"));

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    shareImage(bitmap);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    CommonUtils.snackBar("Permission Denied By User", searchEdit, "300");
                }
                return;
            }
        }
    }

    private void createAndDownloadPdf(MyBillDetailPojo myBillDetailPojo, Dialog dialog) {


        String externalStorageDirectory = "" + Environment.getExternalStorageDirectory().getAbsolutePath();
        String filePah = externalStorageDirectory + "/GST/" + myBillDetailPojo.getInvoiceNo()+"Normal" + ".pdf";
        File file = new File(filePah);

        File file2 = new File(filePah);
        /*if (!file2.exists()) {
            file2.mkdirs();
        }*/

        try {
            if (file.exists()) {
                file.delete();
            } else {
                //file.mkdir();
            }
            file.getParentFile().mkdirs();

            file.createNewFile();

            PdfDocument.PageInfo pageInfo = null;
            FileOutputStream outputStream;
            PdfDocument pdfDocument;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                pageInfo = new PdfDocument.PageInfo.Builder(2250, 1400, 1).create();

                // start a page
                pdfDocument = new PdfDocument();
                PdfDocument.Page page = pdfDocument.startPage(pageInfo);

                int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight() + 200, View.MeasureSpec.EXACTLY);

                content.measure(measureWidth, measuredHeight);
                content.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                content.draw(page.getCanvas());

                // finish the page
                pdfDocument.finishPage(page);
                // add more pages
                // write the document content
                outputStream = new FileOutputStream(file);
                try {
                    pdfDocument.writeTo(outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // getActivity().finish();
                dialog.dismiss();

               // CommonUtils.snackBar("Normal File Downloaded", content, "300");

                Toast.makeText(getActivity(), "  File Downloaded", Toast.LENGTH_SHORT).show();

            } else {
                CommonUtils.snackBar("cant create PDF on this Device", content, "300");
            }

        } catch (Exception e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }

    private void createAndDownloadPdfDublicate(MyBillDetailPojo myBillDetailPojo, Dialog dialog) {


        String externalStorageDirectory = "" + Environment.getExternalStorageDirectory().getAbsolutePath();
        String filePah = externalStorageDirectory + "/GST/" + myBillDetailPojo.getInvoiceNo()+"Duplicate" + ".pdf";
        File file = new File(filePah);

        File file2 = new File(filePah);
        /*if (!file2.exists()) {
            file2.mkdirs();
        }*/

        try {
            if (file.exists()) {
                file.delete();
            } else {
                //file.mkdir();
            }
            file.getParentFile().mkdirs();

            file.createNewFile();

            PdfDocument.PageInfo pageInfo = null;
            FileOutputStream outputStream;
            PdfDocument pdfDocument;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                pageInfo = new PdfDocument.PageInfo.Builder(2250, 1400, 1).create();

                // start a page
                pdfDocument = new PdfDocument();
                PdfDocument.Page page = pdfDocument.startPage(pageInfo);

                int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight() + 200, View.MeasureSpec.EXACTLY);

                content.measure(measureWidth, measuredHeight);
                content.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                content.draw(page.getCanvas());

                // finish the page
                pdfDocument.finishPage(page);
                // add more pages
                // write the document content
                outputStream = new FileOutputStream(file);
                try {
                    pdfDocument.writeTo(outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // getActivity().finish();
                dialog.dismiss();
               // CommonUtils.snackBar("Dublicate File Downloaded", content, "300");
                Toast.makeText(getActivity(), " Duplicate Copy Downloaded", Toast.LENGTH_SHORT).show();


            } else {
                CommonUtils.snackBar("cant create PDF on this Device", content, "300");
            }

        } catch (Exception e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }

    private void createAndDownloadTriple(MyBillDetailPojo myBillDetailPojo, Dialog dialog) {


        String externalStorageDirectory = "" + Environment.getExternalStorageDirectory().getAbsolutePath();
        String filePah = externalStorageDirectory + "/GST/" + myBillDetailPojo.getInvoiceNo()+"Triplicate" + ".pdf";
        File file = new File(filePah);

        File file2 = new File(filePah);
        /*if (!file2.exists()) {
            file2.mkdirs();
        }*/

        try {
            if (file.exists()) {
                file.delete();
            } else {
                //file.mkdir();
            }
            file.getParentFile().mkdirs();

            file.createNewFile();

            PdfDocument.PageInfo pageInfo = null;
            FileOutputStream outputStream;
            PdfDocument pdfDocument;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                pageInfo = new PdfDocument.PageInfo.Builder(2250, 1400, 1).create();

                // start a page
                pdfDocument = new PdfDocument();
                PdfDocument.Page page = pdfDocument.startPage(pageInfo);

                int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight() + 200, View.MeasureSpec.EXACTLY);

                content.measure(measureWidth, measuredHeight);
                content.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                content.draw(page.getCanvas());

                // finish the page
                pdfDocument.finishPage(page);
                // add more pages
                // write the document content
                outputStream = new FileOutputStream(file);
                try {
                    pdfDocument.writeTo(outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // getActivity().finish();
                dialog.dismiss();
                //Toast.makeText(getActivity(), " Triplicate Copy Downloaded", Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity(), " Triplicate Copy Downloaded", Toast.LENGTH_SHORT).show();

                CommonUtils.snackBar("Triplicate  File Downloaded", content, "300");

            } else {
                CommonUtils.snackBar("cant create PDF on this Device", content, "300");
            }

        } catch (Exception e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }



}
