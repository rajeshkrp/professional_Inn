package gstapp.com.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import UtilClasses.CommonUtils;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AboutFragment extends Fragment {


    ApiInterface apiInterface;
    TextView webView;
    private OnFragmentInteractionListener mListener;

    FloatingActionButton fbHome;

    public AboutFragment() {
        // Required empty public constructor
    }


    public static AboutFragment newInstance(String param1) {
        AboutFragment fragment = new AboutFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

View view=inflater.inflate(R.layout.fragment_about, container, false);

fbHome=view.findViewById(R.id.fbHome);
fbHome.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent i=new Intent(getActivity(), DashBoardActivity.class);
        startActivity(i);

    }
});



return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        apiInterface = CommonUtils.InitilizeInterface();
        webView = (TextView) getView().findViewById(R.id.textHtml);
        getAboutContent();
    }

    private void getAboutContent() {

        Call<ResponseBody> bodyCall = apiInterface.getAboutContent();
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String status = null;
                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("200")) {
                            String data = jsonObject.getString("data");
                            webView.setText(Html.fromHtml(data));
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    CommonUtils.snackBar("Error occured,please try again", webView,status);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
