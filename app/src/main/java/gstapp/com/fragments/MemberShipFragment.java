package gstapp.com.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


import ModelClasses.PlanPOJO;
import UtilClasses.CommonUtils;
import UtilClasses.LogeedUserDetails;
import butterknife.ButterKnife;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 4/5/2018.
 */

public class MemberShipFragment extends Fragment {

    TextView plan_basic, plan_gold, plan_platinum, benifit_basic, benifit_gold, benifit_platinum, period_basic, period_gold, period_platinum, price_basic, price_gold, price_platinum;
    Button btn_request_basic, btn_request_gold, btn_request_platinum;
    private ApiInterface apiInterface;
    Dialog mDialog;
    String s="";
    FloatingActionButton fbHome;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_membership, container, false);
        fbHome=view.findViewById(R.id.fbHome);


        ButterKnife.bind(this, view);

        plan_basic = view.findViewById(R.id.plan_basic);
        plan_gold = view.findViewById(R.id.plan_gold);
        plan_platinum = view.findViewById(R.id.plan_platinum);
        benifit_basic = view.findViewById(R.id.benifit_basic);
        benifit_gold = view.findViewById(R.id.benifit_gold);
        benifit_platinum = view.findViewById(R.id.benifit_platinum);
        period_basic = view.findViewById(R.id.period_basic);
        period_gold = view.findViewById(R.id.period_gold);
        period_platinum = view.findViewById(R.id.period_platinum);
        price_basic = view.findViewById(R.id.price_basic);
        price_gold = view.findViewById(R.id.price_gold);
        price_platinum = view.findViewById(R.id.price_platinum);
        btn_request_basic = view.findViewById(R.id.btn_request_basic);
        btn_request_gold = view.findViewById(R.id.btn_request_gold);
        btn_request_platinum = view.findViewById(R.id.btn_request_platinum);
        apiInterface = APIClient.getClient().create(ApiInterface.class);


        LogeedUserDetails userDetails = CommonUtils.getUserDetail(getActivity());
        userDetails.getfName();
        final String name=userDetails.getfName().concat(userDetails.getMName()).concat(userDetails.getLName());


        fbHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getActivity(), DashBoardActivity.class);
                startActivity(i);
            }
        });



        btn_request_basic.setOnClickListener(new View.OnClickListener() {

            final String plan=plan_basic.getText().toString();
            final String benfit=benifit_basic.getText().toString();
            final String periods=period_basic.getText().toString();
            final String prices=price_basic.getText().toString();
            @Override
            public void onClick(View v) {


                Call<PlanPOJO> PlanPOJOCall = apiInterface.getRequestPlan(name,plan,benfit,periods,prices);

                PlanPOJOCall.enqueue(new Callback<PlanPOJO>() {
                    @Override
                    public void onResponse(Call<PlanPOJO> call, Response<PlanPOJO> response) {

                        PlanPOJO planPOJO = response.body();
                       // Toast.makeText(getActivity() ,plan+" "+planPOJO.getMessage(), Toast.LENGTH_SHORT).show();
                        s=plan+" "+planPOJO.getMessage();
                        openBasicDialog(s);
                    }
                    @Override
                    public void onFailure(Call<PlanPOJO> call, Throwable t) {

                    }
                });

            }
        });




        btn_request_gold.setOnClickListener(new View.OnClickListener() {

            final String plan=plan_gold.getText().toString();
            final String benfit=benifit_gold.getText().toString();
            final String periods=period_gold.getText().toString();
            final String prices=price_gold.getText().toString();


            @Override
            public void onClick(View v) {
                Call<PlanPOJO> PlanPOJOCall = apiInterface.getRequestPlan(name,plan,benfit,periods,prices);

                PlanPOJOCall.enqueue(new Callback<PlanPOJO>() {
                    @Override
                    public void onResponse(Call<PlanPOJO> call, Response<PlanPOJO> response) {

                        PlanPOJO planPOJO = response.body();
                        //Toast.makeText(getActivity() ,plan+" "+planPOJO.getMessage(), Toast.LENGTH_SHORT).show();
                        s=plan+" "+planPOJO.getMessage();
                        openGoldDialog(s);

                    }
                    @Override
                    public void onFailure(Call<PlanPOJO> call, Throwable t) {

                    }
                });



            }
        });


        btn_request_platinum.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                final String plan=plan_platinum.getText().toString();
                final String benfit=benifit_platinum.getText().toString();
                final String periods=period_platinum.getText().toString();
                final String prices=price_platinum.getText().toString();
                Call<PlanPOJO> PlanPOJOCall = apiInterface.getRequestPlan(name,plan,benfit,periods,prices);

                PlanPOJOCall.enqueue(new Callback<PlanPOJO>() {
                    @Override
                    public void onResponse(Call<PlanPOJO> call, Response<PlanPOJO> response) {

                        PlanPOJO planPOJO = response.body();
                        //Toast.makeText(getActivity() ,plan+" "+planPOJO.getMessage(), Toast.LENGTH_SHORT).show();
                        s=plan+" "+planPOJO.getMessage();
                        openPlatinumDialog(s);

                    }
                    @Override
                    public void onFailure(Call<PlanPOJO> call, Throwable t) {

                    }
                });


            }
        });

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View getView() {
        return super.getView();
    }
    public void openBasicDialog(String name) {
        final TextView tv_dialog;
       /* final String plan=plan_gold.getText().toString();
        final String benfit=benifit_gold.getText().toString();
        final String periods=period_gold.getText().toString();
        final String prices=price_gold.getText().toString();*/

        mDialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.getWindow();
        mDialog.setContentView(R.layout.dialog_membership_plan);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        tv_dialog = (TextView) mDialog.findViewById(R.id.tv_dialog);
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
       tv_dialog.setText(name);
        mDialog.show();
        mDialog.setCanceledOnTouchOutside(true);


    }

    public void openGoldDialog(String name) {
        final TextView tv_dialog;

        mDialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.getWindow();
        mDialog.setContentView(R.layout.dialog_membership_plan);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        tv_dialog = (TextView) mDialog.findViewById(R.id.tv_dialog);
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        mDialog.show();
        mDialog.setCanceledOnTouchOutside(true);


    }

    public void openPlatinumDialog(String name) {
        final TextView tv_dialog;

        mDialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.getWindow();
        mDialog.setContentView(R.layout.dialog_membership_plan);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        tv_dialog = (TextView) mDialog.findViewById(R.id.tv_dialog);
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tv_dialog.setText(name);
        mDialog.show();
        mDialog.setCanceledOnTouchOutside(true);


    }
}
