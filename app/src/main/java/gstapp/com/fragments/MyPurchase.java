package gstapp.com.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ModelClasses.ImageEntry;
import ModelClasses.ProfileUpdateReciever;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.Adapters.PurchaseAdapter;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyPurchase extends Fragment {


    Button uploadButton;
    ImageView imageView;
    ArrayList<ImageEntry> mSelectedImages;
    ProgressDialog progressDialog;
    Context context;
    FloatingActionButton fbHome;


    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 122;

    static MyPurchase myPurchase;
    private Uri uri;

    ApiInterface apiInterface;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.pager)
    ViewPager viewPager;
    private PurchaseAdapter adapter;
    private Context mcontext;



    public MyPurchase() {
        // Required empty public constructor
    }

    public static MyPurchase newInstance() {

        if (myPurchase == null) {
            myPurchase = new MyPurchase();
        }
        return myPurchase;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mcontext=getActivity();





        View view = inflater.inflate(R.layout.fragment_my_purchase, container, false);
        ButterKnife.bind(this, view);
        context=getActivity();


        fbHome=view.findViewById(R.id.fbHome);



        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mcontext=getActivity();



        tabLayout.addTab(tabLayout.newTab().setText("View Purchased Bills"));
        tabLayout.addTab(tabLayout.newTab().setText("Upload Purchased Bills"));


        fbHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getActivity(), DashBoardActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);






            }
        });
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        adapter = new PurchaseAdapter(this.getChildFragmentManager(), tabLayout.getTabCount(),context);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
       // viewPager.setOffscreenPageLimit(2);

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

               // Toast.makeText(context, "onTabSelected", Toast.LENGTH_SHORT).show();



                Intent localIntent = new Intent("Upload");
                localIntent.putExtra("Upload","Upload");
                LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);



            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {

             //   Toast.makeText(context, "addOnPageChangeListener", Toast.LENGTH_SHORT).show();


                Intent localIntent = new Intent("Upload");
                localIntent.putExtra("Upload","Upload");
                LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);

             //   callLocalBrd(ProfileUpdateReciever.UPDATE_INTENT);

            }
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                //callLocalBrd(ProfileUpdateReciever.UPDATE_INTENT);

            }


            public void onPageSelected(int position) {
             //   Toast.makeText(context, "onPageSelected", Toast.LENGTH_SHORT).show();

                // Check if this is the page you want.
            }
        });










    }


    @Override
    public void onDestroy() {
        super.onDestroy();
      //  context.unregisterReceiver(localBroadcastManager);

    }
    private void callLocalBrd(final String upadtpostion) {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setAction(upadtpostion);
                mcontext.sendBroadcast(intent);
            }
        },500);


    }



    @Override
    public void onDetach() {
        super.onDetach();

    }


    private void pickImages() {

    }


}
