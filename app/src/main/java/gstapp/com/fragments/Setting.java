package gstapp.com.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import UtilClasses.CommonUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Setting extends Fragment {


    EditText currentPass;
    EditText newPass;
    EditText confirmPass;
    Button submitButton;
    FloatingActionButton fbHome;
    Context context;
    ConnectivityManager connMgr;
    NetworkInfo networkInfo;
    ApiInterface apiInterface;
    Button btn_refresh;

    public static Setting newInstance() {
        Setting fragment = new Setting();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = CommonUtils.InitilizeInterface();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        context=getActivity();
        connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            view = inflater.inflate(R.layout.fragment_setting, container, false);


            currentPass = (EditText) view.findViewById(R.id.currentPassEdit);
            newPass = (EditText) view.findViewById(R.id.newPassEdit);
            confirmPass = (EditText) view.findViewById(R.id.confirmPassEdit);
            submitButton = (Button) view.findViewById(R.id.submitpassButton);
            fbHome =(FloatingActionButton) view.findViewById(R.id.fbHome);


        } else {

            view = inflater.inflate(R.layout.no_internet_connection, container, false);

            btn_refresh=view.findViewById(R.id.btn_refresh);
        }



        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (networkInfo != null && networkInfo.isConnected()) {


            submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String oldPass = currentPass.getText().toString();
                    String newPassword = newPass.getText().toString();
                    String confPass = confirmPass.getText().toString();
                    if (oldPass.length() != 0 && newPassword.length() != 0 && confPass.length() != 0) {

                        if (newPassword.equals(confPass)) {
                            Call<ResponseBody> bodyCall = apiInterface.changePassword(CommonUtils.getUserId(getActivity()), oldPass, newPassword);

                            bodyCall.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                    if (response != null) {
                                        try {
                                            String res = response.body().string();
                                            JSONObject jsonObject = new JSONObject(res);
                                            String code = jsonObject.getString("code");
                                            String msg = jsonObject.getString("message");
                                            if (code.equals("200")) {
                                                CommonUtils.snackBar(msg, currentPass, code);

                                            } else {
                                                CommonUtils.snackBar(msg, currentPass, code);

                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                    Log.v("Error", t.toString());
                                }
                            });

                        } else {
                            CommonUtils.snackBar("New Password and confirm password should be same", currentPass, "300");
                        }

                    } else {

                    }


                }
            });

            fbHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(getActivity(), DashBoardActivity.class);
                    startActivity(i);
                }
            });


        }else {
            Log.d("NO_INTERNET","NO_INTERNET");
            btn_refresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.detach(Setting.this).attach(Setting.this).commit();
                }
            });


        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
context=getActivity();

         connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
         networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

        Log.d("TAG","hiii");
        }else {

        }

        }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
