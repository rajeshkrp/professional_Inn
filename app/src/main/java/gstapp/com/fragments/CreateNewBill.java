package gstapp.com.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.pdf.PdfDocument.PageInfo;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

import UtilClasses.CommonUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreateNewBill extends Fragment {

    private OnFragmentInteractionListener mListener;

    AppCompatSpinner appCompatSpinner;
    AppCompatSpinner appCompatSpinner2;

    HashMap<String, String> stateCodes;

    @BindView(R.id.supplier_name)
    EditText supplier_name;

    @BindView(R.id.supplier_address)
    EditText supplier_address;

    @BindView(R.id.supplier_GSTN)
    EditText supplier_GSTN;

    @BindView(R.id.product_desc)
    EditText product_desc;

    @BindView(R.id.sac_HSN)
    EditText sac_HSN;

    @BindView(R.id.stateCodeTxt)
    TextView stateCodeTxt;

    @BindView(R.id.newInvoiceNo)
    TextView newInvoiceNo;

    @BindView(R.id.CGSTRadio)
    RadioButton CGSTRadio;

    @BindView(R.id.IGSTRadio)
    RadioButton IGSTRadio;

    @BindView(R.id.CGSTAmountEdit)
    EditText CGSTAmountEdit;

    @BindView(R.id.SGSTAmountEdit)
    EditText SGSTAmountEdit;

    @BindView(R.id.IGSTAmountEdit)
    EditText IGSTAmountEdit;

    @BindView(R.id.CGSTEdit)
    EditText CGSTEdit;

    @BindView(R.id.SGSTEdit)
    EditText SGSTEdit;

    @BindView(R.id.IGSTEdit)
    EditText IGSTEdit;

    @BindView(R.id.amountEdit)
    EditText amountEdit;

    @BindView(R.id.submit_button)
    Button submit_button;

    public static final String DEST = "results/chapter01/rick_astley.pdf";


    String selectedCatgeory, amount, stateName, currentDate;
    private int enteredAmount;
    ApiInterface apiInterface;
    private ProgressDialog progressDailog;
    private String billNo;
    private String totalTaxAmount;

    public CreateNewBill() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_new_bill, container, false);
        ButterKnife.bind(this, view);
        apiInterface = APIClient.getClient().create(ApiInterface.class);
        getPermission();

        getBillNo();
        return view;
    }


    private void getPermission() {


    }


    private void getBillNo() {

        Call<ResponseBody> responseBodyCall = apiInterface.getBillNo("2");

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String res;
                try {
                    res = response.body().string();

                    JSONObject jsonObject = new JSONObject(res);
                    String status = jsonObject.getString("status");
                    if (status.equals("200")) {

                        billNo = jsonObject.getString("bill_no");
                        newInvoiceNo.setText(billNo);
                    } else {
                        checkandGetBillFromLocal();

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void checkandGetBillFromLocal() {

        billNo = CommonUtils.getBillNo(getActivity());

        if (billNo != null && !billNo.equals("")) {
            increamentAndSaveBillNo(billNo);
        } else {
            ShowAlertDialog();
        }

    }

    private void increamentAndSaveBillNo(String bilNo) {

        String numOnly = bilNo.replaceAll("[\\D]", "");
        long num = Long.parseLong(numOnly);
        String newstr = bilNo.replaceAll("[^A-Za-z]+", "");

        long newNum = num + 1;

        String requiredString = newstr + newNum;

        newInvoiceNo.setText("Invoice No : #" + requiredString);

        saveInvoiceLocal(requiredString);


    }

    private void ShowAlertDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Please Update Bill Series No");
        builder.setCancelable(false);


        builder.setPositiveButton("Update Profile", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                replaceProfileFragment();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                replaceHomeFragment();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void replaceHomeFragment() {

        ((DashBoardActivity) getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("");
        HomeFragment myProfile = HomeFragment.newInstance();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainLayout, myProfile).commit();

    }

    private void replaceProfileFragment() {

        ((DashBoardActivity) getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("Manage Company Details");
        MyProfile myProfile = MyProfile.newInstance();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainLayout, myProfile).commit();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        progressDailog = new ProgressDialog(getActivity());
        progressDailog.setMessage("Please wait...");
        appCompatSpinner = (AppCompatSpinner) getView().findViewById(R.id.spinner);
        appCompatSpinner2 = (AppCompatSpinner) getView().findViewById(R.id.spinner2);


        stateCodes = new HashMap<>();
        CGSTRadio.setChecked(true);

        LinkedList<String> stateList = new LinkedList<>();
        LinkedList<String> taxCat = new LinkedList<>();
        taxCat.add("28%");
        taxCat.add("18%");
        taxCat.add("5%");

        prepareHashMap(stateCodes);
        prepareStateList(stateList);


        amountEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String d = s.toString();
                if (!d.equals("")) {
                    float selectedValue = Float.parseFloat(selectedCatgeory);
                    calculateAmount(selectedValue);
                }

            }
        });

        CGSTRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CGSTRadio.setChecked(true);
                IGSTRadio.setChecked(false);
               /* float selectedValue = Float.parseFloat(selectedCatgeory);
                CGSTEdit.setText(selectedValue / 2 + " %");
                SGSTEdit.setText(selectedValue / 2 + " %");

                IGSTEdit.setText("");
                IGSTAmountEdit.setText("");

                calculateAmount(selectedValue);*/
            }
        });

        IGSTRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CGSTRadio.setChecked(false);
                IGSTRadio.setChecked(true);

                /*CGSTEdit.setText("");
                SGSTEdit.setText("");
                CGSTAmountEdit.setText("");
                SGSTAmountEdit.setText("");
                float selectedValue = Float.parseFloat(selectedCatgeory);
                IGSTEdit.setText(selectedValue + " %");

                calculateAmount(selectedValue);*/
            }
        });

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, stateList);
        appCompatSpinner.setAdapter(arrayAdapter);

        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, taxCat);
        appCompatSpinner2.setAdapter(arrayAdapter2);

        appCompatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String content = appCompatSpinner.getSelectedItem().toString();
                stateName = content;
                stateCodeTxt.setText(stateCodes.get(content));
                String newStateCode = stateCodes.get(content);
                if (newStateCode.equals("07")) {
//select CGST....................

                    float selectedValue = Float.parseFloat(selectedCatgeory);
                    CGSTEdit.setText(selectedValue / 2 + " %");
                    SGSTEdit.setText(selectedValue / 2 + " %");

                    IGSTEdit.setText("");
                    IGSTAmountEdit.setText("");

                    calculateAmount(selectedValue);
                } else {

                    //selectIGST/////...................

                    CGSTEdit.setText("");
                    SGSTEdit.setText("");
                    CGSTAmountEdit.setText("");
                    SGSTAmountEdit.setText("");
                    float selectedValue = 0;
                    if (selectedCatgeory != null) {
                        selectedValue = Float.parseFloat(selectedCatgeory);
                    }

                    IGSTEdit.setText(selectedValue + " %");

                    calculateAmount(selectedValue);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        appCompatSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCatgeory = appCompatSpinner2.getSelectedItem().toString().replace("%", "");

                float selectedValue = Float.parseFloat(selectedCatgeory);


                if (stateCodeTxt.getText().toString().equals("07")) {
                    CGSTEdit.setText(selectedValue / 2 + " %");
                    SGSTEdit.setText(selectedValue / 2 + " %");

                    IGSTEdit.setText("");
                } else {
                    CGSTEdit.setText("");
                    SGSTEdit.setText("");

                    IGSTEdit.setText(selectedValue + " %");
                }

                calculateAmount(selectedValue);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDailog.show();
                getCurrentData();

            }
        });
    }

    private void getCurrentData() {

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(date);
        currentDate = formattedDate;

        SubmitDetails();
    }

    private void SubmitDetails() {

        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("user_id", "3");
        hashMap.put("name_of_supplier", supplier_name.getText().toString());
        hashMap.put("gstin_uin", supplier_GSTN.getText().toString());
        hashMap.put("state_name", stateName);
        hashMap.put("pos", stateCodeTxt.getText().toString());
        hashMap.put("invoice_no", newInvoiceNo.getText().toString());
        hashMap.put("invoice_date", currentDate);
        hashMap.put("invoice_value", "20");
        hashMap.put("hsn_sac_code", sac_HSN.getText().toString());
        hashMap.put("description", product_desc.getText().toString());
        hashMap.put("rate", selectedCatgeory);
        hashMap.put("invoice_taxable_amt", amountEdit.getText().toString());
        hashMap.put("qty", "1");
        hashMap.put("igst", IGSTAmountEdit.getText().toString());
        hashMap.put("igst_per", IGSTEdit.getText().toString().replace("%", ""));
        hashMap.put("cgst", CGSTAmountEdit.getText().toString());
        hashMap.put("cgst_per", CGSTEdit.getText().toString().replace("%", ""));
        hashMap.put("sgst", SGSTAmountEdit.getText().toString());
        hashMap.put("sgst_per", SGSTEdit.getText().toString().replace("%", ""));
        hashMap.put("address", supplier_address.getText().toString());

        Call<ResponseBody> bodyCall = apiInterface.CreateBill(hashMap);

        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String status;
                progressDailog.dismiss();
                try {
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equals("200")) {
                        saveLocal();
                        CreatePDf();
                        // ShowThankYouScreen();
                        CommonUtils.snackBar(message, amountEdit, status);
                    } else {
                        CommonUtils.snackBar(message, amountEdit, status);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                progressDailog.dismiss();
                CommonUtils.snackBar(t.toString(), amountEdit, "300");
                Log.e("Error", t.toString());
            }
        });
    }

    private void saveLocal() {

        String bilNo = CommonUtils.getBillNo(getActivity());
        String numOnly = bilNo.replaceAll("[\\D]", "");
        long num = Long.parseLong(numOnly);
        String newstr = bilNo.replaceAll("[^A-Za-z]+", "");

        long newNum = num + 1;

        String requiredString = newstr + newNum;
        newInvoiceNo.setText("Invoice No : #" + requiredString);
        saveInvoiceLocal(requiredString);

    }

    private void saveInvoiceLocal(String requiredString) {

        CommonUtils.saveBillNo(getActivity(), requiredString);
    }

    private void ShowThankYouScreen() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.thank_you_screen, null, false);
        builder.setView(view);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button view_bill = (Button) view.findViewById(R.id.view_bill);
        view_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void CreatePDf() throws IOException {


        String externalStorageDirectory = "" + Environment.getExternalStorageDirectory().getAbsolutePath();
        String filePah = externalStorageDirectory + "/GST/" + billNo + ".pdf";
        File file = new File(filePah);

        try {
            if (file.exists()) {
                file.delete();
            }
            file.getParentFile().mkdirs();

            file.createNewFile();

            PageInfo pageInfo = null;
            FileOutputStream outputStream;
            PdfDocument pdfDocument;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                pageInfo = new PageInfo.Builder(2250, 1400, 1).create();

                // start a page
                pdfDocument = new PdfDocument();
                PdfDocument.Page page = pdfDocument.startPage(pageInfo);


                // draw something on the page
                LayoutInflater inflater = (LayoutInflater)
                        getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View content = inflater.inflate(R.layout.pdf_layout, null);

                TextView PdfComName = (TextView) content.findViewById(R.id.pdf_come_name);
                TextView PdfComAdd = (TextView) content.findViewById(R.id.pdf_com_address);
                TextView PdfInvoiceNo = (TextView) content.findViewById(R.id.pdf_invoice);
                TextView PdfDate = (TextView) content.findViewById(R.id.pdf_date);

                TextView pdf_gstn = (TextView) content.findViewById(R.id.pdf_gstn);
                TextView pdf_cin = (TextView) content.findViewById(R.id.pdf_cin);
                TextView pdf_pan = (TextView) content.findViewById(R.id.pdf_pan);
                TextView pdf_state_code = (TextView) content.findViewById(R.id.pdf_state_code);
                TextView pdf_state = (TextView) content.findViewById(R.id.pdf_state);
                TextView pdf_cust_name = (TextView) content.findViewById(R.id.pdf_cust_name);
               // TextView igst_pdf = (TextView) content.findViewById(R.id.igst_pdf);

                TextView pdf_cust_add1 = (TextView) content.findViewById(R.id.pdf_cust_add1);
                TextView pdf_cust_state = (TextView) content.findViewById(R.id.pdf_cust_state);
                TextView pdf_cust_stateCode = (TextView) content.findViewById(R.id.pdf_cust_stateCode);
                TextView pdf_cust_GSTN = (TextView) content.findViewById(R.id.pdf_cust_GSTN);
                TextView service_desc = (TextView) content.findViewById(R.id.service_desc);
                TextView hsn_sas_code = (TextView) content.findViewById(R.id.hsn_sas_code);
                TextView tex_amount_pdf = (TextView) content.findViewById(R.id.tex_amount_pdf);
                TextView total_amnt_pdf = (TextView) content.findViewById(R.id.total_amnt_pdf);
                TextView grand_total = (TextView) content.findViewById(R.id.grand_total);


                PdfComName.setText("Wehyphens Pvt Ltd");
                PdfComAdd.setText("Noida Sect 64");
                PdfInvoiceNo.setText(newInvoiceNo.getText().toString());
                PdfDate.setText(currentDate);
                pdf_gstn.setText(supplier_GSTN.getText().toString());

                pdf_cust_name.setText(supplier_name.getText().toString());
                pdf_cust_add1.setText(supplier_address.getText().toString());
                pdf_cust_state.setText(supplier_address.getText().toString());
                pdf_cust_stateCode.setText(stateCodeTxt.getText().toString());
                pdf_cust_GSTN.setText(supplier_GSTN.getText().toString());
                service_desc.setText(product_desc.getText().toString());
                hsn_sas_code.setText(sac_HSN.getText().toString());
                tex_amount_pdf.setText(amountEdit.getText().toString());
                total_amnt_pdf.setText(amountEdit.getText().toString());
               // igst_pdf.setText(totalTaxAmount);
                grand_total.setText(String.valueOf(calcualteGrandTotal()));


                int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight() + 200, View.MeasureSpec.EXACTLY);

                content.measure(measureWidth, measuredHeight);
                content.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                content.draw(page.getCanvas());

                // finish the page
                pdfDocument.finishPage(page);
                // add more pages
                // write the document content
                outputStream = new FileOutputStream(file);
                try {
                    pdfDocument.writeTo(outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {

        }
    }

    private int calcualteGrandTotal() {

        String amount = amountEdit.getText().toString();
        String taxAmount = totalTaxAmount;

        int GrandTotal = Integer.parseInt(amount) + Integer.parseInt(taxAmount);

        return GrandTotal;
    }

    private View getContentView() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.thank_you_screen, null, false);
        return view;
    }

    private void calculateAmount(float selectedValue) {

        int percentAmount;
        String hh = amountEdit.getText().toString();
        if (hh != null && !hh.equals("")) {

            enteredAmount = Integer.parseInt(hh);

            if (stateCodeTxt.getText().toString().equals("07")) {
                percentAmount = (int) (enteredAmount * (selectedValue / 2) / 100);
                totalTaxAmount = String.valueOf(percentAmount * 2);
                CGSTAmountEdit.setText(percentAmount + "");
                SGSTAmountEdit.setText(percentAmount + "");
            } else {
                percentAmount = (int) (enteredAmount * (selectedValue) / 100);
                totalTaxAmount = String.valueOf(percentAmount);
                IGSTAmountEdit.setText(percentAmount + "");
            }


        }


    }

    private void prepareStateList(LinkedList<String> stateList) {

        stateList.add("Andaman and Nicobar Islands");
        stateList.add("Andhra Pradesh");
        stateList.add("Arunachal Pradesh");
        stateList.add("Assam");
        stateList.add("Bihar");
        stateList.add("Chandigarh");
        stateList.add("Chhattisgarh");
        stateList.add("Dadra and Nagar Haveli");
        stateList.add("Daman and Diu");
        stateList.add("Delhi");
        stateList.add("Goa");
        stateList.add("Gujarat");
        stateList.add("Haryana");
        stateList.add("Himachal Pradesh");
        stateList.add("Jammu & Kashmir");
        stateList.add("Jharkhand");
        stateList.add("Karnataka");
        stateList.add("Kerala");
        stateList.add("Lakshadweep Islands");
        stateList.add("Madhya Pradesh");
        stateList.add("Maharashtra");
        stateList.add("Manipur");
        stateList.add("Meghalaya");
        stateList.add("Mizoramh");
        stateList.add("Nagaland");
        stateList.add("Odisha");
        stateList.add("Pondicherry");
        stateList.add("Punjab");
        stateList.add("Rajasthan");
        stateList.add("Sikkim");
        stateList.add("Tamil Nadu");
        stateList.add("Telangana");
        stateList.add("Tripura");
        stateList.add("Uttar Pradesh");
        stateList.add("Uttarakhand");
        stateList.add("West Bengal");
    }

    private void prepareHashMap(HashMap<String, String> stateCodes) {

        stateCodes.put("Andaman and Nicobar Islands", "35");
        stateCodes.put("Andhra Pradesh", "37");
        stateCodes.put("Arunachal Pradesh", "12");
        stateCodes.put("Assam", "18");
        stateCodes.put("Bihar", "10");
        stateCodes.put("Chandigarh", "04");
        stateCodes.put("Chhattisgarh", "22");
        stateCodes.put("Dadra and Nagar Haveli", "26");
        stateCodes.put("Daman and Diu", "25");
        stateCodes.put("Delhi", "07");
        stateCodes.put("Goa", "30");
        stateCodes.put("Gujarat", "24");
        stateCodes.put("Haryana", "06");
        stateCodes.put("Himachal Pradesh", "02");
        stateCodes.put("Jammu & Kashmir", "01");
        stateCodes.put("Jharkhand", "20");
        stateCodes.put("Karnataka", "29");
        stateCodes.put("Kerala", "32");
        stateCodes.put("Lakshadweep Islands", "31");
        stateCodes.put("Madhya Pradesh", "23");
        stateCodes.put("Maharashtra", "27");
        stateCodes.put("Manipur", "14");
        stateCodes.put("Meghalaya", "17");
        stateCodes.put("Mizoramh", "15");
        stateCodes.put("Nagaland", "13");
        stateCodes.put("Odisha", "21");
        stateCodes.put("Pondicherry", "34");
        stateCodes.put("Punjab", "03");
        stateCodes.put("Rajasthan", "08");
        stateCodes.put("Sikkim", "11");
        stateCodes.put("Tamil Nadu", "33");
        stateCodes.put("Telangana", "36");
        stateCodes.put("Tripura", "16");
        stateCodes.put("Uttar Pradesh", "09");
        stateCodes.put("Uttarakhand", "05");
        stateCodes.put("West Bengal", "19");

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
