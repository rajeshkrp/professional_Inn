package gstapp.com.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;

public class NewsFeedFragment extends Fragment {
    FloatingActionButton fbHome;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_news_feed, container, false);


        fbHome = view.findViewById(R.id.fbHome);


        fbHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), DashBoardActivity.class);
                startActivity(i);
            }
        });
        return view;

    }
}
