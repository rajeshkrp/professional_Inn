package gstapp.com.login_module;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Abdul on 4/10/2018.
 */

public interface LoginInteractor {

    interface OnLoginFinishedListener {
        void onUsernameError();

        void onPasswordError();

        void onSuccess();

        SharedPreferences getSharePreference();

        void onRoleError(String message);
    }

    void login(String username, String password, OnLoginFinishedListener listener, Context context);
}
