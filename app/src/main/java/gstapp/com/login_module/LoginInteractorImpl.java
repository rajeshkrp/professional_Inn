package gstapp.com.login_module;


import android.content.Context;
import android.content.SharedPreferences;
import android.telecom.Call;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import UtilClasses.CommonUtils;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginInteractorImpl implements LoginInteractor,LoginActivity.GetContext {

    ApiInterface apiInterface;
    SharedPreferences userPreference;
    Context context;

    @Override
    public void login(String username, String password, final OnLoginFinishedListener listener,Context context) {

    this.context=context;
        apiInterface = APIClient.getClient().create(ApiInterface.class);
        userPreference = listener.getSharePreference();



        retrofit2.Call<ResponseBody> bn = apiInterface.createUser(username, password);
        bn.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, Response<ResponseBody> response) {

                String res = null;
                try {
                    res = response.body().string();

                    Log.e("loginInteractor",res);
                    if (res != null) {
                        JSONObject jsonObject = new JSONObject(res);
                        int status = jsonObject.getInt("status");
                        String message = jsonObject.getString("message");
                        if (status == 200) {
                            JSONArray jsonArray = jsonObject.getJSONArray("user");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                String userID = jsonObject1.getString("id");
                                String first_name = jsonObject1.getString("first_name");
                                String mid_name = jsonObject1.getString("mid_name");
                                String last_name = jsonObject1.getString("last_name");
                                String gender = jsonObject1.getString("gender");
                                String email = jsonObject1.getString("email");
                                String password = jsonObject1.getString("password");
                                String role = jsonObject1.getString("role");
                                String company_name = jsonObject1.getString("company_name");
                                String company_logo = jsonObject1.getString("company_logo");
                                String gstn = jsonObject1.getString("gstn");
                                String cin = jsonObject1.getString("cin");
                                String pan = jsonObject1.getString("pan");
                                String bank_name = jsonObject1.getString("bank_name");
                                String bank_branch = jsonObject1.getString("bank_branch");
                                String ac_no = jsonObject1.getString("ac_no");
                                String ifsc_code = jsonObject1.getString("ifsc_code");
                                String ac_type = jsonObject1.getString("ac_type");
                                String bill_no_series = jsonObject1.getString("bill_no_series");
                                String state_code = jsonObject1.getString("state_code");




                              //  CommonUtils.saveStringPreferences(context,"STATE_CODE",state_code);

                                String state = jsonObject1.getString("state");

                                CommonUtils.saveStringPreferences(context,"Admin_status_code",state_code);
                                CommonUtils.saveStringPreferences(context,"Admin_state",state);


                                String address = jsonObject1.getString("address");
                                String mobile = jsonObject1.getString("mobile");
                                String created_at = jsonObject1.getString("created_at");

                                SharedPreferences.Editor editor = userPreference.edit();
                                editor.putString("userID", userID);
                                editor.putString("first_name", first_name);
                                editor.putString("mid_name", mid_name);
                                editor.putString("last_name", last_name);
                                editor.putString("gender", gender);
                                editor.putString("email", email);
                                editor.putString("password", password);
                                editor.putString("role", role);
                                editor.putString("company_name", company_name);
                                editor.putString("company_logo", company_logo);
                                editor.putString("gstn", gstn);
                                editor.putString("cin", cin);
                                editor.putString("pan", pan);
                                editor.putString("bank_name", bank_name);
                                editor.putString("bank_branch", bank_branch);
                                editor.putString("ac_no", ac_no);
                                editor.putString("ifsc_code", ifsc_code);
                                editor.putString("ac_type", ac_type);
                                editor.putString("bill_no_series", bill_no_series);
                                editor.putString("state_code", state_code);
                                editor.putString("state", state);
                                editor.putString("address", address);
                                editor.putString("mobile", mobile);
                                editor.putString("created_at", created_at);
                                editor.putBoolean("isLoggedIn", true);
                                editor.apply();

                                if (role.equals("2")) {

                                    listener.onSuccess();
                                } else {
                                    listener.onRoleError(message);
                                }
                            }

                        } else {
                            listener.onUsernameError();
                        }
                    } else {
                        listener.onUsernameError();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
                listener.onUsernameError();
            }
        });

    }


    @Override
    public void getcontext(Context context) {
    context=context;

    }
}
