package gstapp.com.login_module;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import UtilClasses.CommonUtils;
import gstapp.com.create_invoice_module.InterGetDEtails;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;

public class LoginActivity extends AppCompatActivity implements LoginView {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    ProgressDialog progressDialog;
    Context context;
    Activity activtiy;

    private LoginPresenter presenter;
    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;

    SharedPreferences userPreference;
    private RelativeLayout outerLayouit;

    GetContext getContext;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context=LoginActivity.this;
        activtiy=LoginActivity.this;
//        getContext=(GetContext)activtiy;

                // Set up the login form.
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Loading...");
        mEmailView = (EditText) findViewById(R.id.loginId);
        mPasswordView = (EditText) findViewById(R.id.password);
        outerLayouit = (RelativeLayout) findViewById(R.id.outerLayouit);



        mEmailView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                mEmailView.setCursorVisible(true);

                return false;
            }
        });
        presenter = new LoginPresenterImp(this, new LoginInteractorImpl());

//        getContext.getcontext(context);

        findViewById(R.id.continueLAyout).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEmailView.length() == 0) {

                    CommonUtils.snackBar("Please Enter Login ID", outerLayouit, "300");
                } else if (mPasswordView.length() == 0) {
                    CommonUtils.snackBar("Please Enter Password", outerLayouit, "300");

                } else if (mEmailView.length() != 0 && mPasswordView.length() != 0) {

                    presenter.validateCredentials(mEmailView.getText().toString(), mPasswordView.getText().toString(),context);


                } else {
                    Toast.makeText(LoginActivity.this, "Please fill all required Fields", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    interface  GetContext{

      public void  getcontext(Context context);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    @Override
    public void showProgress() {

        progressDialog.show();
    }

    @Override
    public void hideProgress() {

        progressDialog.dismiss();
    }

    @Override
    public void setUsernameError() {

        CommonUtils.snackBar("Error Ocuured,Please try", outerLayouit, "300");

    }

    @Override
    public void setPasswordError() {


    }




    @Override
    public void navigateToDashBoard() {

        startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
        finish();
    }

    @Override
    public SharedPreferences getSharedPreferences() {
        userPreference = this.getSharedPreferences("userPref", 0);
        return userPreference;
    }

    @Override
    public void roleError(String message) {

        CommonUtils.snackBar(message, outerLayouit, "300");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}

