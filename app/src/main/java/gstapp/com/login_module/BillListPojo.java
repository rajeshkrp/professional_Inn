package gstapp.com.login_module;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ModelClasses.BillListDetailPojo;

public class BillListPojo {


    @SerializedName("basicRate")
    @Expose
    private Integer basicRate;
    @SerializedName("totalInvoice")
    @Expose
    private Integer totalInvoice;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("bills")
    @Expose
    private List<BillListDetailPojo> bills = null;
    @SerializedName("response")
    @Expose
    private Boolean response;

    public Integer getBasicRate() {
        return basicRate;
    }

    public void setBasicRate(Integer basicRate) {
        this.basicRate = basicRate;
    }

    public Integer getTotalInvoice() {
        return totalInvoice;
    }

    public void setTotalInvoice(Integer totalInvoice) {
        this.totalInvoice = totalInvoice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BillListDetailPojo> getBills() {
        return bills;
    }

    public void setBills(List<BillListDetailPojo> bills) {
        this.bills = bills;
    }

    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }
    
    
    
    
    
    
}
