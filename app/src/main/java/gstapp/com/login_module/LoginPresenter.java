package gstapp.com.login_module;

import android.content.Context;

/**
 * Created by Abdul on 4/6/2018.
 */

public interface LoginPresenter {

    void validateCredentials(String username, String password, Context context);

    void onDestroy();

}
