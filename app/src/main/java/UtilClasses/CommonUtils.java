package UtilClasses;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;

/**
 * Created by Admin on 3/15/2018.
 */

public class CommonUtils {

    public static boolean isFirst = true;
    public static boolean isProfile = false;
    private static Context context;
    public static boolean isLoggedIn(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("userPref", 0);
        boolean isLoggedIn = sharedPreferences.getBoolean("isLoggedIn", false);

        return isLoggedIn;

    }

    public static void saveBillNo(Context context, String s) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("userPref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("BillNo", s);
        editor.apply();

    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static String getUserId(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("userPref", 0);
        String userId = sharedPreferences.getString("userID", "");

        return userId;

    }

    public static void saveStringPreferences(Context context, String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPreferencesString(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(key, "");
    }


    public static String getBillNo(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("userPref", 0);
        String bnillNO = sharedPreferences.getString("BillNo", null);

        return bnillNO;

    }

    public static LogeedUserDetails getUserDetail(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("userPref", 0);

        String fName = sharedPreferences.getString("first_name", "");
        String MName = sharedPreferences.getString("mid_name", "");
        String LName = sharedPreferences.getString("last_name", "");
        String userId = sharedPreferences.getString("userID", "");
        String emailId = sharedPreferences.getString("email", "");
        String gender = sharedPreferences.getString("gender", "");
        String password = sharedPreferences.getString("password", "");
        String role = sharedPreferences.getString("role", "");
        String company_name = sharedPreferences.getString("company_name", "");
        String company_logo = sharedPreferences.getString("company_logo", "");
        String gstn = sharedPreferences.getString("gstn", "");
        String cin = sharedPreferences.getString("cin", "");
        String pan = sharedPreferences.getString("pan", "");
        String bank_name = sharedPreferences.getString("bank_name", "");
        String bank_branch = sharedPreferences.getString("bank_branch", "");
        String ac_no = sharedPreferences.getString("ac_no", "");
        String ifsc_code = sharedPreferences.getString("ifsc_code", "");
        String ac_type = sharedPreferences.getString("ac_type", "");
        String bill_no_series = sharedPreferences.getString("bill_no_series", "");
        String state_code = sharedPreferences.getString("state_code", "");
        String state = sharedPreferences.getString("state", "");
        String address = sharedPreferences.getString("address", "");
        String mobile = sharedPreferences.getString("mobile", "");
        String created_at = sharedPreferences.getString("created_at", "");
        boolean isLoggedIn = sharedPreferences.getBoolean("isLoggedIn", false);


        LogeedUserDetails logeedUserDetails = new LogeedUserDetails();
        logeedUserDetails.setfName(fName);
        logeedUserDetails.setMName(MName);
        logeedUserDetails.setLName(LName);
        logeedUserDetails.setUserId(userId);
        logeedUserDetails.setGender(gender);
        logeedUserDetails.setPassword(password);
        logeedUserDetails.setRole(role);
        logeedUserDetails.setCompany_name(company_name);
        logeedUserDetails.setCompany_logo(company_logo);
        logeedUserDetails.setGstn(gstn);
        logeedUserDetails.setCin(cin);
        logeedUserDetails.setPan(pan);
        logeedUserDetails.setBank_name(bank_name);
        logeedUserDetails.setBank_branch(bank_branch);
        logeedUserDetails.setAc_no(ac_no);
        logeedUserDetails.setIfsc_code(ifsc_code);
        logeedUserDetails.setAc_type(ac_type);
        logeedUserDetails.setBill_no_series(bill_no_series);
        logeedUserDetails.setState_code(state_code);
        logeedUserDetails.setState(state);
        logeedUserDetails.setAddress(address);
        logeedUserDetails.setMobile(mobile);
        logeedUserDetails.setCreated_at(created_at);
        logeedUserDetails.setLoggedIn(isLoggedIn);

        return logeedUserDetails;


    }






    public static LogeedUserDetails setUserDetail(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("userPref", 0);

        String fName = sharedPreferences.getString("first_name", "");
        String MName = sharedPreferences.getString("mid_name", "");
        String LName = sharedPreferences.getString("last_name", "");
        String userId = sharedPreferences.getString("userID", "");
        String emailId = sharedPreferences.getString("email", "");
        String gender = sharedPreferences.getString("gender", "");
        String password = sharedPreferences.getString("password", "");
        String role = sharedPreferences.getString("role", "");
        String company_name = sharedPreferences.getString("company_name", "");
        String company_logo = sharedPreferences.getString("company_logo", "");
        String gstn = sharedPreferences.getString("gstn", "");
        String cin = sharedPreferences.getString("cin", "");
        String pan = sharedPreferences.getString("pan", "");
        String bank_name = sharedPreferences.getString("bank_name", "");
        String bank_branch = sharedPreferences.getString("bank_branch", "");
        String ac_no = sharedPreferences.getString("ac_no", "");
        String ifsc_code = sharedPreferences.getString("ifsc_code", "");
        String ac_type = sharedPreferences.getString("ac_type", "");
        String bill_no_series = sharedPreferences.getString("bill_no_series", "");
        String state_code = sharedPreferences.getString("state_code", "");
        String state = sharedPreferences.getString("state", "");
        String address = sharedPreferences.getString("address", "");
        String mobile = sharedPreferences.getString("mobile", "");
        String created_at = sharedPreferences.getString("created_at", "");
        boolean isLoggedIn = sharedPreferences.getBoolean("isLoggedIn", false);


        LogeedUserDetails logeedUserDetails = new LogeedUserDetails();
        logeedUserDetails.setfName(fName);
        logeedUserDetails.setMName(MName);
        logeedUserDetails.setLName(LName);
        logeedUserDetails.setUserId(userId);
        logeedUserDetails.setGender(gender);
        logeedUserDetails.setPassword(password);
        logeedUserDetails.setRole(role);
        logeedUserDetails.setCompany_name(company_name);
        logeedUserDetails.setCompany_logo(company_logo);
        logeedUserDetails.setGstn(gstn);
        logeedUserDetails.setCin(cin);
        logeedUserDetails.setPan(pan);
        logeedUserDetails.setBank_name(bank_name);
        logeedUserDetails.setBank_branch(bank_branch);
        logeedUserDetails.setAc_no(ac_no);
        logeedUserDetails.setIfsc_code(ifsc_code);
        logeedUserDetails.setAc_type(ac_type);
        logeedUserDetails.setBill_no_series(bill_no_series);
        logeedUserDetails.setState_code(state_code);
        logeedUserDetails.setState(state);
        logeedUserDetails.setAddress(address);
        logeedUserDetails.setMobile(mobile);
        logeedUserDetails.setCreated_at(created_at);
        logeedUserDetails.setLoggedIn(isLoggedIn);

        return logeedUserDetails;


    }




















    public static void snackBar(String s, View v, String statusCode) {

        try {
            Snackbar snackbar = Snackbar.make(v, s, Snackbar.LENGTH_LONG)
                    .setAction("Action", null);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextSize(18);
            tv.setTypeface(null, Typeface.BOLD);
            tv.setTextColor(Color.WHITE);
            tv.setGravity(Gravity.CENTER);

            if (statusCode.equals("200")) {
                snackbar.getView().setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.big_green));
            } else {
                snackbar.getView().setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.red));
            }


            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void makeUserLogout(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("userPref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isLoggedIn", false);
        editor.apply();
    }

    public static ApiInterface InitilizeInterface() {

        ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);

        return apiInterface;
    }




}
