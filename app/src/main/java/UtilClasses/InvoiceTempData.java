package UtilClasses;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ModelClasses.GSTDetails;

public class InvoiceTempData implements Comparable<InvoiceTempData>, Cloneable {

    private String custName;
    private String custAddress;
    private String WholeshippingAddress;

    public String getWholeshippingAddress() {
        return WholeshippingAddress;
    }

    public void setWholeshippingAddress(String wholeshippingAddress) {
        WholeshippingAddress = wholeshippingAddress;
    }

    public String getWholeBillAddress() {
        return WholeBillAddress;
    }

    public void setWholeBillAddress(String wholeBillAddress) {
        WholeBillAddress = wholeBillAddress;
    }

    private String WholeBillAddress;

    public String getBillAddress() {
        return billAddress;
    }

    public void setBillAddress(String billAddress) {
        this.billAddress = billAddress;
    }

    private String billAddress;
    private String custState;
    private String stateCode;

    public String getBillcustState() {
        return billcustState;
    }

    public void setBillcustState(String billcustState) {
        this.billcustState = billcustState;
    }

    private String billcustState;

    public String getBillstateCode() {
        return billstateCode;
    }

    public void setBillstateCode(String billstateCode) {
        this.billstateCode = billstateCode;
    }

    private String billstateCode;
    private String custGstn;
    private String productDesc;
    private String sacOrHas;
    private String amount;
    private String vehicle;

    public String getShiipingAddress() {
        return shiipingAddress;
    }

    public void setShiipingAddress(String shiipingAddress) {
        this.shiipingAddress = shiipingAddress;
    }

    private String shiipingAddress;

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getPertotalamount() {
        return pertotalamount;
    }

    public void setPertotalamount(String pertotalamount) {
        this.pertotalamount = pertotalamount;
    }

    private String pertotalamount;
    private String taxAmount;

    public String getTotalGst() {
        return totalGst;
    }

    public void setTotalGst(String totalGst) {
        this.totalGst = totalGst;
    }

    private String totalGst;
    private String invoiceNo;
    private String selectedCatgeory;
    private String totalAmount;
    private String IGSTAmount;

    public static InvoiceTempData invoiceTempData;

    public static InvoiceTempData getInstance() {

        if (invoiceTempData == null) {
            invoiceTempData = new InvoiceTempData();
        }

        return invoiceTempData;
    }

    public InvoiceTempData() {

    }


    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustState() {
        return custState;
    }

    public void setCustState(String custState) {
        this.custState = custState;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCustGstn() {
        return custGstn;
    }

    public void setCustGstn(String custGstn) {
        this.custGstn = custGstn;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getSacOrHas() {
        return sacOrHas;
    }

    public void setSacOrHas(String sacOrHas) {
        this.sacOrHas = sacOrHas;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }


    public String getIGSTAmountN() {
        return IGSTAmountN;
    }

    public void setIGSTAmountN(String IGSTAmountN) {
        this.IGSTAmountN = IGSTAmountN;
    }

    private String IGSTAmountN;

    public String getCGSTAmountN() {
        return CGSTAmountN;
    }

    public void setCGSTAmountN(String CGSTAmountN) {
        this.CGSTAmountN = CGSTAmountN;
    }

    public String getSGSTAmountN() {
        return SGSTAmountN;
    }

    public void setSGSTAmountN(String SGSTAmountN) {
        this.SGSTAmountN = SGSTAmountN;
    }

    public String getUGSTAmountN() {
        return UGSTAmountN;
    }

    public void setUGSTAmountN(String UGSTAmountN) {
        this.UGSTAmountN = UGSTAmountN;
    }

    private String CGSTAmountN;
    private String SGSTAmountN;
    private String UGSTAmountN;
    private String IGSTPer;
    private String CGSTAmount;
    private String CGSTPer;


    public String getUGSTPer() {
        return UGSTPer;
    }

    public void setUGSTPer(String UGSTPer) {
        this.UGSTPer = UGSTPer;
    }

    private String UGSTPer;

    public String getSGSTPer() {
        return SGSTPer;
    }

    public void setSGSTPer(String SGSTPer) {
        this.SGSTPer = SGSTPer;
    }

    private String SGSTPer;

    private String SGSTAmount;
    private String SGSTAmountPer;
    private String UGSTAmount;

    public String getUGSTAmount() {
        return UGSTAmount;
    }

    public void setUGSTAmount(String UGSTAmount) {
        this.UGSTAmount = UGSTAmount;
    }

    public String getUGSTAmountPer() {
        return UGSTAmountPer;
    }

    public void setUGSTAmountPer(String UGSTAmountPer) {
        this.UGSTAmountPer = UGSTAmountPer;
    }

    private String UGSTAmountPer;

    public String getCGSTAmountPer() {
        return CGSTAmountPer;
    }

    public void setCGSTAmountPer(String CGSTAmountPer) {
        this.CGSTAmountPer = CGSTAmountPer;
    }

    private String CGSTAmountPer;

    private String rate;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    private String quantity;
    private String serialNumber;

    public String getRatewithQtyAmt() {
        return ratewithQtyAmt;
    }

    public void setRatewithQtyAmt(String ratewithQtyAmt) {
        this.ratewithQtyAmt = ratewithQtyAmt;
    }

    private String ratewithQtyAmt;


    InvoiceTempData(String productDesc, String sacOrHas) {

        this.productDesc = productDesc;
        this.sacOrHas = sacOrHas;


    }

    public List<GSTDetails> getGstDetailsList() {
        return gstDetailsList;
    }

    public void setGstDetailsList(List<GSTDetails> gstDetailsList) {
        this.gstDetailsList = gstDetailsList;
    }

    private List<GSTDetails> gstDetailsList = null;


    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }


    public String getSelectedCatgeory() {
        return selectedCatgeory;
    }

    public void setSelectedCatgeory(String selectedCatgeory) {
        this.selectedCatgeory = selectedCatgeory;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getIGSTAmount() {
        return IGSTAmount;
    }

    public void setIGSTAmount(String IGSTAmount) {
        this.IGSTAmount = IGSTAmount;
    }

    public String getIGSTPer() {
        return IGSTPer;
    }

    public void setIGSTPer(String IGSTPer) {
        this.IGSTPer = IGSTPer;
    }

    public String getCGSTAmount() {
        return CGSTAmount;
    }

    public void setCGSTAmount(String CGSTAmount) {
        this.CGSTAmount = CGSTAmount;
    }

    public String getCGSTPer() {
        return CGSTPer;
    }

    public void setCGSTPer(String CGSTPer) {
        this.CGSTPer = CGSTPer;
    }

    public String getSGSTAmount() {
        return SGSTAmount;
    }

    public void setSGSTAmount(String SGSTAmount) {
        this.SGSTAmount = SGSTAmount;
    }

    public String getSGSTAmountPer() {
        return SGSTAmountPer;
    }

    public void setSGSTAmountPer(String SGSTAmountPer) {
        this.SGSTAmountPer = SGSTAmountPer;
    }

    public static InvoiceTempData getInvoiceTempData() {
        return invoiceTempData;
    }

    public static void setInvoiceTempData(InvoiceTempData invoiceTempData) {
        InvoiceTempData.invoiceTempData = invoiceTempData;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    @Override
    public int compareTo(@NonNull InvoiceTempData invoiceTempData1) {
        return selectedCatgeory.compareTo(invoiceTempData1.getSelectedCatgeory());
    }

    @Override
    public InvoiceTempData clone() {
        try {
            return (InvoiceTempData) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
